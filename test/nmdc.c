/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <global.h>

#undef NDEBUG
#include <assert.h>


static void t_myinfo() {
	nmdc_myinfo_t nfo;
	/* Make sure valgrind can detect read buffer overflows. */
#define X(x, v) do {\
		char *buf = malloc(-1+sizeof x);\
		memcpy(buf, x, -1+sizeof x);\
		assert(nmdc_myinfo_parse(&nfo, "UTF-8", buf) == v);\
		free(buf);\
	} while(0)

#define F(x) do { X(x, false); nmdc_myinfo_free(&nfo); } while(0)
	F("|");
	F("$MyINFO $ALL|");
	F("$MyINFO $ALL |");
	F("$MyINFO $ALL Nick|");
	F("$MyINFO $ALL Nick $ $|");
	F("$MyINFO $ALL Nick $ $1$$$ |");
#undef F

#define T(x, tnick, tdesc, tconn, tmail, tclient, thn, thr, tho, tslots, tflag, tmode, tas, tshare) do {\
		X(x, true);\
		assert(strcmp(nfo.nick, tnick) == 0);\
		assert(tdesc   ? strcmp(nfo.desc,   tdesc  ?tdesc  :"") == 0 : !nfo.desc  );\
		assert(tconn   ? strcmp(nfo.conn,   tconn  ?tconn  :"") == 0 : !nfo.conn  );\
		assert(tmail   ? strcmp(nfo.mail,   tmail  ?tmail  :"") == 0 : !nfo.mail  );\
		assert(tclient ? strcmp(nfo.client, tclient?tclient:"") == 0 : !nfo.client);\
		assert(nfo.hn    == thn   );\
		assert(nfo.hr    == thr   );\
		assert(nfo.ho    == tho   );\
		assert(nfo.slots == tslots);\
		assert(nfo.flag  == tflag );\
		assert(nfo.mode  == tmode );\
		assert(nfo.as    == tas   );\
		assert(nfo.share == tshare);\
		nmdc_myinfo_free(&nfo);\
	} while(0)

	T("$MyINFO $ALL Nick $ $\x01$$0$|",
		"Nick", NULL, NULL, NULL, NULL,
		0, 0, 0, 0, 1, 0, 0, 0);
	T("$MyINFO $ALL Ayo description$ $connection1$email$0$|",
		"Ayo", "description", "connection", "email", NULL,
		0, 0, 0, 0, '1', 0, 0, 0);
	T("$MyINFO $ALL 123 <++ V:0.23,M:P,H:1/2/3,S:4,O:5>$ $c1$e$1234$|",
		"123", NULL, "c", "e", "++ 0.23",
		1, 2, 3, 4, '1', 'P', 5*1024, 1234);
	T("$MyINFO $ALL The-Master $A$Modem\x0b$$$|",
		"The-Master", NULL, "Modem", NULL, NULL,
		0, 0, 0, 0, 0x0b, 'A', 0, 0);
#undef T
}


int main(int argc, char **argv) {
	/* nmdc_convert_check() */
#define T(x) assert(nmdc_convert_check(x) == true)
#define F(x) assert(nmdc_convert_check(x) == false)
	T("UTF-8");
	T("CP1250");
	T("CP1251");
	T("CP1252");
	T("ISO-8859-7");
	T("KOI8-R");
	F("UTF-16");
	F("SOME_NONEXISTING_ENCODING");
#undef T
#undef F

	/* nmdc_escape() and nmdc_unescape() */
#define T(in, out) do {\
		char *x = nmdc_escape("UTF-8", in, -1+sizeof in);\
		assert(strcmp(x, out) == 0);\
		free(x);\
		x = nmdc_unescape("UTF-8", out, -1+sizeof out);\
		assert(strcmp(x, in) == 0);\
		free(x);\
	} while(0)
	T("", "");
	T("a", "a");
	T("a b c", "a b c");
	T("|&$", "&#124;&&#36;");
	T(" | & $ ", " &#124; & &#36; ");
	T("&amp; &#124; &#36;", "&amp;amp; &amp;#124; &amp;#36;");
#undef T

	/* nmdc_myinfo_parse() */
	t_myinfo();

	/* nmdc_conn2speed() */
#define T(in, out) assert(nmdc_conn2speed(in) == out);
	T(NULL, 0);
	T("", 0);
	T("0.005", 625);
	T("1", 125000);
	T("5", 625000);
	T("1000", 125000000);
	T("100 KiB/s", 100*1024);
	T("19.00 KiB/s", 19*1024);
	T("DSL", 128*1024);
	T("LAN(T3)", 5592000);
	T("56 Kbps", 7000);
	T("33.6Kbps", 4200);
	/*T("169,00 KiB/s", 169*1024); This one fails, but a comma is sometimes used in practice. :-( */
#undef T

	/* nmdc_chat_parse() */
#define T(in, nickoff, nicklen, isme, msgoff) do {\
		const char *m = in; char *no; int nl; bool me;\
		assert(nmdc_chat_parse(m, &no, &nl, &me) == m+msgoff);\
		assert(no == (nickoff ? m+nickoff : NULL));\
		if(nickoff)\
			assert(nl == nicklen);\
		assert(me == isme);\
	} while(0)
	T("|", 0, 0, false, 0);
	T("<|", 0, 0, false, 0);
	T("<>|", 0, 0, false, 0);
	T("<> |", 0, 0, false, 0);
	T("<X>|", 0, 0, false, 0);
	T("*|", 0, 0, false, 0);
	T("**|", 0, 0, false, 0);
	T("** |", 0, 0, false, 0);
	T("**X|", 0, 0, false, 0);
	T("**X |", 0, 0, false, 0);
	T("Message|", 0, 0, false, 0);
	T("<X> |", 1, 1, false, 4);
	T("<Ayo> Cats are cute|", 1, 3, false, 6);
	T("** Ayo says Hi!|", 3, 3, true, 7);
#undef T

	return 0;
}

/* vim: set noet sw=4 ts=4: */
