/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <global.h>

#undef NDEBUG
#include <assert.h>


/* Tests adc_str2sid() and its reverse */
static void t_sid() {
	char str[5];

#define F(in) assert(adc_str2sid(in) == -1)
	F("");
	F("Z");
	F("ZZ");
	F("ZZZ");
	F("FF0F");
	F("1234");
#undef F

#define T(_str, _sid)\
		assert(adc_str2sid(_str) == _sid);\
		assert(adc_str2sid(_str "\n") == _sid);\
		assert(adc_str2sid(_str "EXTENSIONS!") == _sid);\
		str[0] = str[1] = str[2] = str[3] = str[4] = 0;\
		adc_sid2str(_sid, str);\
		assert(strcmp(str, _str) == 0);
	T("AAAA", 0);
	T("7777", 1048575);
	T("AAAB", 1);
	T("AAA7", 31);
	T("AABA", 32);
	T("ABAA", 1024);
	T("BAAA", 32768);
#undef T
}


/* Testing the adc_(un)escape(_kstr) functions. */
static void t_escape() {
	kstring_t kstr;
	char *str;

	/* Incorrect escapes */
#define F(_str)\
		memset(&kstr, 0, sizeof(kstring_t));\
		assert(!adc_unescape_kstr(&kstr, _str));\
		free(kstr.s);\
		assert(!adc_unescape(_str));
	F("\\");
	F("\\f");
	F("\\b");
	F("aaa\\");
	F("aaa\\  ");
#undef F

	/* Round-trips */
#define CMP(_func, _in, _out)\
		memset(&kstr, 0, sizeof(kstring_t));\
		kputs("PREFIX", &kstr);\
		_func##_kstr(&kstr, _in);\
		assert(strcmp(kstr.s, "PREFIX" _out) == 0);\
		free(kstr.s);\
		str = _func(_in);\
		assert(str != NULL && strcmp(str, _out) == 0);\
		free(str);
#define T(_unesc, _esc)\
		CMP(adc_escape, _unesc, _esc);\
		CMP(adc_unescape, _esc, _unesc);\
		CMP(adc_unescape, _esc" GARBAGE", _unesc);\
		CMP(adc_unescape, _esc"\nGARBAGE", _unesc);
	T("", "");
	T("A", "A");
	T("\\", "\\\\");
	T("\n", "\\n");
	T(" ", "\\s");
	T("\r\n", "\r\\n");
	T("\n\n\n", "\\n\\n\\n");
	T("A .()!@#$%^&*()_=+-á", "A\\s.()!@#$%^&*()_=+-á");
	T("オリジナルサウンドトラック", "オリジナルサウンドトラック");
#undef T
#undef CMP
}


static void t_validate() {
#define F(s) assert(adc_validate(s, sizeof s - 1) == 0)
#define T(s)\
		assert(adc_validate(s, sizeof s - 1) == sizeof s - 1);\
		assert(adc_validate(s "GARBAGE", sizeof s + 6) == sizeof s - 1);
	F("");
	F("F");
	F("IMSG");
	T("IMSG\n");
	T("HUNK\n");
	F("HuNK\n");
	F(" IMSG\n");
	F("XMSG\n");
	F("BMSG\n");
	F("BMSG \n");
	F("BMSG 445\n");
	F("BMSG 4451\n");
	F("BMSG 445F0\n");
	T("BMSG 4455\n");
	T("BMSG 44X5\n");
	F("BMSG 44x5\n");
	F("EMSG 4455\n");
	F("EMSG 4455 445\n");
	T("EMSG 4455 445X\n");
	F("DMSG 4455\n");
	T("DMSG 4455 445X\n");
	T("HSUP \n");
	T("HSUP  \n");
	T("HSUP 0 \n");
	T("HSUP  0 \n");
	T("HSUP A B\n");
	T("HSUP A  B\n");
	T("HSUP A  B \n");
	T("HMSG Random\\sString\n");
	T("HMSG Ra\\\\nd\\som\\sStr\\ning\n");
	F("HMSG Ra\\xnd\\som\\sStr\\ning\n");
	F("HMSG A B\\ C\n");
	F("USUP\n");
	F("USUP \n");
	T("USUP A\n");
	T("USUP ABCDEFGHIJKLMNOPQRSTUVWXYZ234567\n");
	F("USUP 0\n");
	F("FSUP\n");
	F("FSUP ABCD\n");
	F("FSUP ABCD \n");
	T("FSUP ABCD +WHUT-ZOMG\n");
	T("HARG \t\r\n");
	F("HARG \0\n");
	F("HARG \1\n");
	T("HARG オリジナルサウンドトラック\n");
	T("HARG \xC2\xA2 \xE2\x82\xAC \xF0\xA4\xAD\xA2\n"); /* UTF-8 examples from Wikipedia */
	F("HARG \xC2");
	F("HARG \xC2\n");
	F("HARG \xC0\x80\n");
	F("HARG \xF0\x82\x82\xAC\n"); /* Overlong (Example from wikipedia) */
#undef F
#undef T
}


int main(int argc, char **argv) {
	t_sid();
	t_escape();
	t_validate();
	return 0;
}

/* vim: set noet sw=4 ts=4: */
