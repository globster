/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <global.h>

#undef NDEBUG
#include <assert.h>


static char *hexdecode(const char *hex) {
	char *dat = malloc(strlen(hex)/2);
	unsigned char *cur = (unsigned char *)dat;
	while(*hex) {
		assert(sscanf(hex, "%02hhx", cur) == 1);
		hex += 2;
		cur++;
	}
	return dat;
}


#define T(dat, len, b32) do {\
		char *buf = malloc(len*8/5+2);\
		base32_encode(dat, buf, len);\
		assert(strcmp(buf, b32) == 0);\
		free(buf);\
		buf = malloc(len);\
		base32_decode(b32, buf, len);\
		assert(memcmp(dat, buf, len) == 0);\
		free(buf);\
		assert(isbase32(b32));\
		assert(!isbase32("_"b32));\
		assert(!isbase32(b32"0"));\
	} while(0)

#define S(str, b32) T(str, (-1+sizeof str), b32)
#define H(hex, b32) do {\
		char *hdat = hexdecode(hex);\
		T(hdat, (-1+sizeof hex)/2, b32);\
		free(hdat);\
	} while(0)


int main(int argc, char **argv) {
	/* Some simple test vectors from RFC4648 */
	S("", "");
	S("f", "MY");
	S("fo", "MZXQ");
	S("foo", "MZXW6");
	S("foob", "MZXW6YQ");
	S("fooba", "MZXW6YTB");
	S("foobar", "MZXW6YTBOI");

	/* ...but I want to test more sizes */
	S("abcdefg", "MFRGGZDFMZTQ");
	S("abcdefgh", "MFRGGZDFMZTWQ");
	S("abcdefghi", "MFRGGZDFMZTWQ2I");
	S("abcdefghij", "MFRGGZDFMZTWQ2LK");
	S("abcdefghijk", "MFRGGZDFMZTWQ2LKNM");
	S("abcdefghijkl", "MFRGGZDFMZTWQ2LKNNWA");
	S("abcdefghijklm", "MFRGGZDFMZTWQ2LKNNWG2");
	S("abcdefghijklmn", "MFRGGZDFMZTWQ2LKNNWG23Q");
	S("abcdefghijklmno", "MFRGGZDFMZTWQ2LKNNWG23TP");
	S("abcdefghijklmnop", "MFRGGZDFMZTWQ2LKNNWG23TPOA");
	S("abcdefghijklmnopq", "MFRGGZDFMZTWQ2LKNNWG23TPOBYQ");
	S("abcdefghijklmnopqr", "MFRGGZDFMZTWQ2LKNNWG23TPOBYXE");
	S("abcdefghijklmnopqrs", "MFRGGZDFMZTWQ2LKNNWG23TPOBYXE4Y");
	S("abcdefghijklmnopqrst", "MFRGGZDFMZTWQ2LKNNWG23TPOBYXE43U");
	S("abcdefghijklmnopqrstu", "MFRGGZDFMZTWQ2LKNNWG23TPOBYXE43UOU");
	S("abcdefghijklmnopqrstuv", "MFRGGZDFMZTWQ2LKNNWG23TPOBYXE43UOV3A");
	S("abcdefghijklmnopqrstuvw", "MFRGGZDFMZTWQ2LKNNWG23TPOBYXE43UOV3HO");
	S("abcdefghijklmnopqrstuvwx", "MFRGGZDFMZTWQ2LKNNWG23TPOBYXE43UOV3HO6A");
	S("abcdefghijklmnopqrstuvwxy", "MFRGGZDFMZTWQ2LKNNWG23TPOBYXE43UOV3HO6DZ");
	S("abcdefghijklmnopqrstuvwxyz", "MFRGGZDFMZTWQ2LKNNWG23TPOBYXE43UOV3HO6DZPI");
	S("abcdefghijklmnopqrstuvwxyz0", "MFRGGZDFMZTWQ2LKNNWG23TPOBYXE43UOV3HO6DZPIYA");
	S("abcdefghijklmnopqrstuvwxyz01", "MFRGGZDFMZTWQ2LKNNWG23TPOBYXE43UOV3HO6DZPIYDC");
	S("abcdefghijklmnopqrstuvwxyz012", "MFRGGZDFMZTWQ2LKNNWG23TPOBYXE43UOV3HO6DZPIYDCMQ");
	S("abcdefghijklmnopqrstuvwxyz0123", "MFRGGZDFMZTWQ2LKNNWG23TPOBYXE43UOV3HO6DZPIYDCMRT");
	S("abcdefghijklmnopqrstuvwxyz01234", "MFRGGZDFMZTWQ2LKNNWG23TPOBYXE43UOV3HO6DZPIYDCMRTGQ");
	S("abcdefghijklmnopqrstuvwxyz012345", "MFRGGZDFMZTWQ2LKNNWG23TPOBYXE43UOV3HO6DZPIYDCMRTGQ2Q");
	S("abcdefghijklmnopqrstuvwxyz0123456", "MFRGGZDFMZTWQ2LKNNWG23TPOBYXE43UOV3HO6DZPIYDCMRTGQ2TM");
	S("abcdefghijklmnopqrstuvwxyz01234567", "MFRGGZDFMZTWQ2LKNNWG23TPOBYXE43UOV3HO6DZPIYDCMRTGQ2TMNY");
	S("abcdefghijklmnopqrstuvwxyz012345678", "MFRGGZDFMZTWQ2LKNNWG23TPOBYXE43UOV3HO6DZPIYDCMRTGQ2TMNZY");
	S("abcdefghijklmnopqrstuvwxyz0123456789", "MFRGGZDFMZTWQ2LKNNWG23TPOBYXE43UOV3HO6DZPIYDCMRTGQ2TMNZYHE");

	/* ...and binary stuff */
	H("00", "AA");
	H("AA", "VI");
	H("FF", "74");
	H("0000", "AAAA");
	H("AAAA", "VKVA");
	H("FFFF", "777Q");
	/* File hashes */
	H("4EC2BCF9684EFEAA626496576507E17FD49927CD68E2DFAD", "J3BLZ6LIJ37KUYTESZLWKB7BP7KJSJ6NNDRN7LI");
	H("FB2055A44DF4423697B699552BAAF910FBA0753007803B93", "7MQFLJCN6RBDNF5WTFKSXKXZCD52A5JQA6ADXEY");
	H("97461502D84B8CAA958EC035BD68687907DBC9534116551D", "S5DBKAWYJOGKVFMOYA2322DIPED5XSKTIELFKHI");
	/* Some stuff I received as GPA challenge */
	H("36A45459CDD7A4B2DA13F0E3038C0BABD0C51BD971C739E6", "G2SFIWON26SLFWQT6DRQHDALVPIMKG6ZOHDTTZQ");
	H("987DEDF2186E1FD27832381125880B0485535CC398E3C4C63AEB3B3484B0AA77", "TB6634QYNYP5E6BSHAISLCALASCVGXGDTDR4JRR25M5TJBFQVJ3Q");
	H("4788E8D646891F4DC8C09FCCCF92991BDEB860359086C51831AA1E6A0D212D6E", "I6EORVSGREPU3SGAT7GM7EUZDPPLQYBVSCDMKGBRVIPGUDJBFVXA");
	return 0;
}

/* vim: set noet sw=4 ts=4: */
