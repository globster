EXTRA_DIST=
noinst_LIBRARIES=
noinst_PROGRAMS=
MOSTLYCLEANFILES=
AM_CPPFLAGS=$(DBUS_CFLAGS) $(GNUTLS_CFLAGS) -I$(srcdir)/src -Isrc\
	-I$(srcdir)/deps/freetiger -I$(srcdir)/deps/klib -I$(srcdir)/deps/ylib -DYLOG_PTHREAD


# Manual pages
if USE_POD2MAN
man_MANS=doc/globster.1 doc/globster-launch.1 doc/globsterctl.1 doc/globster-api.7
MOSTLYCLEANFILES+=$(man_MANS)
endif
EXTRA_DIST+=doc/globster.pod doc/globster-launch.pod doc/globsterctl.pod doc/api.pod

POD2MANRULE=$(AM_V_GEN)mkdir -p doc && pod2man --center "globster manual" --release "@PACKAGE@-@VERSION@" $< >$@
doc/globster.1: $(srcdir)/doc/globster.pod
	$(POD2MANRULE)
doc/globster-launch.1: $(srcdir)/doc/globster-launch.pod
	$(POD2MANRULE)
doc/globsterctl.1: $(srcdir)/doc/globsterctl.pod
	$(POD2MANRULE)
doc/globster-api.7: $(srcdir)/doc/api.pod
	$(POD2MANRULE)


# deps/ stuff
noinst_LIBRARIES+=libdeps.a
libdeps_a_CFLAGS=-w
libdeps_a_SOURCES=

# libev
if HAVE_SYSEV
EV_LIBS=-lev
else
EV_LIBS=
libdeps_a_SOURCES+=deps/ev/ev.c
AM_CPPFLAGS+=-I$(srcdir)/deps/ev
endif
EXTRA_DIST+=\
	deps/ev/LICENSE\
	deps/ev/ev_epoll.c\
	deps/ev/ev.h\
	deps/ev/ev_kqueue.c\
	deps/ev/ev_poll.c\
	deps/ev/ev_port.c\
	deps/ev/ev_select.c\
	deps/ev/ev_vars.h\
	deps/ev/ev_wrap.h

# freetiger
libdeps_a_SOURCES+=deps/freetiger/tiger.c
EXTRA_DIST+=\
	deps/freetiger/tiger.h\
	deps/freetiger/ttable.h

# klib
libdeps_a_SOURCES+=deps/klib/kstring.c
EXTRA_DIST+=\
	deps/klib/khash.h\
	deps/klib/kstring.h

# ylib
libdeps_a_SOURCES+=\
	deps/ylib/dbusev.c\
	deps/ylib/evtp.c\
	deps/ylib/sqlasync.c\
	deps/ylib/ylog.c\
	deps/ylib/yuri.c
EXTRA_DIST+=\
	deps/ylib/dbusev.h\
	deps/ylib/evtp.h\
	deps/ylib/sqlasync.h\
	deps/ylib/ylog.h\
	deps/ylib/yopt.h\
	deps/ylib/yuri.h



# Tests for some internal stuff
TESTS=test/adc test/base32 test/nmdc test/vec
check_PROGRAMS=$(TESTS)
test_adc_SOURCES=test/adc.c src/util/adc.c
test_base32_SOURCES=test/base32.c src/util/base32.c
test_nmdc_SOURCES=test/nmdc.c src/util/nmdc.c
test_nmdc_LDADD=libdeps.a -lm
test_vec_SOURCES=test/vec.c


# util/
dist_bin_SCRIPTS=util/globsterctl util/globster-launch


# globster
bin_PROGRAMS=globster
globster_LDADD=libdeps.a -lm -lpthread $(Z_LIBS) $(EV_LIBS) $(DBUS_LIBS) $(GCRYPT_LIBS) $(GNUTLS_LIBS) $(SQLITE_LIBS)
globster_SOURCES=\
	src/app.c\
	src/db.c\
	src/dbo.c\
	src/hub/adc.c\
	src/hub/chat.c\
	src/hub/connection.c\
	src/hub/hub.c\
	src/hub/manager.c\
	src/hub/nmdc.c\
	src/hub/users.c\
	src/main.c\
	src/util/adc.c\
	src/util/base32.c\
	src/util/logfile.c\
	src/util/netstream.c\
	src/util/netutil.c\
	src/util/nmdc.c

nodist_globster_SOURCES=src/interfaces.c

noinst_HEADERS=\
	src/app.h\
	src/compat.h\
	src/db.h\
	src/dbo.h\
	src/global.h\
	src/hub/adc.h\
	src/hub/chat.h\
	src/hub/connection.h\
	src/hub/global.h\
	src/hub/hub.h\
	src/hub/local.h\
	src/hub/manager.h\
	src/hub/nmdc.h\
	src/hub/users.h\
	src/main.h\
	src/util/adc.h\
	src/util/base32.h\
	src/util/list.h\
	src/util/logfile.h\
	src/util/netstream.h\
	src/util/netutil.h\
	src/util/nmdc.h\
	src/util/vec.h

MOSTLYCLEANFILES+=version.h src/interfaces.c src/interfaces.h
EXTRA_DIST+=src/itfgen.pl


# Create a separate version.h and make sure only app.c depends on it. This
# avoids the need to recompile everything on each commit.
if USE_GIT_VERSION
version.h: $(srcdir)/.git/logs/HEAD
	$(AM_V_GEN)echo '"'"`git describe --abbrev=8 --dirty=-d`"'"' >version.h
else
version.h: Makefile
	$(AM_V_GEN)echo '"'"@VERSION@"'"' >version.h
endif
src/app.$(OBJEXT): version.h


$(globster_OBJECTS): src/interfaces.h
src/interfaces.h: src/interfaces.c
src/interfaces.c: $(srcdir)/doc/api.pod $(srcdir)/src/itfgen.pl
	$(AM_V_GEN)$(srcdir)/src/itfgen.pl $@ src/interfaces.h <$<
