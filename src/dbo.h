/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


#ifndef DBO_H
#define DBO_H


typedef enum {
	DBO_DEPRECATED =  1, /* Methods, signals, properties */
	DBO_NOREPLY    =  2, /* Methods */
	DBO_READ       =  4, /* Properties */
	DBO_WRITE      =  8, /* Properties */
	DBO_READWRITE  =  8|4,
	DBO_EMITNONE   = 16, /* Properties: EmitsChangedSignal = false */
	DBO_EMITCHANGE = 32  /* Properties: EmitsChangedSignal = invalidates */
} dbo_flags_t;


typedef void(*dbo_wrapper_t)(void *, DBusMessage *, DBusMessageIter *, void *);
typedef void(*dbo_getter_t)(void *, DBusMessageIter *);


typedef struct {
	const char *name;
	dbo_flags_t flags;  /* deprecated, noreply */
	const char *in;
	const char *out;
	dbo_wrapper_t wrapper;
} dbo_method_t;


typedef struct {
	const char *name;
	dbo_flags_t flags;  /* deprecated */
	const char *args;
} dbo_signal_t;


typedef struct {
	const char *name;
	const char *type;
	dbo_flags_t flags; /* read,write,deprecated,emitnone,emitchange */
	dbo_getter_t getter;
	dbo_wrapper_t setter;
} dbo_property_t;


typedef struct {
	const char *name;
	int n_methods, n_signals, n_properties;
	const dbo_method_t *methods;
	const dbo_signal_t *signals;
	const dbo_property_t *properties;
} dbo_interface_t;


typedef struct {
	const dbo_interface_t *itf;
	void **vtable;
	size_t poffset; /* Offset from the dbo_t struct to the properties struct */
} dbo_reg_t;


typedef struct {
	char *path;
	const dbo_reg_t *regs;
	int numregs;
	bool hasprops;
} dbo_t;



/* Register the dbo handlers to dbuscon. */
void dbo_init();


/* Export a new object at the given path. The *regs argument should point to a
 * list of interfaces exported on this object. E.g.
 *
 *   static dbo_reg_t regs[] = {
 *     { itf, vtable, offsetof(my_struct, name_of_itf_properties_member) },
 *     ..
 *   };
 *   dbo_register(myobj, mypath, regs, sizeof(regs)/sizeof(*regs));
 */
void dbo_register(dbo_t *o, const char *path, const dbo_reg_t *regs, int numregs);


/* Remove the object. The same object can be re-used again with _register(). */
static inline void dbo_unregister(dbo_t *o) {
	dbus_connection_unregister_object_path(dbuscon, o->path);
}


/* Convenient send-and-free wrapper */
static inline void dbo_sendfree(DBusMessage *msg) {
	dbus_connection_send(dbuscon, msg, NULL);
	dbus_message_unref(msg);
}


/* Send a reply to a "set" message. */
static inline void dbo_prop_reply(DBusMessage *msg) {
	dbo_sendfree(dbus_message_new_method_return(msg));
}


/* API for sending out the PropertiesChanged signal */

typedef struct {
	unsigned char n;
	/* Property indices to notify. This struct definition imposes a limit of 7
	 * properties per batch notification, and requires that an interface has no
	 * more than 256 properties. The upside is that the struct can easily be
	 * stack-allocated. (In fact, this struct is as small as a int64_t) */
	unsigned char l[7];
} dbo_prop_changed_t;

/* If table == NULL, the properties will only be invalidated. Otherwise, their
 * values will be sent with the signal. */
void dbo_prop_changed(dbo_prop_changed_t props, void *obj, const dbo_interface_t *itf, void *table);


#endif
/* vim: set noet sw=4 ts=4: */
