#!/usr/bin/perl

#  Copyright (c) 2012-2013 Yoran Heling
#
#  Permission is hereby granted, free of charge, to any person obtaining
#  a copy of this software and associated documentation files (the
#  "Software"), to deal in the Software without restriction, including
#  without limitation the rights to use, copy, modify, merge, publish,
#  distribute, sublicense, and/or sell copies of the Software, and to
#  permit persons to whom the Software is furnished to do so, subject to
#  the following conditions:
#
#  The above copyright notice and this permission notice shall be included
#  in all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
#  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
#  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
#  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
#  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


use strict;
use warnings;
use Digest::SHA;

# TODO: Provide some sort of support for C enums

my %dbus_basic_types = (
	qw|y uint8_t b bool n int16_t q uint16_t i int32_t u uint32_t x int64_t t uint64_t d double|,
	's', 'char *',
	'o', 'char *',
	'g', 'char *',
);

# itf1 => { name, methods, signals, properties }, ...
# methods = [ { name, flags, in => [ [ type, name ], .. ], out }, .. ]
# signals = [ { name, args => [ [ type, name ], .. ] }, .. ]
# properties = [ { type, name, access, flags }, .. ]
my %itf;


sub read_itf {
	my $F = do { local $/; <STDIN> };
	my $itf;

	sub p_arglist {
		return map m/ /?[ split / / ]:(), split /, +/, shift||'';
	}

	# TODO: Support multiline method/signal definitions?
	# TODO: Flags
	my $m_name = qr/[a-zA-Z0-9_]+/;
	my $m_type = qr/[a-z{}\)\(]+/;
	my $m_arglist = qr/\(((?:,? *$m_type +$m_name)*)\)/;
	while($F =~ m{^(?:
		=head.\s+(net\.blicky\.[a-zA-Z0-9-.]+)\s\(([a-z0-9]+)\)    | # 1, 2
		[\s\t]+method\s+($m_name)$m_arglist(?:\s+->\s+$m_arglist)? | # 3, 4, 5
		[\s\t]+signal\s+($m_name)$m_arglist                        | # 6, 7
		[\s\t]+property\s+($m_type)\s+($m_name)\s+(r|w|rw)           # 8, 9, 10
	)$}mxg) {
		my($i_name, $i_key, $m_name, $m_in, $m_out, $s_name, $s_args, $p_type, $p_name, $p_access)
			= ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10);
		#printf "%s\n", $i_key || $m_name || $s_name || $p_name;

		# New interface
		if($i_key) {
			$itf = $i_key;
			$itf{$itf} = { name => $i_name, map +($_=>[]), qw|methods signals properties| };
			next;
		}

		# Method
		if($m_name) {
			#printf "method %s(%s) -> (%s)\n", $m_name, $m_in, $m_out||'';
			push @{$itf{$itf}{methods}}, {
				name => $m_name,
				flags => '',
				in => [p_arglist $m_in],
				out => [p_arglist $m_out]
			};
			next;
		}

		# Signal
		if($s_name) {
			#print "signal $s_name($s_args)\n";
			push @{$itf{$itf}{signals}}, {
				name => $s_name,
				flags => '',
				args => [p_arglist $s_args]
			};
			next;
		}

		# Property
		if($p_name) {
			die "Unsupported property type for $p_name\n" if !$dbus_basic_types{$p_type};
			#print "property $p_type $p_name $p_access\n";
			push @{$itf{$itf}{properties}}, {
				name => $p_name,
				type => $p_type,
				access => $p_access,
				flags => '',
			};
			next;
		}
	}
}


# Creates a signature string from an argument list.
sub arg_sig {
	join '', map $_->[0], @{$_[0]};
}


# Converts a signature into a string that can be used in a C identifier name,
# without really changing its meaning.
sub sig_cname {
	(local $_ = shift) =~ tr/\{\}\(\)/eErR/;
	return $_;
}


# "Normalize" a signature so that signatures that result in equivalent code for
# the wrapper and reply/signal functions also result in the same signature string.
# That is, types that result in the use of a generic DBusMessageIter are
# considered equivalent to 'v'.
# XXX: This function assumes that the wrapper and reply/signal function
# generators have the same type support (With the exception of 'v', which
# reply/signal functions don't support).
sub sig_normalize {
	local $_ = shift;
	my $r='';
	# Consume a single complete type from $_
	sub consume {
		s/^a// && return consume();
		s/^\{// && consume() && consume() && return s/\}//;
		if(s/^\(//) {
			consume() while(!s/^\)//);
			return 1;
		}
		s/^.//;
		return 1;
	}
	while($_) {
		# We don't support arrays, dicts, structs and unix_fd's (yet).
		$r .= 'v', consume(), next if s/^a// || /^\{/ || /^\(/ || /^h/;
		# Otherwise, assume a basic type that we support.
		s/^(.)//, $r .= $1;
	}
	return $r;
}


# Splits a normalized signature.
sub sig_split {
	return split //, shift;
}


# Returns the C type of a single complete normalized type.
sub sig_ctype {
	return $dbus_basic_types{ shift() }||'DBusMessageIter *';
}


# Same as sig_ctype, but prepends 'const' for strings.
sub sig_ctypec {
	my $t = shift;
	return ($t =~ /^[sog]$/ ? 'const ' : '').sig_ctype($t);
}


# Returns the c expression to compare two values of the same basic type for equality.
sub sig_eq {
	my($t, $a, $b) = @_;
	return $t =~ /^[sog]$/ ? "strcmp($a, $b) == 0" : "$a == $b";
}


# Returns the type (as a function pointer) of a method corresponding to the normalized signature.
sub method_type {
	my($sig, $objtype, $defname) = @_;
	return "void(*$defname)($objtype *, DBusMessage *".join('', map ', '.sig_ctypec($_), sig_split $sig).')';
}


# Creates a wrapper function for the given normalized signature.
# TODO: Would it make sense to declare all arguments first? And in a more
#       optimal order? Or will the compiler do that for us already?
# TODO: Better support for non-basic types?
sub wrapper_create {
	my $sig = shift;
	my @t = sig_split($sig);
	return
		"static void wrapper_".sig_cname($sig)."(void *obj, DBusMessage *msg, DBusMessageIter *iter, void *func) {\n".
		join("\tdbus_message_iter_next(iter);\n", map {
			$dbus_basic_types{$t[$_]}
			? "\t".($t[$_] eq 'b' ? 'dbus_bool_t' : sig_ctypec($t[$_]))." arg_$_;\n".
			  "\tdbus_message_iter_get_basic(iter, &arg_$_);\n"
			: "\tDBusMessageIter arg_$_ = *iter;\n"
		} 0..$#t).
		"\t((".method_type($sig, 'void', '').")func)(obj, msg".
			join('', map ", ".($t[$_] eq 'v' ? '&' : '')."arg_$_", 0..$#t).
			");\n".
		"}\n";
}


# Determines whether we can create a reply/signal function for the given normalized signature.
sub repsig_can {
	return shift !~ /v/;
}


# Create the function declaration for the reply/signal function with the given normalized signature.
sub repsig_cdecl {
	my $sig = shift;
	my @t = sig_split $sig;
	return "void dbo_repsig_".sig_cname($sig)."(DBusMessage *msg".
		join('', map ', '.sig_ctypec($t[$_])." arg_$_", 0..$#t).")";
}


# Create the implementation for the reply/signal function with the given normalized signature.
# TODO: Better support for non-basic types.
# XXX: These also hardcode the use of the global 'dbuscon'.
sub repsig_create {
	my $sig = shift;
	my @t = sig_split $sig;
	return repsig_cdecl($sig)." {\n".
		join('', map "\tdbus_bool_t argb_$_ = arg_$_;\n", grep $t[$_] eq 'b', 0..$#t).
		(!@t ? '' :
			"\tdbus_message_append_args(msg".
			join('', map ", (int)'$t[$_]', &arg".($t[$_]eq'b'?'b':'')."_$_", 0..$#t).
			", DBUS_TYPE_INVALID);\n").
		"\tdbo_sendfree(msg);\n".
		"}\n";
}


# Create a _reply() or _signal() function (if possible for the signature)
sub repsig_func {
	my($itf, $m, $reply) = @_;
	my $a = $m->{ $reply ? 'out' : 'args' };
	my $sig = sig_normalize(arg_sig $a);
	return '' if !repsig_can $sig;
	my @t = sig_split($sig);

	my $fargs = join '', map ", ".sig_ctypec($t[$_])." $a->[$_][1]", 0..@t-1;
	my $cargs = join '', map ", $a->[$_][1]", 0..@t-1;
	return "static inline void dbo_${itf}_$m->{name}_" . ($reply
		? "reply(DBusMessage *_msg$fargs) {\n"
		 ."\tdbo_repsig_".sig_cname($sig)."(dbus_message_new_method_return(_msg)$cargs);\n"
		: "signal(void *_obj$fargs) {\n"
		 ."\tdbo_repsig_".sig_cname($sig)."(dbus_message_new_signal(((dbo_t *)_obj)->path, \"$itf{$itf}{name}\", \"$m->{name}\")$cargs);\n"
		)."}\n";
};


# Returns the properties struct declaration for the given interface.
sub prop_struct {
	my $itf = shift;
	return "typedef struct {\n".
		join('', map "\t".sig_ctype($_->{type})." $_->{name};\n", @{$itf{$itf}{properties}}).
		"} dbo_${itf}_properties_t;\n";
}


sub prop_getter {
	my($itf, $type, $name) = @_;
	my $val = "((dbo_${itf}_properties_t *)tbl)->$name";
	return "static void ${itf}_get_${name}(void *tbl, DBusMessageIter *iter) {\n".
		($type eq 'b' ?
			"\tdbus_bool_t val = $val;\n".
			"\tdbus_message_iter_append_basic(iter, DBUS_TYPE_BOOLEAN, &val);\n" :
			"\tdbus_message_iter_append_basic(iter, (int)'$type', &($val));\n").
		"}\n";
}


# The _set_noemit(), _set(), _add_batch(), _set_batch(), _setreply() and DBO_*_SETTER() functions/macros.
sub prop_setter {
	my($itf, $i) = @_;
	local $_ = $itf{$itf}{properties}[$i];
	my $r = '';
	$r .= "static inline bool dbo_${itf}_$_->{name}_set_noemit(dbo_${itf}_properties_t *p, ".sig_ctypec($_->{type})." val) {\n";
	$r .= "\tif(".sig_eq($_->{type}, "p->$_->{name}", 'val').")\n";
	$r .= "\t\treturn false;\n";
	$r .= $_->{type} eq 's'
		? "\tfree(p->$_->{name});\n\tp->$_->{name} = strdup(val);\n"
		: "\tp->$_->{name} = val;\n";
	$r .= "\treturn true;\n";
	$r .= "}\n";

	$r .= "static inline bool dbo_${itf}_$_->{name}_set(void *obj, dbo_${itf}_properties_t *p, ".sig_ctypec($_->{type})." val) {\n";
	if($_->{flags} =~ /emitnone/) {
		$r .= "\treturn dbo_${itf}_$_->{name}_set_noemit(p, val);\n";
	} else {
		$r .= "\tbool changed = dbo_${itf}_$_->{name}_set_noemit(p, val);\n";
		$r .= "\tdbo_prop_changed((dbo_prop_changed_t){changed,{$i}}, obj, &dbo_${itf}_interface, ".
		      ($_->{flags} =~ /emitchange/ ? 'NULL' : 'p').");\n";
		$r .= "\treturn changed;\n";
	}
	$r .= "}\n";

	$r .= "static inline void dbo_${itf}_$_->{name}_add_batch(dbo_prop_changed_t *props) {\n";
	$r .= "\tprops->l[props->n++] = $i;\n";
	$r .= "}\n";

	$r .= "static inline bool dbo_${itf}_$_->{name}_set_batch(".
	      "dbo_prop_changed_t *props, dbo_${itf}_properties_t *p, ".sig_ctypec($_->{type})." val) {\n";
	$r .= "\tbool changed = dbo_${itf}_$_->{name}_set_noemit(p, val);\n";
	$r .= "\tif(changed)\n";
	$r .= "\t\tdbo_${itf}_$_->{name}_add_batch(props);\n";
	$r .= "\treturn changed;\n";
	$r .= "}\n";

	if($_->{access} =~ /w/) {
		$r .= "static inline bool dbo_${itf}_$_->{name}_setreply(void *obj, dbo_${itf}_properties_t *p, DBusMessage *msg, ".sig_ctypec($_->{type})." val) {\n";
		$r .= "\tdbo_prop_reply(msg);\n";
		$r .= "\treturn dbo_${itf}_$_->{name}_set(obj, p, val);\n";
		$r .= "}\n";
		$r .= "#define DBO_\U${itf}\E_$_->{name}_SETTER(prefix, obj_type, tbl_name)\\\n";
		$r .= "\tvoid prefix##set_$_->{name}(obj_type *obj, DBusMessage *msg, ".sig_ctypec($_->{type})." val) {\\\n";
		$r .= "\t\tdbo_${itf}_$_->{name}_setreply(obj, &(((obj_type *)obj)->tbl_name), msg, val);\\\n";
		$r .= "\t}\n";
	}
	return $r;
}


sub get_sigs {
	my $methods = shift; # true = method+property signatures, false = signal+reply signatures
	my %sigs;
	for (keys %itf) {
		$sigs{ sig_normalize(arg_sig $_) } = 1 for(
			$methods
			? map $_->{in}, @{$itf{$_}{methods}}
			: ((map $_->{out}, @{$itf{$_}{methods}}), map $_->{args}, @{$itf{$_}{signals}})
		);
	}
	# No need to normalize property signatures, we simply can't handle unsupported types.
	$sigs{$_} = 1 for ($methods ? map $_->{type}, grep $_->{access}=~/w/, map @{$itf{$_}{properties}}, keys %itf : ());
	return keys %sigs;
}


# Returns the C formatted flags for a method/signal/property.
sub cflags {
	my($a, $f) = ($_[0]{access}||'', $_[0]{flags}||'');
	return join '|',
		!$a ? 0 : $a eq 'r' ? 'DBO_READ' : $a eq 'w' ? 'DBO_WRITE' : 'DBO_READWRITE',
		map $f =~/$_/?("DBO_\U$_"):(), qw|deprecated noreply emitnone emitchange|;
}


# Turns a list of [ [ a, b ], [ c, d ] ] into "a\0b\0c\0d\0"
sub argl {
	my $a = shift;
	return !@$a ? 'NULL' : '"'.join('\0', map @$_, @$a).'\0"';
};


sub write_c_itf {
	my($F, $itf) = @_;

	# Property getters
	print $F prop_getter $itf, $_->{type}, $_->{name} for grep $_->{access} =~ /r/, @{$itf{$itf}{properties}};
	print $F "\n";

	# Method list
	printf $F "static const dbo_method_t %s_methods[] = {", $itf;
	print  $F join ",", map sprintf("\n\t".'{"%s", %s, %s, %s, wrapper_%s}',
		$_->{name}, cflags($_), argl($_->{in}), argl($_->{out}), sig_normalize(arg_sig $_->{in})
	), @{$itf{$itf}{methods}};
	print  $F "\n};\n";

	# Signal list
	printf $F "static const dbo_signal_t %s_signals[] = {", $itf;
	print  $F join ",", map sprintf("\n\t".'{"%s", %s, %s}',
		$_->{name}, cflags($_), argl($_->{args})
	), @{$itf{$itf}{signals}};
	print  $F "\n};\n";

	# Properties
	if(@{$itf{$itf}{properties}}) {
		printf $F "static const dbo_property_t %s_properties[] = {", $itf;
		print  $F join ",", map sprintf("\n\t".'{"%s", "%s", %s, %s, %s}',
			$_->{name}, $_->{type}, cflags($_),
			$_->{access} =~ /r/ ? "${itf}_get_$_->{name}" : 'NULL',
			$_->{access} =~ /w/ ? 'wrapper_'.sig_cname($_->{type}) : 'NULL'
		), @{$itf{$itf}{properties}};
		print  $F "\n};\n";
	}

	# The interface object
	printf $F 'const dbo_interface_t dbo_%s_interface = {"%s", %d, %d, %d, %1$s_methods, %1$s_signals, %s};'."\n",
		$itf, $itf{$itf}{name}, map((scalar @{$itf{$itf}{$_}}), qw|methods signals properties|),
		@{$itf{$itf}{properties}} ? "${itf}_properties" : 'NULL';
}


sub write_c {
	my $cf = shift;

	open(my $F, '>', "$cf") or die "Writing to $cf: $!\n";
	print $F "/* File automatically generated by itf2h.pl, DO NOT EDIT. */\n\n";
	print $F "#include \"global.h\"\n";

	print $F "/* Wrapper functions */\n\n";
	print $F wrapper_create $_ for (get_sigs 1);
	print $F "\n\n\n";
	print $F "/* Reply + signal functions */\n\n";
	print $F repsig_create $_ for (grep repsig_can($_), get_sigs);
	print $F "\n\n";

	for my $itf (keys %itf) {
		print $F "\n\n/* $itf -> $itf{$itf}{name} */\n\n";
		write_c_itf $F, $itf;
	}
	close $F or die "Writing to $cf: $!\n";
}


sub write_h_itf {
	my($F, $itf) = @_;

	# Interface object
	printf $F "extern const dbo_interface_t dbo_${itf}_interface;\n";

	# vtable
	print $F "#define dbo_${itf}_vtable_t(_obj_type)\\\n";
	print $F "\tstruct {\\";
	print $F "\n\t\t".method_type(sig_normalize(arg_sig $_->{in}), "_obj_type", $_->{name}).";\\" for @{$itf{$itf}{methods}};
	print $F "\n\t\t".method_type($_->{type}, "_obj_type", "set_$_->{name}").";\\" for grep $_->{access}=~/w/, @{$itf{$itf}{properties}};
	print $F "\n\t}\n";
	print $F "#define dbo_${itf}_funcs(prefix) {". join(', ',
		(map "prefix##$_->{name}", @{$itf{$itf}{methods}}),
		(map "prefix##set_$_->{name}", grep $_->{access}=~/w/, @{$itf{$itf}{properties}})
	)."}\n";

	# Reply and signal macros
	print $F repsig_func($itf, $_, 1) for(@{$itf{$itf}{methods}});
	print $F repsig_func($itf, $_, 0) for(@{$itf{$itf}{signals}});

	# Properties
	print $F prop_struct $itf if @{$itf{$itf}{properties}};
	# Free function
	my @strings = grep $_->{type} eq 's', @{$itf{$itf}{properties}};
	if(@strings) {
		print $F "static inline void dbo_${itf}_properties_free(dbo_${itf}_properties_t *p) {\n";
		print $F "\tfree(p->$_->{name});\n" for @strings;
		print $F "}\n";
	}
	# Property setter
	print $F prop_setter($itf, $_) for(0..$#{$itf{$itf}{properties}});

	print $F "\n\n";
}


# TODO: All interfaces are currently combined into a single header file.
# Splitting them up into one header files for each interface will prevent
# recompilation of the entire project if only that interface has changed.
sub write_h {
	my $cf = shift;

	open(my $F, '>', "$cf~") or die "Writing to $cf~: $!\n";
	print $F "/* File automatically generated by itf2h.pl, DO NOT EDIT. */\n\n";

	print $F "#ifndef ITF_H\n";
	print $F "#define ITF_H\n\n\n";

	print $F "/* Reply + signal functions */\n\n";
	print $F repsig_cdecl($_).";\n" for (grep repsig_can($_), get_sigs);
	print $F "\n\n";

	for my $itf (keys %itf) {
		print $F "\n\n/* $itf -> $itf{$itf}{name} */\n\n";
		write_h_itf $F, $itf;
	}
	print $F "#endif\n";

	close $F or die "Writing to $cf~: $!\n";
	if(!-e $cf or Digest::SHA->new(256)->addfile("$cf~")->digest() ne Digest::SHA->new(256)->addfile("$cf")->digest()) {
		rename "$cf~", $cf or die "Renaming $cf~ to $cf: $!\n";
	} else {
		unlink "$cf~";
	}
}


read_itf;
for(@ARGV) {
	write_c($_) if /\.c$/;
	write_h($_) if /\.h$/;
}

# vim: set noet sw=4 ts=4:
