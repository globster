/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <global.h>
#include <hub/local.h>


__KHASH_IMPL(chatgroups, static kh_inline, int64_t, hub_chat_group_t, 1, kh_int64_hash_func, kh_int64_hash_equal);


/* Ref/unref a user/group ID */
static void user_ref(hub_t *h, int64_t user) {
	hub_user_t *u = user < 0 ? NULL : hub_users_get(h, user);
	if(u) hub_user_ref(u);
}

static void user_unref(hub_t *h, int64_t user) {
	hub_user_t *u = user < 0 ? NULL : hub_users_get(h, user);
	if(u) hub_user_unref(h, u);
}


/* Remove the last item from the group log (without reading it) */
static void glog_pop(hub_t *h, hub_chat_group_t *g) {
	assert(g->n > 0);
	hub_chat_msg_t *m = g->tail;
	list_remove(*g, m);
	g->n--;
	user_unref(h, m->user);
	free(m);
}


/* Ensure that the number of log messages doesn't exceed n. Also removes the group if n == 0. */
static void glog_fit(hub_t *h, khiter_t k, int n) {
	if(k == kh_end(h->ch.groups))
		return;
	hub_chat_group_t *g = &kh_val(h->ch.groups, k);
	while(g->n > n)
		glog_pop(h, g);
	if(!n) {
		user_unref(h, kh_key(h->ch.groups, k));
		kh_del(chatgroups, h->ch.groups, k);
	}
}


static void SendChat(hub_t *h, DBusMessage *msg, int64_t group, const char *message, bool me) {
	hub_user_t *u = group < 0 ? NULL : hub_users_get(h, group);
	if(group >= 0 && !u)
		dbo_sendfree(dbus_message_new_error_printf(msg, DBUS_ERROR_INVALID_ARGS, "No such group"));
	else if(!h->proto->chat(h, u, message, me))
		dbo_sendfree(dbus_message_new_error_printf(msg, DBUS_ERROR_INVALID_ARGS, "Not logged in"));
	else
		dbo_hubch_SendChat_reply(msg);
}


static void ChatLog(hub_t *h, DBusMessage *msg, int64_t group, uint32_t number, double maxage) {
	DBusMessage *r = dbus_message_new_method_return(msg);
	DBusMessageIter iter, sub, item;
	dbus_message_iter_init_append(r, &iter);
	dbus_message_iter_open_container(&iter, DBUS_TYPE_ARRAY, "(dxsb)", &sub);

	khiter_t k = kh_get(chatgroups, h->ch.groups, group);
	if(k == kh_end(h->ch.groups))
		goto done;

	ev_tstamp tm = ev_now(EV_DEFAULT);
	uint32_t n = number;
	hub_chat_group_t *g = &kh_val(h->ch.groups, k);

	hub_chat_msg_t *m;
	for(m=g->head; m; m=m->next) {
		double age = tm-m->tm;
		if(!n-- || age > maxage)
			break;
		dbus_message_iter_open_container(&sub, DBUS_TYPE_STRUCT, NULL, &item);
		dbus_message_iter_append_basic(&item, DBUS_TYPE_DOUBLE, &age);
		dbus_message_iter_append_basic(&item, DBUS_TYPE_INT64, &m->user);
		const char *message = m->msg;
		dbus_message_iter_append_basic(&item, DBUS_TYPE_STRING, &message);
		dbus_bool_t me = m->me;
		dbus_message_iter_append_basic(&item, DBUS_TYPE_BOOLEAN, &me);
		dbus_message_iter_close_container(&sub, &item);
	}

done:
	dbus_message_iter_close_container(&iter, &sub);
	dbo_sendfree(r);
}


static void ChatGroups(hub_t *h, DBusMessage *msg) {
	DBusMessage *r = dbus_message_new_method_return(msg);
	DBusMessageIter iter, sub;
	dbus_message_iter_init_append(r, &iter);
	dbus_message_iter_open_container(&iter, DBUS_TYPE_ARRAY, "x", &sub);
	khiter_t k;
	for(k=kh_begin(h->ch.groups); k!=kh_end(h->ch.groups); k++)
		if(kh_exist(h->ch.groups, k))
			dbus_message_iter_append_basic(&sub, DBUS_TYPE_INT64, &kh_key(h->ch.groups, k));
	dbus_message_iter_close_container(&iter, &sub);
	dbo_sendfree(r);
}


static void ClearLog(hub_t *h, DBusMessage *msg, int64_t group, uint32_t keep) {
	khiter_t k = kh_get(chatgroups, h->ch.groups, group);
	if(k != kh_end(h->ch.groups))
		glog_fit(h, k, keep);
	dbo_hubch_ClearLog_reply(msg);
}


static void set_ChatLogSize(hub_t *h, DBusMessage *msg, uint32_t val) {
	if(val >= INT32_MAX/2) {
		dbo_sendfree(dbus_message_new_error(msg, DBUS_ERROR_INVALID_ARGS, "Too large value for ChatLogSize"));
		return;
	}
	khiter_t k;
	for(k=kh_begin(h->ch.groups); k!=kh_end(h->ch.groups); k++)
		if(kh_exist(h->ch.groups, k))
			glog_fit(h, k, val);
	dbo_hubch_ChatLogSize_setreply(h, &h->ch.props, msg, val);
}


void hub_chat_message(hub_t *h, int64_t group, int64_t from, const char *message, bool me) {
	dbo_hubch_ReceiveChat_signal(h, group, from, message, me);
	if(!h->ch.props.ChatLogSize)
		return;

	int r = 0;
	khiter_t k = kh_put(chatgroups, h->ch.groups, group, &r);
	hub_chat_group_t *g = &kh_val(h->ch.groups, k);
	if(r) {
		g->head = g->tail = NULL;
		g->n = 0;
		user_ref(h, group);
	}

	hub_chat_msg_t *m = malloc(offsetof(hub_chat_msg_t, msg) + strlen(message) + 1);
	m->tm = ev_now(EV_DEFAULT);
	m->user = from;
	m->me = me;
	strcpy(m->msg, message);

	user_ref(h, from);
	list_insert_before(*g, m, g->head);
	g->n++;
	if((uint32_t)g->n > h->ch.props.ChatLogSize)
		glog_pop(h, g);
}


void hub_chat_init(hub_t *h) {
	h->ch.props.ChatLogSize = 0;
	h->ch.groups = kh_init(chatgroups);
}


void hub_chat_destroy(hub_t *h) {
	khiter_t k;
	for(k=kh_begin(h->ch.groups); k!=kh_end(h->ch.groups); k++)
		if(kh_exist(h->ch.groups, k))
			glog_fit(h, k, 0);
	kh_destroy(chatgroups, h->ch.groups);
}

const hub_chat_vt_t hub_chat_vt = dbo_hubch_funcs();

/* vim: set noet sw=4 ts=4: */
