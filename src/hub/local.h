/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef HUB_LOCAL_H
#define HUB_LOCAL_H

typedef struct hub_t hub_t;
typedef struct hub_proto_t hub_proto_t;


#include <hub/adc.h>
#include <hub/chat.h>
#include <hub/connection.h>
#include <hub/hub.h>
#include <hub/manager.h>
#include <hub/nmdc.h>
#include <hub/users.h>


/* Protocol vtable. Functions that adc.c and nmdc.c need to implement */
struct hub_proto_t {
	/* _connected() and _disconnected() get called from connection.c when
	 * moving into or out of the CONN state. */
	void (*connected)(hub_t *h);
	void (*disconnected)(hub_t *h);

	/* Called from hub.c when new user info has to be pushed to the hub. Will
	 * only be called while logged in to the hub. */
	void (*sendinf)(hub_t *h);

	/* Periodically called from hub.c while logged in to the hub. Should send a
	 * keepalive message. */
	void (*sendkeepalive)(hub_t *h);

	/* Send a chat message to the specified user, or to the main chat if it's
	 * NULL. Returns false if we're not logged in. */
	bool (*chat)(hub_t *h, hub_user_t *u, const char *message, bool me);

	/* Called from connection.c when some new data has been read from the socket.
	 * This function must consume all complete commands in the buffer, and returns
	 * the number of bytes consumed. */
	int  (*read)(hub_t *h, const char *buf, int oldlen, int len);
};


struct hub_t {
	dbo_t dbo;
	int id;
	hub_manager_type_t type;
	const hub_proto_t *proto;
	hub_hub_t h;
	hub_conn_t c;
	union {
		hub_adc_t a;
		hub_nmdc_t n;
	};
	hub_users_t u;
	hub_chat_t ch;
};

#endif
/* vim: set noet sw=4 ts=4: */
