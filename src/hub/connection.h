/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef HUB_CONNECTION_H
#define HUB_CONNECTION_H

/* These are visible on D-Bus */
typedef enum { HUBC_IDLE, HUBC_BUSY, HUBC_CONN, HUBC_DISC, HUBC_TIMER } hub_conn_state_t;

/* These aren't. (HUBCD_NO is used for hub_hub_seterror()) */
typedef enum { HUBCD_FORCE = 1, HUBCD_RECONNECT = 2, HUBCD_NO = 4 } hub_conn_flags_t;


typedef struct {
	dbo_hubc_properties_t props;
	uint16_t conntimeout;
	bool wantreconnect : 1; /* Used in DISC */
	bool istls : 1;
	/* TIMER / DISC */
	ev_timer timer;
	/* BUSY */
	DBusMessage *cmsg;
	net_connect_t *con;
	/* CONN / DISC */
	nets_t *n;
	char *buf;
	size_t buflen, bufsize;
} hub_conn_t;


#define hub_conn_state(_h) ((hub_conn_state_t)((_h)->c.props.ConnectState))


/* Called when moving from any state to the DISC, IDLE, or TIMER state.
 * The HUBCD_FORCE flag only makes sense in the CONN state. All other states
 * directly move to IDLE or TIMER. */
void hub_conn_disconnect(hub_t *h, hub_conn_flags_t flags, const char *reason, const char *message);


/* Called shortly after hub_conn_init() if global autoconnect is enabled. */
void hub_conn_autoconnect(hub_t *h);

void hub_conn_init(hub_t *h, const char *addr, bool autoconnect, uint16_t timeout);
void hub_conn_destroy(hub_t *h);

typedef dbo_hubc_vtable_t(hub_t) hub_conn_vt_t;
extern const hub_conn_vt_t hub_conn_vt;

#endif
/* vim: set noet sw=4 ts=4: */
