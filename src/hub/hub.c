/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <global.h>
#include <hub/local.h>


static void set_MyNick(hub_t *h, DBusMessage *msg, const char *val) {
	if(!*val) {
		dbo_sendfree(dbus_message_new_error(msg, DBUS_ERROR_INVALID_ARGS, "Nick name too short"));
		return;
	}
	if(strlen(val) > 32) {
		dbo_sendfree(dbus_message_new_error(msg, DBUS_ERROR_INVALID_ARGS, "Nick name too long"));
		return;
	}

	int i;
	for(i=strlen(val)-1; i>=0; i--)
		if(val[i] == '$' || val[i] == '|' || val[i] == ' ' || val[i] == '<' || val[i] == '>')
			break;
	if(i >= 0) {
		dbo_sendfree(dbus_message_new_error(msg, DBUS_ERROR_INVALID_ARGS, "Invalid character in nick name"));
		return;
	}

	hub_hub_setvar(h, "MyNick", sqlasync_text(SQLASYNC_COPY, val));
	dbo_hub_MyNick_setreply(h, &h->h.props, msg, val);
	hub_hub_infchange(h, true);
}


static void set_MyDescription(hub_t *h, DBusMessage *msg, const char *val) {
	hub_hub_setvar(h, "MyDescription", *val ? sqlasync_text(SQLASYNC_COPY, val) : sqlasync_null());
	dbo_hub_MyDescription_setreply(h, &h->h.props, msg, val);
	hub_hub_infchange(h, true);
}


static void set_MyEmail(hub_t *h, DBusMessage *msg, const char *val) {
	hub_hub_setvar(h, "MyEmail", *val ? sqlasync_text(SQLASYNC_COPY, val) : sqlasync_null());
	dbo_hub_MyEmail_setreply(h, &h->h.props, msg, val);
	hub_hub_infchange(h, true);
}


static void set_MyConnection(hub_t *h, DBusMessage *msg, uint32_t val) {
	hub_hub_setvar(h, "MyConnection", val ? sqlasync_int(val) : sqlasync_null());
	dbo_hub_MyConnection_setreply(h, &h->h.props, msg, val);
	hub_hub_infchange(h, true);
}


static DBO_HUB_MyPassword_SETTER(, hub_t, h.props);


static void set_HubEncoding(hub_t *h, DBusMessage *msg, const char *val) {
	if(!nmdc_convert_check(val))
		dbo_sendfree(dbus_message_new_error(msg, DBUS_ERROR_INVALID_ARGS, "Unsupported character encoding"));
	else {
		hub_hub_setvar(h, "HubEncoding",
			strcmp(val, "UTF-8") != 0 ? sqlasync_text(SQLASYNC_COPY, val) : sqlasync_null());
		dbo_hub_HubEncoding_setreply(h, &h->h.props, msg, val);
	}
}


static void hub_hub_nfo_timeout(EV_P_ ev_timer *w, int revents) {
	hub_t *h = w->data;
	ydebug("%d: Sending user info", h->id);
	h->proto->sendinf(h);
}


static void hub_hub_keepalive_timeout(EV_P_ ev_timer *w, int revents) {
	hub_t *h = w->data;
	h->proto->sendkeepalive(h);
}


static void hub_hub_login_timeout(EV_P_ ev_timer *w, int revents) {
	hub_t *h = w->data;
	assert(!h->h.loggedin);
	yerror("%d: Login timed out", h->id);
	hub_conn_disconnect(h, HUBCD_RECONNECT, "net.blicky.Globster.Error.LoginTimeout", "Login timed out");
}


void hub_hub_loggedin(hub_t *h) {
	yinfo("%d: Successfully logged into the hub", h->id);
	h->h.loggedin = true;
	if(h->h.infchanged) {
		h->h.infchanged = false;
		hub_hub_infchange(h, h->h.infimportant);
	}
	ev_timer_stop(EV_DEFAULT_UC_ &h->h.timer);
	ev_timer_init(&h->h.timer, hub_hub_keepalive_timeout, 0.0, 120.0);
	ev_timer_start(EV_DEFAULT_UC_ &h->h.timer);
}


void hub_hub_infchange(hub_t *h, bool important) {
	/* This may get called even when we're not connected. Don't bother to queue
	 * an info change in that case, since a full info message will be sent
	 * whenever we initiate a new login, anyway. */
	if(hub_conn_state(h) != HUBC_CONN)
		return;

	ydebug("%d: User info changed, queueing infchange", h->id);
	/* Delay the info change for after we've logged in */
	if(!h->h.loggedin) {
		h->h.infchanged = true;
		h->h.infimportant = h->h.infimportant || important;
		return;
	}
	/* NMDC hubs tend to be more picky about $MyINFO throttling, so the timeout
	 * is a bit higher for those. ADC also benefits a lot from incremental
	 * updates. */
	double timeout = important ? 1.0 : h->proto == &hub_adc ? 60.0 : 120.0;
	if(!ev_is_active(&h->h.infdelay)) {
		ev_timer_init(&h->h.infdelay, hub_hub_nfo_timeout, timeout, 0.0);
		ev_timer_start(EV_DEFAULT_UC_ &h->h.infdelay);
	} else if(ev_timer_remaining(EV_DEFAULT_UC_ &h->h.infdelay) > timeout) {
		ev_timer_stop(EV_DEFAULT_UC_ &h->h.infdelay);
		ev_timer_set(&h->h.infdelay, timeout, 0.0);
		ev_timer_start(EV_DEFAULT_UC_ &h->h.infdelay);
	}
}


void hub_hub_seterror(hub_t *h, hub_conn_flags_t disconnectflags, const char *err, const char *msg, const char *redir) {
	dbo_prop_changed_t props = {};
	dbo_hub_HubError_set_batch(&props, &h->h.props, err);
	dbo_hub_HubErrorMessage_set_batch(&props, &h->h.props, msg);
	if(redir)
		dbo_hub_HubRedirect_set_batch(&props, &h->h.props, redir);
	dbo_prop_changed(props, h, &dbo_hub_interface, &h->h.props);

	if(!(disconnectflags & HUBCD_NO))
		hub_conn_disconnect(h, disconnectflags, "net.blicky.Globster.Error.Hub", msg);
}


void hub_hub_setvar(hub_t *h, const char *name, sqlasync_value_t val) {
	if(val.type == SQLITE_NULL)
		sqlasync_sql(db_sql, NULL, SQLASYNC_STATIC,
			"DELETE FROM hub_vars WHERE hub = ? AND name = ?",
			2, sqlasync_int(h->id), sqlasync_text(SQLASYNC_STATIC, name));
	else
		sqlasync_sql(db_sql, NULL, SQLASYNC_STATIC,
			"INSERT OR REPLACE INTO hub_vars (hub, name, value) VALUES (?,?,?)",
			3, sqlasync_int(h->id), sqlasync_text(SQLASYNC_STATIC, name), val);
}


void hub_hub_connected(hub_t *h) {
	h->h.loggedin = h->h.infchanged = h->h.infimportant = false;

	dbo_hub_HubError_set_noemit(&h->h.props, "");
	dbo_hub_HubErrorMessage_set_noemit(&h->h.props, "");
	dbo_hub_HubRedirect_set_noemit(&h->h.props, "");

	ev_timer_init(&h->h.timer, hub_hub_login_timeout, 60.0, 0.0);
	h->h.timer.data = h;
	ev_timer_start(EV_DEFAULT_UC_ &h->h.timer);

	ev_timer_init(&h->h.infdelay, hub_hub_nfo_timeout, 0.0, 0.0);
	h->h.infdelay.data = h;
}


void hub_hub_disconnected(hub_t *h) {
	dbo_hub_HubName_set_noemit(&(h)->h.props, "");
	dbo_hub_HubDescription_set_noemit(&(h)->h.props, "");
	dbo_hub_HubVersion_set_noemit(&(h)->h.props, "");
	h->h.loggedin = false;

	ev_timer_stop(EV_DEFAULT_UC_ &h->h.timer);
	ev_timer_stop(EV_DEFAULT_UC_ &h->h.infdelay);
}



void hub_hub_init(hub_t *h, const char *nick, const char *desc, const char *mail, uint32_t conn, const char *encoding) {
	if(nick)
		h->h.props.MyNick = strdup(nick);
	else {
		h->h.props.MyNick = malloc(20);
		snprintf(h->h.props.MyNick, 19, "Mr.%05d", rand() % 100000);
		hub_hub_setvar(h, "MyNick", sqlasync_text(SQLASYNC_COPY, h->h.props.MyNick));
	}

	h->h.props.MyDescription = strdup(desc);
	h->h.props.MyEmail = strdup(mail);
	h->h.props.MyConnection = conn;
	h->h.props.MyPassword = strdup("");

	h->h.props.HubEncoding = strdup(encoding);

	h->h.props.HubName = strdup("");
	h->h.props.HubDescription = strdup("");
	h->h.props.HubVersion = strdup("");

	h->h.props.HubError = strdup("");
	h->h.props.HubErrorMessage = strdup("");
	h->h.props.HubRedirect = strdup("");
}


void hub_hub_destroy(hub_t *h) {
	dbo_hub_properties_free(&h->h.props);
}


const hub_hub_vt_t hub_hub_vt = dbo_hub_funcs();


/* vim: set noet sw=4 ts=4: */
