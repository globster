/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef HUB_ADC_H
#define HUB_ADC_H

/* A cache of the last INF values we sent to the hub. Used to ensure that we
 * only send changed values. */
typedef struct {
	uint16_t HN;
	uint16_t HR;
	uint16_t HO;
	uint32_t US;
	char *NI;
	char *DE;
	char *EM;
} hub_adc_myinf_t;


/* SID -> uid lookup table */
__KHASH_TYPE(adcsids, adc_sid_t, int64_t);


typedef struct {
	adc_state_t state;
	adc_sid_t mysid;
	hub_adc_myinf_t lastinf;
	kh_adcsids_t *sids;
	bool havelastinf : 1;
	bool zlibenable : 1;
} hub_adc_t;

extern const hub_proto_t hub_adc;

#endif
/* vim: set noet sw=4 ts=4: */
