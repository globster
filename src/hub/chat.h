/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef HUB_CHAT_H
#define HUB_CHAT_H

typedef struct hub_chat_msg_t hub_chat_msg_t;

struct hub_chat_msg_t {
	hub_chat_msg_t *next, *prev;
	ev_tstamp tm;
	int64_t user;
	bool me;
	char msg[];
};


typedef struct {
	hub_chat_msg_t *head, *tail;
	int n;
} hub_chat_group_t;


__KHASH_TYPE(chatgroups, int64_t, hub_chat_group_t);


typedef struct {
	dbo_hubch_properties_t props;
	kh_chatgroups_t *groups;
} hub_chat_t;


/* Called from hub/proto when we've received a message */
void hub_chat_message(hub_t *h, int64_t group, int64_t from, const char *message, bool me);

void hub_chat_init(hub_t *h);
void hub_chat_destroy(hub_t *h);

typedef dbo_hubch_vtable_t(hub_t) hub_chat_vt_t;
extern const hub_chat_vt_t hub_chat_vt;

#endif
/* vim: set noet sw=4 ts=4: */
