/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef HUB_NMDC_H
#define HUB_NMDC_H


typedef enum { HUBN_LOCK = 1, HUBN_IDENTIFY = 2, HUBN_VERIFY = 4, HUBN_NORMAL = 8 } hub_nmdc_state_t;


/* Some userinfo is received out-of-order with respect to $MyINFO messages,
 * such as the op flag ($OpList) and the IP ($UserIP).
 * If we receive that info before a $MyINFO (i.e. the user isn't officially
 * considered "online" yet), we have to buffer that information until we can
 * consider the user as "online". */
typedef struct {
	bool op;
	struct in_addr ip4;
	struct in6_addr ip6;
} hub_nmdc_defer_t;


/* uid -> defer lookup table. */
__KHASH_TYPE(nmdcdefer, int64_t, hub_nmdc_defer_t);


typedef struct {
	hub_nmdc_state_t state;
	char *lastinfo; /* Last $MyINFO we sent to the hub */
	char *nick;     /* Our nick, in the hub encoding */
	kh_nmdcdefer_t *defer;
	/* Timer used to detect when we (may) have the complete user list. */
	ev_timer listtimer;
	/* Short timeout to wait for a "is kicking" or $Quit message for a particular user */
	ev_timer quittimer;
	int64_t quituser;
	int64_t quitinitiator;
	char *quitmessage;
	bool listcomplete;
	bool isreg;
	bool nogetinfo;
} hub_nmdc_t;


extern const hub_proto_t hub_nmdc;

#endif
/* vim: set noet sw=4 ts=4: */
