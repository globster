/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <global.h>
#include <hub/local.h>

#define MANAGER_PATH "/net/blicky/Globster/HubManager"
#define HUB_BASE_PATH "/net/blicky/Globster/Hub"


typedef struct {
	dbo_t dbo;
	dbo_hubm_properties_t props;
	vec_t(hub_t *) hubs; /* Ordered by h->id */
} obj_t;

static obj_t manager;




#define LOADVALUES\
	V(AutoConnect)\
	V(ConnectAddress)\
	V(ReconnectTimeout)\
	V(MyNick)\
	V(MyDescription)\
	V(MyEmail)\
	V(MyConnection)\
	V(HubEncoding)\
	V(UserIDSalt)


/* Create a hub object and register it with dbo */
static hub_t *hub_create(int id, sqlasync_result_t *r) {
	static const dbo_reg_t regs[] = {
		{ &dbo_hub_interface,   (void**)&hub_hub_vt,   offsetof(hub_t, h.props)  },
		{ &dbo_hubc_interface,  (void**)&hub_conn_vt,  offsetof(hub_t, c.props)  },
		{ &dbo_hubu_interface,  (void**)&hub_users_vt, offsetof(hub_t, u.props)  },
		{ &dbo_hubch_interface, (void**)&hub_chat_vt,  offsetof(hub_t, ch.props) }
	};

	char path[50];
	assert(snprintf(path, sizeof(path), HUB_BASE_PATH"/%d", id) < (int)sizeof(path));
	yinfo("Creating hub object: %d", id);

	int colnum = 1;
#define V(n) sqlasync_value_t n = r ? r->col[colnum++] : sqlasync_null();
	LOADVALUES
#undef V

	hub_t *h = calloc(1, sizeof(hub_t));
	h->id = id;
	h->type = HUBM_HNONE;
	h->proto = &hub_adc;
	dbo_register(&h->dbo, path, regs, sizeof(regs)/sizeof(*regs));

#define INTVAL(v, min, max, def) (v.type == SQLITE_INTEGER && v.val.i64 >= min && v.val.i64 <= max ? v.val.i64 : def)

	hub_conn_init(h,
			ConnectAddress.type == SQLITE_TEXT ? ConnectAddress.val.ptr : "",
			INTVAL(AutoConnect, 0, 1, 0),
			INTVAL(ReconnectTimeout, 0, UINT16_MAX, 60)
	);
	hub_hub_init(h,
			MyNick.type        == SQLITE_TEXT ? MyNick.val.ptr        : NULL,
			MyDescription.type == SQLITE_TEXT ? MyDescription.val.ptr : "",
			MyEmail.type       == SQLITE_TEXT ? MyEmail.val.ptr       : "",
			INTVAL(MyConnection, 0, UINT32_MAX, 0),
			HubEncoding.type   == SQLITE_TEXT ? HubEncoding.val.ptr   : ""
	);
	hub_users_init(h, UserIDSalt.type == SQLITE_TEXT && isbase32_32(UserIDSalt.val.ptr) ? UserIDSalt.val.ptr : NULL);
	hub_chat_init(h);

#undef INTVAL

	return h;
}


static void hub_destroy(hub_t *h) {
	yinfo("Destroying hub object: %d", h->id);

	sqlasync_sql(db_sql, NULL, SQLASYNC_STATIC, "DELETE FROM hub_vars WHERE hub = ?", 1, sqlasync_int(h->id));

	hub_conn_destroy(h);
	hub_hub_destroy(h);
	hub_chat_destroy(h); /* chat before users, otherwise the _unref()s will fail */
	hub_users_destroy(h);
	dbo_unregister(&h->dbo);
	free(h);
}


static void Create(obj_t *o, DBusMessage *msg) {
	int id = manager.hubs.n > 0 ? manager.hubs.a[manager.hubs.n-1]->id+1 : 1;
	hub_t *h = hub_create(id, NULL);
	/* Always insert at the end. The above id selection guarantees that a newly
	 * created hub always has the highest id. */
	vec_insert_order(manager.hubs, manager.hubs.n, h);

	dbo_hubm_Create_reply(msg, id);
	dbo_hubm_Created_signal(o, id);
}


static void Delete(obj_t *o, DBusMessage *msg, uint32_t id) {
	size_t idx = (size_t)-1;
	vec_search(manager.hubs, manager.hubs.a[i]->id - id, idx=i);
	if(idx == (size_t)-1)
		dbo_sendfree(dbus_message_new_error_printf(msg, DBUS_ERROR_INVALID_ARGS, "Invalid hub identifier"));
	else {
		hub_destroy(manager.hubs.a[idx]);
		vec_remove_order(manager.hubs, idx);
		dbo_hubm_Delete_reply(msg);
		dbo_hubm_Deleted_signal(o, id);
	}
}


static void List(obj_t *o, DBusMessage *msg) {
	DBusMessageIter iter, sub;
	DBusMessage *r = dbus_message_new_method_return(msg);
	dbus_message_iter_init_append(r, &iter);
	dbus_message_iter_open_container(&iter, 'a', "u", &sub);
	size_t i;
	for(i=0; i<o->hubs.n; i++) {
		uint32_t n = o->hubs.a[i]->id;
		dbus_message_iter_append_basic(&sub, 'u', &n);
	}
	dbus_message_iter_close_container(&iter, &sub);
	dbo_sendfree(r);
}


static void hub_manager_infchange(EV_P_ ev_idle *w, int revents) {
	ev_idle_stop(EV_A_ w);
	size_t i;
	for(i=0; i<manager.hubs.n; i++)
		hub_hub_infchange(manager.hubs.a[i], false);
}


void hub_manager_settype(hub_t *h, hub_manager_type_t new) {
	uint16_t hn = manager.props.NormalHubs,
	         hr = manager.props.RegHubs,
	         ho = manager.props.OpHubs;
	static ev_idle idl = {};
	switch(h->type) {
	case HUBM_HNONE: break;
	case HUBM_HNORM: hn--; break;
	case HUBM_HREG:  hr--; break;
	case HUBM_HOP:   ho--; break;
	}
	switch(new) {
	case HUBM_HNONE: break;
	case HUBM_HNORM: hn++; break;
	case HUBM_HREG:  hr++; break;
	case HUBM_HOP:   ho++; break;
	}
	ydebug("Hub %d set type to %d, new counts are %"PRIu16"/%"PRIu16"/%"PRIu16, h->id, (int)new, hn, hr, ho);
	h->type = new;

	dbo_prop_changed_t props = {};
	dbo_hubm_NormalHubs_set_batch(&props, &manager.props, hn);
	dbo_hubm_RegHubs_set_batch(   &props, &manager.props, hr);
	dbo_hubm_OpHubs_set_batch(    &props, &manager.props, ho);
	dbo_prop_changed(props, h, &dbo_hubm_interface, &manager.props);

	/* Notify hubs of the infchange. This is done in the next iteration of the
	 * event loop, as this function may be called while the hub is being
	 * destroyed. Or rather, this function may be (indirectly) called from
	 * hub/hub.c and the notification goes back to hub/hub.c. Immediate
	 * callbacks are confusing. */
	if(!idl.data) {
		idl.data = &manager;
		ev_idle_init(&idl, hub_manager_infchange);
	}
	ev_idle_start(EV_DEFAULT_UC_ &idl);
}


uint16_t hub_manager_hnorm() { return manager.props.NormalHubs; }
uint16_t hub_manager_hreg()  { return manager.props.RegHubs; }
uint16_t hub_manager_hop()   { return manager.props.OpHubs; }


static void hub_manager_load() {
	/* Get everything we need with a single query. It looks kinda awkward, but
	 * the column trick makes processing easier.
	 * The UserIDSalt variable is used to obtain a listing of all available hub
	 * IDs. This variable is present for each hub, after all. */
	sqlasync_queue_t *q = sqlasync_sql(db_sql, sqlasync_queue_sync(), SQLASYNC_STATIC,
			"SELECT hub"
#define V(n) ", (SELECT l.value FROM hub_vars l WHERE l.hub = g.hub AND l.name = \""#n"\") "
			LOADVALUES
#undef V
			"FROM hub_vars g "
			"WHERE name = \"UserIDSalt\"", 0);

	sqlasync_result_t *r;
	while((r = db_get(q, "getting hub config")) && r->result == SQLITE_ROW) {
		int id = r->col[0].type == SQLITE_INTEGER && r->col[0].val.i64 > 0 && r->col[0].val.i64 < INT_MAX/2 ? r->col[0].val.i64 : -1;
		if(id > 0) {
			hub_t *h = hub_create(id, r);
			vec_insert_sorted(manager.hubs, manager.hubs.a[i]->id - id, h);
		}
		sqlasync_result_free(r);
	}
	sqlasync_result_free(r);
	sqlasync_queue_destroy(q);
}


void hub_manager_create(bool autoconnect) {
	static const dbo_hubm_vtable_t(obj_t) vt = dbo_hubm_funcs();
	static const dbo_reg_t reg = { &dbo_hubm_interface, (void**)&vt, offsetof(obj_t, props) };

	manager.props.NormalHubs = manager.props.RegHubs = manager.props.OpHubs = 0;

	dbo_register(&manager.dbo, MANAGER_PATH, &reg, 1);
	hub_manager_load();

	size_t i;
	if(autoconnect)
		for(i=0; i<manager.hubs.n; i++)
			hub_conn_autoconnect(manager.hubs.a[i]);
}


void hub_manager_shutdown() {
	/* Force disconnect all hubs. It'd be "cleaner" to do a graceful
	 * disconnect, but we shouldn't take too long to shut down... */
	size_t i;
	for(i=0; i<manager.hubs.n; i++) {
		hub_t *h = manager.hubs.a[i];
		hub_conn_disconnect(h, HUBCD_FORCE, "net.blicky.Globster.Disconnected", "Shutting down");
		hub_users_shutdown(h);
	}
}


/* vim: set noet sw=4 ts=4: */
