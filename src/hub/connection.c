/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <global.h>
#include <hub/local.h>


/* Maximum waiting time for a clean disconnect */
#define DISC_TIMEOUT 15

/* Read buffer parameters */
#define RDBUF_MAXLEN  (1024*1024) /* Maximum buffer size */
#define RDBUF_MINSIZE 512         /* Minimum size of a read */
/* Initial read buffer size = 2*RDBUF_MINSIZE,
 * Maximum message length = RDBUF_MAXLEN - RDBUF_MINSIZE */


static void hub_conn_destroycb(nets_t *n, int sock) {
	close(sock);
	free(nets_data_get(n));
}


static void hub_conn_synccb(nets_t *n, nets_sync_t direction) {
	if(!n)
		return;
	hub_t *h = nets_data_get(n);
	assert(hub_conn_state(h) == HUBC_DISC);
	hub_conn_disconnect(h, h->c.wantreconnect ? HUBCD_RECONNECT : 0, NULL, NULL);
}


static void hub_conn_errorcb(nets_t *n, nets_error_t err, int val) {
	hub_t *h = nets_data_get(n);
	const char *errmsg = nets_strerror(err, val);
	yerror("%d: Network error: %s (%s)", h->id, errmsg, nets_strerror_name(err));

	/* We can get a network error in the DISC state, make sure to honor
	 * wantreconnect in that case. */
	hub_conn_flags_t reconnectflag = hub_conn_state(h) == HUBC_CONN || h->c.wantreconnect ? HUBCD_RECONNECT : 0;
	const char *errcode = err == NETSE_TLS_HANDSHAKE ? "net.blicky.Globster.Error.TLSHandshake" : DBUS_ERROR_IO_ERROR;
	hub_conn_disconnect(h, HUBCD_FORCE|reconnectflag, errcode, errmsg);
}


static void hub_conn_readcb(nets_t *n, nets_read_t event, char *buffer, size_t length) {
	/* If the read has been cancelled, that means we're not in the CONN state
	 * anymore and we should stop doing any processing, even if length > 0. */
	if(!n || event & NETSR_CANCELLED)
		return;
	hub_t *h = nets_data_get(n);

	int consumed = h->proto->read(h, h->c.buf, h->c.buflen, h->c.buflen + length);
	/* proto->read() may have disconnected if there was an error. */
	if(hub_conn_state(h) != HUBC_CONN)
		return;

	h->c.buflen += length;
	if(consumed > 0) {
		memmove(h->c.buf, h->c.buf + consumed, h->c.buflen - consumed);
		h->c.buflen -= consumed;
	}

	if(h->c.bufsize - h->c.buflen < RDBUF_MINSIZE) {
		h->c.bufsize <<= 1;
		if(h->c.bufsize > RDBUF_MAXLEN) {
			yerror("%d: Read buffer overflow", h->id);
			hub_conn_disconnect(h, HUBCD_FORCE|HUBCD_RECONNECT, DBUS_ERROR_IO_ERROR, "Read buffer overflow");
			return;
		}
		h->c.buf = realloc(h->c.buf, h->c.bufsize);
	}

	if((event & NETSR_DISCONNECT) || (event & NETSR_TLS_BYE)) {
		/* If we've received an error message before this disconnect, then emit
		 * that instead of a generic network disconnect error. */
		const char *emsg = hub_hub_errormessage(h);
		hub_conn_disconnect(h, HUBCD_RECONNECT,
			*emsg ? "net.blicky.Globster.Error.Hub" : "net.blicky.Globster.Error.ConnectionReset",
			*emsg ? emsg : strerror(ECONNRESET));
	} else
		nets_read(h->c.n, h->c.buf + h->c.buflen, h->c.bufsize - h->c.buflen, hub_conn_readcb);
}


static void hub_conn_handshakecb(nets_t *n, gnutls_session_t ses) {
	if(!n)
		return;
	hub_t *h = nets_data_get(n);

	char kpraw[32];
	char kpb32[53];
	unsigned int len;
	const gnutls_datum_t *certs = gnutls_certificate_get_peers(ses, &len);
	assert(certs && len >= 1); /* It shouldn't be possible to handshake with a server that has no certificate */
	crypt_sha256(certs[0].data, certs[0].size, kpraw);
	base32_encode(kpraw, kpb32, 32);

	/* TODO: Validate keyprint with the ConnectAddress
	 * TODO: Export TLS certificate (chain) over D-Bus? */
	dbo_hubc_KeyprintSHA256_set(h, &h->c.props, kpb32);
	yinfo("%d: TLS handshake complete", h->id);
}


/* Initializes and moves to the CONN state */
static void hub_conn_start_conn(hub_t *h, int sock) {
	dbo_hubc_ConnectState_set(h, &h->c.props, HUBC_CONN);

	h->c.n = nets_create(sock, hub_conn_errorcb);
	nets_data_set(h->c.n, h);
	nets_rbuf_size(h->c.n, 2*RDBUF_MINSIZE);

	h->c.buflen = 0;
	h->c.bufsize = 2*RDBUF_MINSIZE;
	h->c.buf = malloc(h->c.bufsize);

	if(h->c.istls) {
		gnutls_session_t tls;
		const char *m;
		gnutls_init(&tls, GNUTLS_CLIENT);
		gnutls_priority_set_direct(tls, "NORMAL", &m);
		gnutls_credentials_set(tls, GNUTLS_CRD_CERTIFICATE, app_certificate());
		nets_tls_enable(h->c.n, tls, hub_conn_handshakecb);
	}

	hub_hub_connected(h);
	h->proto->connected(h);
	nets_read(h->c.n, h->c.buf, h->c.bufsize, hub_conn_readcb);
}


static void hub_conn_conncb(net_connect_t *c, net_connect_status_t status, int val, struct addrinfo *addr) {
	hub_t *h = c->data;

	char addrstr[MAX_ADDRSTRLEN];
	if(addr)
		net_sockaddr_str(addr->ai_family, addr->ai_addr, addrstr);

	if(status != NET_CONNECT_PROGRESS_DNS && status != NET_CONNECT_PROGRESS_CONNECT) {
		free(c);
		h->c.con = NULL;
	}

	switch(status) {
	case NET_CONNECT_PROGRESS_DNS:
		ydebug("%d: DNS resolution successful", h->id);
		return;
	case NET_CONNECT_PROGRESS_CONNECT:
		ydebug("%d: Attempting connection to %s", h->id, addrstr);
		dbo_hubc_ResolvedAddress_set(h, &h->c.props, addrstr);
		return;
	case NET_CONNECT_CANCELLED:
		ydebug("Connection cancelled.");
		break;
	case NET_CONNECT_ERROR_DNS:
		yinfo("%d: getaddrinfo failed: %s", h->id, gai_strerror(val));
		hub_conn_disconnect(h, HUBCD_RECONNECT, "net.blicky.Globster.Error.DNS", gai_strerror(val));
		break;
	case NET_CONNECT_ERROR_TIMEOUT:
		errno = ETIMEDOUT;
	case NET_CONNECT_ERROR_BIND: /* doesn't happen at the moment, we don't bind() */
	case NET_CONNECT_ERROR_CONNECT:
		yinfo("%d: Network error while connecting: %s", h->id, strerror(errno));
		hub_conn_disconnect(h, HUBCD_RECONNECT,
			errno == ETIMEDOUT    ? "org.freedesktop.DBus.Error.Timeout" :
			errno == ECONNREFUSED ? "net.blicky.Globster.Error.ConnectionRefused" :
			DBUS_ERROR_IO_ERROR, strerror(errno));
		break;
	case NET_CONNECT_DONE:
		yinfo("%d: Connected to %s with socket %d", h->id, addrstr, val);
		if(h->c.cmsg) {
			dbo_hubc_Connect_reply(h->c.cmsg);
			dbus_message_unref(h->c.cmsg);
			h->c.cmsg = NULL;
		}
		hub_conn_start_conn(h, val);
		break;
	}
}


/* Moves from the IDLE or TIMER state to the BUSY state and connects to the
 * given address. @msg may be NULL when auto-reconnecting. */
static void hub_conn_connect(hub_t *h, const char *addr, DBusMessage *msg) {
	assert(hub_conn_state(h) == HUBC_IDLE || hub_conn_state(h) == HUBC_TIMER);

	yuri_t uri;
	if(yuri_parse(addr, &uri)) {
		yinfo("%d: Address '%s' is invalid", h->id, addr);
		if(msg)
			dbo_sendfree(dbus_message_new_error_printf(msg, DBUS_ERROR_INVALID_ARGS, "Invalid address format"));
		hub_conn_disconnect(h, 0, NULL, NULL);
		return;
	}

	h->proto = strcmp(uri.scheme, "adc") == 0 || strcmp(uri.scheme, "adcs") == 0 ? &hub_adc : &hub_nmdc;
	if(!uri.port && h->proto == &hub_adc) {
		yinfo("%d: Address '%s' is missing a port", h->id, addr);
		if(msg)
			dbo_sendfree(dbus_message_new_error_printf(msg, DBUS_ERROR_INVALID_ARGS, "Missing port"));
		hub_conn_disconnect(h, 0, NULL, NULL);
		return;
	}
	if(!uri.port)
		uri.port = 411;

	yinfo("%d: Connecting to %s", h->id, addr);
	h->c.cmsg = msg ? dbus_message_ref(msg) : NULL;
	if(strcmp(addr, h->c.props.ConnectAddress) != 0) {
		dbo_hubc_ConnectAddress_set(h, &h->c.props, addr);
		hub_hub_setvar(h, "ConnectAddress", sqlasync_text(SQLASYNC_COPY, addr));
	}
	dbo_hubc_KeyprintSHA256_set(h, &h->c.props, "");

	h->c.istls = strncmp(addr, "adcs://", 7) == 0 || strncmp(addr, "nmdcs://", 7) == 0;

	/* TODO: Set bind addresses, if requested */
	h->c.con = calloc(1, sizeof(net_connect_t));
	h->c.con->data = h;
	h->c.con->timeout = h->c.conntimeout;
	net_connect(h->c.con, uri.host, uri.port, hub_conn_conncb);

	/* Make sure to stop the timer, since we're be leaving the TIMER state now */
	if(hub_conn_state(h) == HUBC_TIMER)
		ev_timer_stop(EV_DEFAULT_UC_ &h->c.timer);
	dbo_hubc_ConnectState_set(h, &h->c.props, HUBC_BUSY);
}



static void hub_conn_timer(EV_P_ ev_timer *timer, int status) {
	hub_t *h = timer->data;
	if(hub_conn_state(h) == HUBC_TIMER)
		hub_conn_connect(h, h->c.props.ConnectAddress, NULL);
	else {
		assert(hub_conn_state(h) == HUBC_DISC);
		ydebug("%d: Clean disconnect timed out, forcing", h->id);
		hub_conn_disconnect(h, h->c.wantreconnect ? HUBCD_RECONNECT : 0, NULL, NULL);
	}
}


void hub_conn_disconnect(hub_t *h, hub_conn_flags_t flags, const char *reason, const char *message) {
	hub_conn_state_t cur = hub_conn_state(h);
	hub_conn_state_t next = h->c.props.ReconnectTimeout && (flags & HUBCD_RECONNECT) ? HUBC_TIMER : HUBC_IDLE;

	switch(cur) {
	case HUBC_IDLE:
		break;
	case HUBC_BUSY:
		/* We could have been called from hub_conn_conncb(), at which point
		 * h->c.con has already been freed. */
		if(h->c.con)
			net_connect_cancel(h->c.con);
		if(h->c.cmsg) {
			dbo_sendfree(dbus_message_new_error(h->c.cmsg, reason, message));
			dbus_message_unref(h->c.cmsg);
			h->c.cmsg = NULL;
		}
		break;
	case HUBC_CONN:
		h->proto->disconnected(h);
		hub_hub_disconnected(h);
		hub_users_disconnected(h);
		if(!(flags & HUBCD_FORCE))
			next = HUBC_DISC;
		break;
	case HUBC_DISC:
	case HUBC_TIMER:
		ev_timer_stop(EV_DEFAULT_UC_ &h->c.timer);
		break;
	}

	dbo_hubc_ConnectState_set(h, &h->c.props, next);
	if(cur == HUBC_CONN || cur == HUBC_BUSY)
		dbo_hubc_Disconnected_signal(h, reason, message);

	if((cur == HUBC_CONN || cur == HUBC_DISC) && (next == HUBC_IDLE || next == HUBC_TIMER)) {
		/* We can't destroy the read buffer here because the netstream may still
		 * use it in a thread pool. Use the destroy callback to free it instead. */
		nets_data_set(h->c.n, h->c.buf);
		nets_destroy(h->c.n, hub_conn_destroycb);
		h->c.buf = NULL;
		h->c.n = NULL;
	}

	switch(next) {
	case HUBC_TIMER:
		ydebug("%d: Reconnecting in %"PRIu16" seconds", h->id, h->c.props.ReconnectTimeout);
		ev_timer_set(&h->c.timer, h->c.props.ReconnectTimeout, 0);
		ev_timer_start(EV_DEFAULT_UC_ &h->c.timer);
		break;
	case HUBC_DISC:
		ydebug("%d: Starting clean disconnect", h->id);
		ev_timer_set(&h->c.timer, DISC_TIMEOUT, 0);
		ev_timer_start(EV_DEFAULT_UC_ &h->c.timer);
		h->c.wantreconnect = flags & HUBCD_RECONNECT;
		nets_read_cancel(h->c.n);
		if(h->c.istls)
			nets_tls_disable(h->c.n, NULL);
		nets_sync(h->c.n, NETSS_READWRITE, hub_conn_synccb);
		/* Keep in mind that hub_conn_synccb() may be called immediately, and
		 * we have recursion because it calls this function again. */
		break;
	default:
		ydebug("%d: Disconnected", h->id);
	}
}


static void Connect(hub_t *h, DBusMessage *msg, const char *addr, uint16_t timeout_sec) {
	switch(hub_conn_state(h)) {
	case HUBC_TIMER:
	case HUBC_IDLE:
		if(!*addr)
			addr = h->c.props.ConnectAddress;
		if(!*addr)
			dbo_sendfree(dbus_message_new_error_printf(msg, DBUS_ERROR_INVALID_ARGS, "Empty address"));
		else {
			h->c.conntimeout = timeout_sec;
			hub_conn_connect(h, addr, msg);
		}
		break;
	default:
		dbo_sendfree(dbus_message_new_error_printf(msg, DBUS_ERROR_INVALID_ARGS, "Invalid state"));
	}
}


static void Disconnect(hub_t *h, DBusMessage *msg) {
	switch(hub_conn_state(h)) {
	case HUBC_BUSY:
	case HUBC_TIMER:
	case HUBC_CONN:
		hub_conn_disconnect(h, 0, "net.blicky.Globster.Disconnected", "Disconnected by user");
		dbo_hubc_Disconnect_reply(msg);
		break;
	default:
		dbo_sendfree(dbus_message_new_error_printf(msg, DBUS_ERROR_INVALID_ARGS, "Invalid state"));
	}
}


static void ForceDisconnect(hub_t *h, DBusMessage *msg) {
	hub_conn_disconnect(h, HUBCD_FORCE, "net.blicky.Globster.Disconnected", "Disconnected by user");
	dbo_hubc_ForceDisconnect_reply(msg);
}


static void set_AutoConnect(hub_t *h, DBusMessage *msg, bool val) {
	hub_hub_setvar(h, "AutoConnect", val ? sqlasync_int(1) : sqlasync_null());
	dbo_hubc_AutoConnect_setreply(h, &h->c.props, msg, val);
}


static void set_ReconnectTimeout(hub_t *h, DBusMessage *msg, uint16_t timeout) {
	dbo_hubc_ReconnectTimeout_setreply(h, &h->c.props, msg, timeout);
	hub_hub_setvar(h, "ReconnectTimeout", sqlasync_int(timeout));
	if(hub_conn_state(h) == HUBC_TIMER)
		hub_conn_disconnect(h, HUBCD_RECONNECT, NULL, NULL);
}


void hub_conn_autoconnect(hub_t *h) {
	if(h->c.props.AutoConnect && *h->c.props.ConnectAddress) {
		h->c.conntimeout = 60;
		hub_conn_connect(h, h->c.props.ConnectAddress, NULL);
	}
}


void hub_conn_init(hub_t *h, const char *addr, bool autoconnect, uint16_t timeout) {
	h->c.props.AutoConnect = autoconnect;
	h->c.props.ConnectState = HUBC_IDLE;
	h->c.props.ConnectAddress = strdup(addr);
	h->c.props.ResolvedAddress = strdup("");
	h->c.props.ReconnectTimeout = timeout;
	h->c.props.KeyprintSHA256 = strdup("");

	ev_timer_init(&h->c.timer, hub_conn_timer, h->c.props.ReconnectTimeout, 0);
	h->c.timer.data = h;
}


void hub_conn_destroy(hub_t *h) {
	hub_conn_disconnect(h, HUBCD_FORCE, "net.blicky.Globster.Disconnected", "Hub deleted");
	dbo_hubc_properties_free(&h->c.props);
}


const hub_conn_vt_t hub_conn_vt = dbo_hubc_funcs();

/* vim: set noet sw=4 ts=4: */
