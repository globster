/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef HUB_HUB_H
#define HUB_HUB_H


typedef struct {
	dbo_hub_properties_t props;
	/* When not logged in, this timer sets a timeout to ensure that we will log
	 * in within a certain timeout. When logged in, this timer is used to
	 * periodically send a keepalive message to the hub. */
	ev_timer timer;
	/* Timer to delay a $MyINFO or BINF message */
	ev_timer infdelay;
	bool infchanged;
	bool infimportant;
	bool loggedin;
} hub_hub_t;


/* TODO: Convert to static inline functions?
 * Not easily possible, the hub_t struct isn't defined yet at this point. :-(
 */
#define hub_hub_mynick(_h)        ((_h)->h.props.MyNick)
#define hub_hub_mydescription(_h) ((_h)->h.props.MyDescription)
#define hub_hub_myemail(_h)       ((_h)->h.props.MyEmail)
#define hub_hub_myconnection(_h)  ((_h)->h.props.MyConnection)
#define hub_hub_mypassword(_h)    ((_h)->h.props.MyPassword)

#define hub_hub_encoding(_h)      ((_h)->h.props.HubEncoding)
#define hub_hub_name(_h)          ((_h)->h.props.HubName)
#define hub_hub_errormessage(_h)  ((_h)->h.props.HubErrorMessage)

#define hub_hub_setname(_h, _n)        dbo_hub_HubName_set(_h, &(_h)->h.props, _n ? _n : "");
#define hub_hub_setdescription(_h, _n) dbo_hub_HubDescription_set(_h, &(_h)->h.props, _n ? _n : "");
#define hub_hub_setversion(_h, _n)     dbo_hub_HubVersion_set(_h, &(_h)->h.props, _n ? _n : "");


/* Called from hub/proto when we've successfully logged into the hub */
void hub_hub_loggedin(hub_t *h);


/* Called when some info that affects our user info has changed. */
void hub_hub_infchange(hub_t *h, bool important);


/* Set the hub error properties. The hub will be disconnected unless HUBCD_NO
 * is specified. */
void hub_hub_seterror(hub_t *h, hub_conn_flags_t disconnectflags, const char *err, const char *msg, const char *redir);


/* Convenience function to save a hub variable.
 * The `name' pointer should be a static string.
 * If val is SQLITE_NULL, the variable is deleted instead (NULL is always default). */
void hub_hub_setvar(hub_t *h, const char *name, sqlasync_value_t val);


void hub_hub_connected(hub_t *h);
void hub_hub_disconnected(hub_t *h);
void hub_hub_init(hub_t *h, const char *nick, const char *desc, const char *mail, uint32_t conn, const char *encoding);
void hub_hub_destroy(hub_t *h);

typedef dbo_hub_vtable_t(hub_t) hub_hub_vt_t;
extern const hub_hub_vt_t hub_hub_vt;

#endif
/* vim: set noet sw=4 ts=4: */
