/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef HUB_USERS_H
#define HUB_USERS_H

/* Just to make things confusing, a hub_user_t describes a single user,
 * hub_users_t describes the user list. */


/* User fields (number, internal name, dbus type) */
#define HUBU_FIELDS\
	F( 1, NICK,        "s")\
	F( 2, DESCRIPTION, "s")\
	F( 3, CLIENT,      "s")\
	F( 4, MAIL,        "s")\
	F( 5, SLOTS,       "u")\
	F( 6, SHAREDFILES, "u")\
	F( 7, SHARESIZE,   "t")\
	F( 8, CONN,        "u")\
	F( 9, HUBS,        "(uuu)")\
	F(10, IP4,         "s")\
	F(11, IP6,         "s")\
	F(12, AS,          "u")\
	F(13, CID,         "ay")\
	F(14, FLAGS,       "u")

#define HUBU_FIELDNUM 14

typedef enum {
#define F(i, n, t) HUBU_##n = i,
	HUBU_FIELDS
#undef F
} hub_user_field_t;


/* Members are ordered by size to improve packing. */
typedef struct {
	bool online : 1;
	bool adc : 1;
	bool away : 1;
	bool op : 1;
	bool tls : 1;
	bool active : 1;
	bool delmark : 1;
	uint8_t hnorm;
	uint8_t hreg;
	uint8_t hop;
	uint8_t slots;
	uint16_t udp4;
	uint16_t udp6;
	uint32_t ref;  /* Reference count (always 1+ when online) */
	uint32_t as;   /* Auto-open slot thing */
	uint32_t conn; /* upload speed, in bytes/second */
	uint32_t sharedfiles;
	struct in_addr ip4;
	int64_t id;
	uint64_t sharesize;
	struct in6_addr ip6;
	char *nick;
	char *desc;
	char *mail;
	char *client;
	/* To differentiate between ADC / NMDC. */
	union {
		struct {
			adc_sid_t sid;
			char cid[24]; /* XXX: May grow when tiger-CIDs are being replaced */
		} a;
		struct {
			char *nick; /* Nick in hub-encoding, NULL if equivalent to UTF-8 nick. */
		} n;
	};
} hub_user_t;


__KHASH_TYPE(userlist, hub_user_t *, char);


typedef struct {
	dbo_hubu_properties_t props;
	kh_userlist_t *list;
	char idsalt[32];
	ev_timer sweep;
	/* A value that alternates at every sweep period. It is used for comparison
	 * against user->delmark to ensures that it will stay in the list for at
	 * least one sweep period. */
	bool delmark;
	bool needsweep;
} hub_users_t;


typedef struct {
	hub_t *h;
	hub_user_t *u;
	uint64_t oldsharesize;
	uint16_t changes[HUBU_FIELDNUM];
	int changenum;
} hub_user_update_t;


int64_t hub_users_genid(hub_t *h, const char *data, int len);
hub_user_t *hub_user_new(bool adc);

void hub_user_ref(hub_user_t *u);
void hub_user_unref(hub_t *h, hub_user_t *u);


/* Functions to perform a user info update, to be performed by a protocol
 * handler.
 * Fields that do not have a hub_user_field_t type may be modified without
 * requiring a call to hub_user_update_field().  The following fields may NOT
 * be modified:
 *   online, ref, delmark, id
 * When adding a new user, first create an object with hub_user_new(), set its
 * id, and then perform the update as if it is an old user.
 * Note that the hub_user_t object may be freed in hub_user_update_done(), so
 * don't keep the pointer around.
 */
void hub_user_update_init(hub_t *h, hub_user_update_t *d, hub_user_t *user);
void hub_user_update_field(hub_user_update_t *d, hub_user_field_t field);
void hub_user_update_done(hub_user_update_t *d, bool newuser);


/* Called when a user has left the hub or when we've disconnected. Returns
 * false if there is no such user online. */
bool hub_user_left(hub_t *h, int64_t id, int64_t initiator, const char *message);


/* Returns NULL if there's no user online with that ID */
hub_user_t *hub_users_get(hub_t *h, int64_t id);


/* Called by hub/proto when the user list has been completely fetched */
void hub_users_listcomplete(hub_t *h);


void hub_users_disconnected(hub_t *h);
void hub_users_init(hub_t *h, const char *salt);
void hub_users_shutdown(hub_t *h);
void hub_users_destroy(hub_t *h);

typedef dbo_hubu_vtable_t(hub_t) hub_users_vt_t;
extern const hub_users_vt_t hub_users_vt;

#endif
/* vim: set noet sw=4 ts=4: */
