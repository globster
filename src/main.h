/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef MAIN_H
#define MAIN_H

/* Global dbus connection */
extern DBusConnection *dbuscon;

/* Global thread pool */
extern evtp_t *threadpool;


/* Main program state */
typedef enum {
	MAIN_INIT,    /* Before ev_run() */
	MAIN_RUNNING, /* Inside ev_run, before main_shutdown() has been called */
	MAIN_SHUTDOWN /* After main_shutdown() has been called */
} main_state_t;

extern main_state_t main_state;


/* Initiates program shutdown. Can be called from the RUNNING and SHUTDOWN
 * states. */
void main_shutdown();

#endif

/* vim: set noet sw=4 ts=4: */
