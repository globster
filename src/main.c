/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <global.h>
#include <dbusev.h>
#include <yopt.h>
#include <syslog.h>


DBusConnection *dbuscon;
evtp_t *threadpool;
main_state_t main_state;


#define BUS_NAME "net.blicky.Globster"


static bool conf_system = false;
static bool conf_autoconnect = true;
static bool conf_daemon = true;
static char *conf_log_file = "";
static char *conf_log_level = NULL;
static char *conf_session_dir = NULL;

static logfile_t *log_file;
static pid_t daemonize_parent;
static ev_signal termsig, intsig, hupsig;


static DBusHandlerResult dbusdisconnectfilter(DBusConnection *c, DBusMessage *msg, void *dat) {
	if(dbus_message_is_signal(msg, DBUS_INTERFACE_LOCAL, "Disconnected") && dbus_message_has_path(msg, DBUS_PATH_LOCAL)) {
		yinfo("Lost connection with D-Bus");
		main_shutdown();
		return DBUS_HANDLER_RESULT_HANDLED;
	}
	return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
}


/* Creates a dbus connection and requests (or rather, requires) our BUS_NAME.
 * Exits on error. */
static void init_dbus() {
	DBusError err;
	dbus_error_init(&err);

	dbuscon = dbus_bus_get_private(conf_system ? DBUS_BUS_SYSTEM : DBUS_BUS_SESSION, &err);
	if(dbus_error_is_set(&err)) {
		printf("Failed to connect to the bus: %s\n", err.message);
		exit(1);
	}

	int ret = dbus_bus_request_name(dbuscon, BUS_NAME, DBUS_NAME_FLAG_DO_NOT_QUEUE, &err);
	if(dbus_error_is_set(&err) || ret == DBUS_REQUEST_NAME_REPLY_EXISTS) {
		printf("Failed to acquire dbus name: %s\n", dbus_error_is_set(&err) ? err.message : "Name already taken");
		exit(1);
	}
	dbus_connection_set_exit_on_disconnect(dbuscon, FALSE);
	dbus_connection_add_filter(dbuscon, dbusdisconnectfilter, NULL, NULL);

	dbusev_register(EV_DEFAULT_UC_ dbuscon);

	/* Register a NULL wakeup function in order to unregister the one set by
	 * dbusev.c. We don't call dbus from multiple threads, so there's no need
	 * for that handler. Having too many ev_async handlers registered at a time
	 * may slow down event loop processing. This is also the only event for
	 * which dbusev.c has to keep an event handler active with libev even after
	 * the dbus connection has been shut down. Keeping it registered will stall
	 * the shutdown process. */
	dbus_connection_set_wakeup_main_function(dbuscon, NULL, NULL, NULL);
}


static void log_handler(const char *file, int line, int level, const char *message) {
#ifdef GLOBSTER_FN_PREFIX
	if(strncmp(file, GLOBSTER_FN_PREFIX, -1+sizeof GLOBSTER_FN_PREFIX) == 0)
		file += -1+sizeof GLOBSTER_FN_PREFIX;
#endif
	static pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;
	char lvl[8], tstr[32] = {};
	if(strcmp(conf_log_file, "syslog") != 0) {
		time_t t = time(NULL);
		strftime(tstr, sizeof(tstr)-1, "%F %H:%M:%S %Z", localtime(&t));
		switch(level) {
			case YLOG_ERR:   strcpy(lvl, "ERROR"); break;
			case YLOG_WARN:  strcpy(lvl, "WARN-"); break;
			case YLOG_INFO:  strcpy(lvl, "info-"); break;
			case YLOG_DEBUG: strcpy(lvl, "debug"); break;
			default:         sprintf(lvl, "%5d", level);
		}
	}

	pthread_mutex_lock(&m);
	if(log_file)
		logfile_logf(log_file, "%s -%s- %s:%d: %s\n", tstr, lvl, file, line, message);
	else if(strcmp(conf_log_file, "syslog") == 0)
		syslog(level == YLOG_ERR  ? LOG_ERR :
		       level == YLOG_WARN ? LOG_WARNING :
		       level == YLOG_INFO ? LOG_INFO : LOG_DEBUG,
		       "%s:%d: %s", file, line, message);
	else
		fprintf(strcmp(conf_log_file, "stderr") == 0 ? stderr : stdout,
			"%s -%s- %s:%d: %s\n", tstr, lvl, file, line, message);
	pthread_mutex_unlock(&m);
}


#ifdef USE_GCRYPT
GCRY_THREAD_OPTION_PTHREAD_IMPL;
#endif

static void init_crypt() {
#ifdef USE_GCRYPT
	gcry_control(GCRYCTL_SET_THREAD_CBS, &gcry_threads_pthread);
	if(!gcry_check_version(GCRYPT_VERSION)) {
		fputs("libgcrypt version mismatch\n", stderr);
		exit(1);
	}
	gcry_control(GCRYCTL_ENABLE_QUICK_RANDOM);
	gcry_control(GCRYCTL_INITIALIZATION_FINISHED, 0);
#endif
	gnutls_global_init();
}


static void print_help() {
	puts("globster <options>\n\n"
		"  -V, --version              Print version information and exit\n"
		"  -h, --help                 This help message\n"
		"  --system                   Connect to the D-Bus system bus\n"
		"  --session                  Connect to the D-Bus session bus (default)\n"
		"  --no-daemon                Don't detatch from the console\n"
		"  -l, --log-file FILE        Log to the given file, `stdout', `stderr' or `syslog'\n"
		"  --log-level                Set the log level\n"
		"  -c, --session-dir PATH     Set the session directory\n"
		"  -n                         Disable autoconnect");
}


static void argv_parse(int argc, char **argv) {
	yopt_t yopt;
	int v;
	char *val;

	static const yopt_opt_t opts[] = {
		{ 'V', 0, "-V,--version" },
		{ 'h', 0, "-h,--help"    },
		{ 'Y', 0, "--system"     },
		{ 'S', 0, "--session"    },
		{ 'D', 0, "--no-daemon"  },
		{ 'l', 1, "-l,--log-file"},
		{ 'L', 1, "--log-level"  },
		{ 'c', 1, "-c,--session-dir" },
		{ 'n', 0, "-n"           },
		{0,0,NULL}
	};

	yopt_init(&yopt, argc, argv, opts);
	while((v = yopt_next(&yopt, &val)) != -1) {
		switch(v) {
		case 'V':
			printf("%s version %s\n", PACKAGE, app_version_long());
			exit(0);
		case 'h':
			print_help();
			exit(0);
		case 'Y': conf_system = true; break;
		case 'S': conf_system = false; break;
		case 'D': conf_daemon = false; break;
		case 'l': conf_log_file = val; break;
		case 'L': conf_log_level = val; break;
		case 'c': conf_session_dir = val; break;
		case 'n': conf_autoconnect = false; break;
		case -2:
			printf("%s: %s.\n", argv[0], val);
			exit(1);
		}
	}
}


void main_shutdown() {
	assert(main_state != MAIN_INIT);
	if(main_state != MAIN_RUNNING)
		return;
	main_state = MAIN_SHUTDOWN;

	/* We do a clean shutdown of the daemon by stopping all activities. When no
	 * new actions are queued with libev, ev_run() should return. */
	yinfo("Preparing for shutdown");

	ev_signal_stop(EV_DEFAULT_UC_ &termsig);
	ev_signal_stop(EV_DEFAULT_UC_ &intsig);
	ev_signal_stop(EV_DEFAULT_UC_ &hupsig);
	hub_manager_shutdown();

	dbus_connection_close(dbuscon);

	/* XXX: See note in db.h for a caveat regarding db_shutdown(). */
	db_shutdown();
}




static void daemonize_usr1(int sig) {
	exit(0);
}


static void daemonize_init() {
	if(!conf_daemon)
		return;

	daemonize_parent = getpid();
	signal(SIGUSR1, daemonize_usr1);
	pid_t pid = fork();

	if(pid == -1) {
		printf("Error forking: %s.\n", strerror(errno));
		exit(1);
	}
	if(pid == 0) /* Child */
		return;

	/* We're now in the parent */
	int status;
	while(waitpid(pid, &status, 0) != pid)
		;
	/* If we're here, then the child failed to initialize. Otherwise we should
	 * have shut down cleanly in a SIGUSR1. */
	exit(1);
}


static void daemonize_done() {
	if(!conf_daemon)
		return;

	/* TODO: chdir("/")? This is currently not a good idea because we may still
	 * access the filesystem using db_dir, and that path may be relative. Or
	 * maybe we should chdir() to that instead? */

	int fd = open("/dev/null", O_RDWR, 0);
	if(fd >= 0) {
		if(dup2(fd, 0) < 0 || dup2(fd, 1) < 0 || dup2(fd, 2) < 0) {
			printf("Error dup2()'ing: %s.\n", strerror(errno));
			exit(1);
		}
		if(fd > 2)
			close(fd);
	} else {
		close(0);
		close(1);
		close(2);
	}

	setsid();
	kill(daemonize_parent, SIGUSR1);
}




static void shutdown_sig(EV_P_ ev_signal *w, int revents) {
	main_shutdown();
}


static void hup_sig(EV_P_ ev_signal *w, int revents) {
	logfile_global_reopen();
}


int main(int argc, char **argv) {
	ylog_set_handler(log_handler);
	argv_parse(argc, argv);

	main_state = MAIN_INIT;
	daemonize_init();

	if(strcmp(conf_log_file, "syslog") == 0)
		openlog("globster", 0, LOG_USER);
	else if(strcmp(conf_log_file, "stderr") != 0 && strcmp(conf_log_file, "stdout") != 0)
		log_file = logfile_open(conf_log_file);

	if(!conf_log_level)
		conf_log_level = getenv("YLOG_LEVEL");
	ylog_set_level(YLOG_DEFAULT, conf_log_level);

	/* Ensure we have a default loop */
	ev_default_loop(0);
	threadpool = evtp_create(EV_DEFAULT_UC_ 4);

	ev_signal_init(&termsig, shutdown_sig, SIGTERM);
	ev_signal_init(&intsig, shutdown_sig, SIGINT);
	ev_signal_init(&hupsig, hup_sig, SIGHUP);
	ev_signal_start(EV_DEFAULT_UC_ &termsig);
	ev_signal_start(EV_DEFAULT_UC_ &intsig);
	ev_signal_start(EV_DEFAULT_UC_ &hupsig);

	init_crypt();
	init_dbus();
	dbo_init();
	db_init(conf_session_dir, *conf_log_file ? NULL : &log_file);
	app_init(conf_log_level);
	hub_manager_create(conf_autoconnect);

	daemonize_done();
	main_state = MAIN_RUNNING;

	/* Make sure to dispatch() before entering the main loop, we may have missed
	 * a dispatchstatus update while initializing. */
	while(dbus_connection_dispatch(dbuscon) != DBUS_DISPATCH_COMPLETE)
		;

	yinfo("Entering main loop");
	ev_run(EV_DEFAULT_UC_ 0);

	yinfo("Main loop shut down");

	evtp_die(threadpool, 1);
	evtp_destroy(threadpool);
	db_destroy();

	yinfo("Clean shutdown");

	if(log_file)
		logfile_close(log_file);
	else if(strcmp(conf_log_file, "syslog") == 0)
		closelog();

	return 0;
}

/* vim: set noet sw=4 ts=4: */
