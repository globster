/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef GLOBAL_H
#define GLOBAL_H

#include "config.h"

/* Just give me a nice C99 + POSIX environment! */
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdarg.h>
#include <errno.h>
#include <math.h>

#include <unistd.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/uio.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <netdb.h>
#include <pthread.h>
#include <iconv.h>

#include <sqlite3.h>
#include <gnutls/gnutls.h>
#include <gnutls/x509.h>
#ifdef USE_GCRYPT
#include <gcrypt.h>
#else
#include <gnutls/crypto.h>
#endif
#include <dbus/dbus.h>
#include <ev.h>
#include <zlib.h>

#include <evtp.h>
#include <khash.h>
#include <kstring.h>
#include <sqlasync.h>
#include <ylog.h>
#include <yuri.h>

#include <compat.h>
#include <util/adc.h>
#include <util/base32.h>
#include <util/list.h>
#include <util/logfile.h>
#include <util/netstream.h>
#include <util/netutil.h>
#include <util/nmdc.h>
#include <util/vec.h>

#include <main.h>
#include <app.h>
#include <db.h>
#include <dbo.h>
#include <hub/global.h>
#include <interfaces.h>

#endif

/* vim: set noet sw=4 ts=4: */
