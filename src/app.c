/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <global.h>
#include <tiger.h>


#define APP_PATH "/net/blicky/Globster"


static const char *version =
#include <version.h>
;


typedef struct {
	dbo_t obj;
	dbo_app_properties_t props;
} app_t;

static app_t app_obj;


const char *app_version_long()  {
	return version;
}


const char *app_version_short() {
	static char *v = NULL;
	if(!v) {
		v = strdup(version);
		char *x;
		if((x = strchr(v, '-')) && (x = strchr(x+1, '-')))
			*x = 0;
	}
	return v;
}


int app_version_num() {
	static int v = -1;
	if(v < 0) {
		int a=0,i=0,p=0,c=0;
		if(    sscanf(version, "%d.%d.%d-%d", &a, &i, &p, &c) != 4
			&& sscanf(version, "%d.%d-%d", &a, &i, &c) != 3
			&& sscanf(version, "%d-%d", &a, &c) != 2
			&& sscanf(version, "%d.%d.%d", &a, &i, &p) != 3
			&& sscanf(version, "%d.%d", &a, &i) != 2
			&& sscanf(version, "%d", &a) != 1)
			yerror("Unable to parse program version: %s", version);
		/* aaiiippccc */
		v = (a * 100000000) + (i * 100000) + (p * 1000) + c;
	}
	return v;
}




static gnutls_certificate_credentials_t app_cert;

gnutls_certificate_credentials_t app_certificate() { return app_cert; }


static void app_cert_gen(gnutls_x509_crt_t cert, gnutls_x509_privkey_t key) {
	const size_t buflen = 32*1024;
	char *keystr = malloc(buflen);
	char *certstr = malloc(buflen);
	size_t len;

	yinfo("Generating client certificate");

	/* Private key */
	int bits = 2432;
#if GNUTLS_VERSION_MAJOR > 2 || (GNUTLS_VERSION_MAJOR == 2 && GNUTLS_VERSION_MINOR >= 12)
	bits = gnutls_sec_param_to_pk_bits(GNUTLS_PK_RSA, GNUTLS_SEC_PARAM_NORMAL);
#endif
	gnutls_x509_privkey_generate(key, GNUTLS_PK_RSA, bits, 0);
	len = buflen;
	assert(gnutls_x509_privkey_export(key, GNUTLS_X509_FMT_PEM, (char *)keystr, &len) == 0);

	/* Certificate */
	time_t t = time(NULL);
	gnutls_x509_crt_set_dn_by_oid(cert, GNUTLS_OID_X520_ORGANIZATION_NAME,        0, "Unknown", strlen("Unknown"));
	gnutls_x509_crt_set_dn_by_oid(cert, GNUTLS_OID_X520_ORGANIZATIONAL_UNIT_NAME, 0, "Unknown", strlen("Unknown"));
	gnutls_x509_crt_set_dn_by_oid(cert, GNUTLS_OID_X520_COMMON_NAME,              0, "Unknown", strlen("Unknown"));
	gnutls_x509_crt_set_dn_by_oid(cert, GNUTLS_OID_X520_LOCALITY_NAME,            0, "Unknown", strlen("Unknown"));
	gnutls_x509_crt_set_dn_by_oid(cert, GNUTLS_OID_X520_STATE_OR_PROVINCE_NAME,   0, "Unknown", strlen("Unknown"));
	gnutls_x509_crt_set_dn_by_oid(cert, GNUTLS_OID_X520_COUNTRY_NAME,             0, "UN", strlen("UN"));
	gnutls_x509_crt_set_key(cert, key);
	gnutls_x509_crt_set_serial(cert, &t, sizeof(t));
	gnutls_x509_crt_set_activation_time(cert, t-(24*3600));
	gnutls_x509_crt_set_expiration_time(cert, t+(3560*24*3600));
	gnutls_x509_crt_sign(cert, cert, key);
	len = buflen;
	assert(gnutls_x509_crt_export(cert, GNUTLS_X509_FMT_PEM, (char *)certstr, &len) == 0);

	sqlasync_sql(db_sql, NULL, SQLASYNC_STATIC,
			"INSERT OR REPLACE INTO vars (name, value) VALUES"
				"(\"CertificateKey\", ?),"
				"(\"CertificateCrt\", ?)",
			2, sqlasync_text(SQLASYNC_FREE, keystr), sqlasync_text(SQLASYNC_FREE, certstr));
}


static void app_cert_init() {
	gnutls_x509_crt_t cert;
	gnutls_x509_privkey_t key;
	gnutls_x509_crt_init(&cert);
	gnutls_x509_privkey_init(&key);

	sqlasync_queue_t *q = sqlasync_sql(db_sql, sqlasync_queue_sync(), SQLASYNC_STATIC,
			"SELECT (SELECT value FROM vars WHERE name = \"CertificateKey\"),"
			"       (SELECT value FROM vars WHERE name = \"CertificateCrt\")", 0);
	sqlasync_result_t *r = db_get(q, "getting certificate");
	sqlasync_queue_destroy(q);

	bool create = true;
	if(r->result == SQLITE_ROW && r->col[0].type == SQLITE_TEXT && r->col[1].type == SQLITE_TEXT) {
		gnutls_datum_t keydat = { r->col[0].val.ptr, strlen(r->col[0].val.ptr) };
		gnutls_datum_t crtdat = { r->col[1].val.ptr, strlen(r->col[1].val.ptr) };
		int r = gnutls_x509_crt_import(cert, &crtdat, GNUTLS_X509_FMT_PEM);
		if(r == 0)
			r = gnutls_x509_privkey_import(key, &keydat, GNUTLS_X509_FMT_PEM);
		if(r == 0)
			create = false;
		else
			ywarn("Error loading certificate: %s", gnutls_strerror(r));
	}
	sqlasync_result_free(r);

	if(create)
		app_cert_gen(cert, key);

	gnutls_certificate_allocate_credentials(&app_cert);

	gnutls_x509_crt_deinit(cert);
	gnutls_x509_privkey_deinit(key);
}




const char *app_tiger_cid() { return app_obj.props.TigerCID; }
const char *app_tiger_pid() { return app_obj.props.TigerPID; }


static void app_pid_save(const char *pid) {
	ydebug("Saving new PID: %s", pid);
	sqlasync_sql(db_sql, NULL, SQLASYNC_STATIC,
			"INSERT OR REPLACE INTO vars (name, value) VALUES (\"TigerPID\", ?)",
			1, sqlasync_text(SQLASYNC_COPY, pid));
}


static void app_pid_init(char *pid) {
	pid[0] = 0;

	sqlasync_queue_t *q = sqlasync_sql(db_sql, sqlasync_queue_sync(), SQLASYNC_STATIC,
			"SELECT value FROM vars WHERE name = \"TigerPID\"", 0);

	sqlasync_result_t *r = db_get(q, "getting PID");
	if(r->result == SQLITE_ROW && r->col[0].type == SQLITE_TEXT && isbase32_24(r->col[0].val.ptr))
		strcpy(pid, r->col[0].val.ptr);
	sqlasync_result_free(r);
	sqlasync_queue_destroy(q);

	if(pid[0])
		return;

	char buf[24];
	crypt_rnd(buf, 24);
	base32_encode(buf, pid, 24);
	app_pid_save(pid);
}


static char *app_cid_create(const char *pid, char *res) {
	t_res cid;

	base32_decode(pid, res, 24);
	tiger(res, 24, cid);
	t_to_from_littleendian(cid);
	base32_encode((char *)cid, res, 24);
	return res;
}


static void set_TigerPID(app_t *o, DBusMessage *msg, const char *newpid) {
	if(!isbase32_24(newpid))
		dbo_sendfree(dbus_message_new_error_printf(msg, DBUS_ERROR_INVALID_ARGS, "Invalid PID"));
	else {
		char cid[40];
		dbo_app_TigerCID_set(o, &o->props, app_cid_create(newpid, cid));
		dbo_app_TigerPID_setreply(o, &o->props, msg, newpid);
		app_pid_save(newpid);
		yinfo("PID/CID changed to %s/%s", o->props.TigerPID, o->props.TigerCID);
	}
}




static void set_LogLevel(app_t *o, DBusMessage *msg, const char *newlvl) {
	ylog_set_level(YLOG_DEFAULT, newlvl);
	dbo_app_LogLevel_setreply(o, &o->props, msg, newlvl);
	yinfo("Log level changed to: %s", newlvl);
}


static void Shutdown(app_t *obj, DBusMessage *msg) {
	dbo_app_Shutdown_reply(msg);
	main_shutdown();
}


void app_init(const char *loglvl) {
	static const dbo_app_vtable_t(app_t) vt = dbo_app_funcs();
	static const dbo_reg_t reg = { &dbo_app_interface, (void**)&vt, offsetof(app_t, props) };

	char pid[40];
	char cid[40];

	app_obj.props.VersionString = (char *)app_version_long(); /* No need to strdup(), won't get free()'d */
	app_obj.props.VersionNumber = app_version_num();

	app_obj.props.LogLevel = strdup(loglvl ? loglvl : "");

	app_pid_init(pid);
	app_obj.props.TigerPID = strdup(pid);
	app_obj.props.TigerCID = strdup(app_cid_create(pid, cid));

	app_cert_init();

	dbo_register(&app_obj.obj, APP_PATH, &reg, 1);
}

/* vim: set noet sw=4 ts=4: */
