/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <global.h>


sqlasync_t *db_sql;
const char *db_dir;


static sqlasync_wakeup_t *db_wakeup;
static ev_async db_async;


#define SQLVER     2  /* Current SQLite user_version value */
#define SQLVERSTR "2" /* Decimal string version of the above (can't use bind parameters in PRAGMA queries) */


/* Queries to execute to convert from one version to another. */
static const struct { int fromver, tover; const char *sql; } schema[] = {
	{ 0, 0,
		"CREATE TABLE vars ("
			"name TEXT NOT NULL PRIMARY KEY,"
			"value NONE NOT NULL"
		")"
	},
	{ 0, 1,
		"CREATE TABLE hub_vars ("
			"hub INTEGER NOT NULL,"
			"name TEXT NOT NULL,"
			"value NONE NOT NULL,"
			"PRIMARY KEY(hub, name)"
		")"
	}
};


static void db_checkschema() {
	sqlasync_queue_t *q = sqlasync_sql(db_sql, sqlasync_queue_sync(), SQLASYNC_STATIC, "PRAGMA user_version", 0);
	sqlasync_result_t *r = db_get(q, "getting user_version");
	int ver = r->col[0].val.i64;
	sqlasync_result_free(r);
	sqlasync_result_free(db_get(q, "getting user_version (2)")); /* The SQLITE_DONE */

	ydebug("On-disk database version: %d; Program database version: "SQLVERSTR, ver);
	if(ver == SQLVER) {
		sqlasync_queue_destroy(q);
		return;
	}

	sqlasync_lock(db_sql);
	int i, qnum = 1;
	for(i=0; i<(int)(sizeof(schema)/sizeof(*schema)); i++) {
		if(ver < schema[i].fromver || ver > schema[i].tover)
			continue;
		ydebug("Queueing update query %d: %s", i, schema[i].sql);
		qnum++;
		sqlasync_sql_unlocked(db_sql, q, SQLASYNC_STATIC|SQLASYNC_NEXT, schema[i].sql, 0);
	}
	sqlasync_sql_unlocked(db_sql, q, SQLASYNC_STATIC|SQLASYNC_LAST, "PRAGMA user_version = "SQLVERSTR, 0);
	sqlasync_unlock(db_sql);

	char msg[48] = {};
	for(i=0; i<qnum; i++) {
		snprintf(msg, sizeof(msg), "update query %d/%d (%d -> "SQLVERSTR")", i, qnum, ver);
		r = db_get(q, msg);
		while(!r->last) {
			sqlasync_result_free(r);
			r = db_get(q, msg);
		}
		sqlasync_result_free(r);
	}

	sqlasync_queue_destroy(q);
}


sqlasync_result_t *db_get(sqlasync_queue_t *q, const char *op) {
	sqlasync_result_t *r = sqlasync_queue_get(q);
	assert(r != NULL);
	if(r->result == SQLITE_OK || r->result == SQLITE_ROW || r->result == SQLITE_DONE)
		return r;

	/* TODO: An SQL error only really happens in the following cases:
	 * - Database corruption (caused by the user or failty hardware)
	 * - We've ran out of disk space.
	 * How should those errors be handled? Shutdown or recover? */

	/* SQL error in INIT is always fatal. */
	if(main_state == MAIN_INIT) {
		printf("SQL error on %s: %s (%d)\n", op, (char*)r->col[0].val.ptr, r->result);
		exit(1);
	}

	yerror("SQL error on %s: %s (%d)", op, (char*)r->col[0].val.ptr, r->result);
	sqlasync_result_free(r);
	return NULL;
}


static void db_wakeup_cb(sqlasync_wakeup_t *w, void *data) {
	ev_async_send(EV_DEFAULT_UC_ &db_async);
}


static void db_async_cb(EV_P_ ev_async *async, int revents) {
	sqlasync_dispatch(db_wakeup);
}


static void db_error_cb(sqlasync_queue_t *q, void *data) {
	sqlasync_result_t *r = db_get(q, "asynchronous COMMIT");
	if(!r)
		return;

	/* If we received a non-error result, that means that the database has been
	 * closed and we have no more actions scheduled. We can safely stop the
	 * async watcher here, and doing so will drop the reference that the
	 * database has on the main loop, allowing ev_run() to return. */
	assert(main_state == MAIN_SHUTDOWN);
	ev_async_stop(EV_DEFAULT_UC_ &db_async);
	sqlasync_queue_destroy(q);
	sqlasync_result_free(r);
}


void db_sql_init() {
	ev_async_init(&db_async, db_async_cb);
	ev_async_start(EV_DEFAULT_UC_ &db_async);

	db_wakeup = sqlasync_wakeup_create(db_wakeup_cb, NULL, NULL);

	struct timespec transtimeout = { 5, 0 };
	db_sql = sqlasync_create(&transtimeout);

	kstring_t fn = {};
	ksprintf(&fn, "%s/%s", db_dir, "globster.sqlite3");
	sqlasync_queue_t *q = sqlasync_open(db_sql,
			sqlasync_queue_sync(),
			sqlasync_queue_async(db_wakeup, 1, db_error_cb, NULL),
			fn.s, 0);
	free(fn.s);

	sqlasync_result_t *r = db_get(q, "database open");
	sqlasync_result_free(r);
	sqlasync_queue_destroy(q);
}


static const char *db_getdir(const char *dir) {
	kstring_t d = {};
	if(!dir)
		dir = getenv("GLOBSTER_DIR");
	if(dir)
		kputs(dir, &d);

	if(!dir) {
		dir = getenv("HOME");
		if(!dir || !strlen(dir))
			kputs(".globster", &d);
		else
			ksprintf(&d, "%s%s", dir, dir[strlen(dir)-1] == '/' ? ".globster" : "/.globster");
	}

	if(d.s[d.l-1] == '/')
		d.s[d.l-1] = 0;
	if(!d.s[0]) {
		printf("Can't use root or an empty string as session directory.\n");
		exit(1);
	}
	return d.s;
}


static void db_ensuredirexists(const char *dir) {
	struct stat st;
	int r = stat(dir, &st);
	if(!r && !S_ISDIR(st.st_mode)) {
		printf("The path \"%s\" does not point to a directory.\n", dir);
		exit(1);
	}
	if(r == -1 && errno != ENOENT) {
		printf("Error stating \"%s\": %s.\n", dir, strerror(errno));
		exit(1);
	}
	if(r == -1 && errno == ENOENT && mkdir(dir, 0700) != 0) {
		printf("Error creating \"%s\": %s.\n", dir, strerror(errno));
		exit(1);
	}
}


static void db_lockdir() {
	kstring_t fn = {};
	ksprintf(&fn, "%s/%s", db_dir, "lock");
	int fd = open(fn.s, O_RDWR|O_CREAT, 0600);
	struct flock lck;
	lck.l_type = F_WRLCK;
	lck.l_whence = SEEK_SET;
	lck.l_start = 0;
	lck.l_len = 0;
	if(fd == -1 || fcntl(fd, F_SETLK, &lck) == -1) {
		printf("Unable to open lock file. Is another instance of the Globster daemon running with the same configuration directory?\n");
		exit(1);
	}
	free(fn.s);
}


void db_init(const char *dir, logfile_t **log) {
	db_dir = db_getdir(dir);
	db_ensuredirexists(db_dir);
	db_lockdir();

	kstring_t fn = {};
	ksprintf(&fn, "%s/%s", db_dir, "logs");
	db_ensuredirexists(fn.s);

	if(log) {
		ksprintf(&fn, "/%s", "main.log");
		*log = logfile_open(fn.s);
	}
	free(fn.s);

	yinfo("Starting Globster %s (%d)", app_version_long(), app_version_num());
	yinfo("Using database directory: %s", db_dir);

	db_sql_init();
	db_checkschema();
}


void db_shutdown() {
	sqlasync_close(db_sql);
}


void db_destroy() {
	sqlasync_wakeup_destroy(db_wakeup);
	sqlasync_destroy(db_sql);
}


/* vim: set noet sw=4 ts=4: */
