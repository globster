/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <global.h>


static DBusObjectPathVTable vtable;

#define XML_INTERFACE_INTROSPECTABLE \
	"  <interface name=\"org.freedesktop.DBus.Introspectable\">\n"\
	"    <method name=\"Introspect\">\n"\
	"      <arg type=\"s\" name=\"xml_data\" direction=\"out\"/>\n"\
	"    </method>\n"\
	"  </interface>\n"


#define XML_INTERFACE_PEER \
	"  <interface name=\"org.freedesktop.DBus.Peer\">\n"\
	"    <method name=\"Ping\"/>\n"\
	"    <method name=\"GetMachineId\">\n"\
	"      <arg type=\"s\" name=\"machine_uuid\" direction=\"out\"/>\n"\
	"    </method>\n"\
	"  </interface>\n"


#define XML_INTERFACE_PROPERTIES \
	"  <interface name=\"org.freedesktop.DBus.Properties\">\n"\
	"    <method name=\"Get\">\n"\
	"      <arg type=\"s\" name=\"interface_name\" direction=\"in\"/>\n"\
	"      <arg type=\"s\" name=\"property_name\" direction=\"in\"/>\n"\
	"      <arg type=\"v\" name=\"value\" direction=\"out\"/>\n"\
	"    </method>\n"\
	"    <method name=\"GetAll\">\n"\
	"      <arg type=\"s\" name=\"interface_name\" direction=\"in\"/>\n"\
	"      <arg type=\"a{sv}\" name=\"properties\" direction=\"out\"/>\n"\
	"    </method>\n"\
	"    <method name=\"Set\">\n"\
	"      <arg type=\"s\" name=\"interface_name\" direction=\"in\"/>\n"\
	"      <arg type=\"s\" name=\"property_name\" direction=\"in\"/>\n"\
	"      <arg type=\"v\" name=\"value\" direction=\"in\"/>\n"\
	"    </method>\n"\
	"    <signal name=\"PropertiesChanged\">\n"\
	"      <arg type=\"s\" name=\"interface_name\"/>\n"\
	"      <arg type=\"a{sv}\" name=\"changed_properties\"/>\n"\
	"      <arg type=\"as\" name=\"invalidated_properties\"/>\n"\
	"    </signal>\n"\
	"  </interface>\n"


/* TODO: To improve performance (and to make this operation re-entrant), it
 * would help to pre-calculate the signatures when generating the interface
 * structures. */
static const char *method_signature(const dbo_method_t *m) {
	static kstring_t s = {};
	const char *arg = m->in;
	s.l = 0;
	while(arg && *arg) {
		kputs(arg, &s);
		arg += strlen(arg)+1;
		arg += strlen(arg)+1;
	}
	return s.l ? s.s : "";
}


static void introspect_itf_annotations(char flags, kstring_t *s) {
	if(flags & DBO_DEPRECATED)
		kputs("      <annotation name=\"org.freedesktop.DBus.Deprecated\" value=\"true\"/>\n", s);
	if(flags & DBO_NOREPLY)
		kputs("      <annotation name=\"org.freedesktop.DBus.Method.NoReply\" value=\"true\"/>\n", s);
	if(flags & DBO_EMITNONE)
		kputs("      <annotation name=\"org.freedesktop.DBus.Propery.EmitsChangedSignal\" value=\"false\"/>\n", s);
	else if(flags & DBO_EMITCHANGE)
		kputs("      <annotation name=\"org.freedesktop.DBus.Propery.EmitsChangedSignal\" value=\"invalidates\"/>\n", s);
}


/* Serialize an interface for introspection. */
static void introspect_itf(const dbo_interface_t *itf, kstring_t *s) {
	int i;
	const char *arg;
	ksprintf(s, "  <interface name=\"%s\">\n", itf->name);

	/* Methods */
	for(i=0; i<itf->n_methods; i++) {
		const dbo_method_t *m = itf->methods+i;
		ksprintf(s, "    <method name=\"%s\"", m->name);
		if(!m->flags && !m->in && !m->out) {
			kputs("/>\n", s);
			continue;
		}
		kputs(">\n", s);
		for(arg=m->in; arg && *arg; arg += strlen(arg)+1) {
			const char *type = arg;
			arg += strlen(arg)+1;
			ksprintf(s, "      <arg name=\"%s\" type=\"%s\" direction=\"in\"/>\n", arg, type);
		}
		for(arg=m->out; arg && *arg; arg += strlen(arg)+1) {
			const char *type = arg;
			arg += strlen(arg)+1;
			ksprintf(s, "      <arg name=\"%s\" type=\"%s\" direction=\"out\"/>\n", arg, type);
		}
		introspect_itf_annotations(m->flags, s);
		kputs("    </method>\n", s);
	}

	/* Signals */
	for(i=0; i<itf->n_signals; i++) {
		const dbo_signal_t *sig = itf->signals+i;
		ksprintf(s, "    <signal name=\"%s\"", sig->name);
		if(!sig->flags && !sig->args) {
			kputs("/>\n", s);
			continue;
		}
		kputs(">\n", s);
		for(arg=sig->args; arg && *arg; arg += strlen(arg)+1) {
			const char *type = arg;
			arg += strlen(arg)+1;
			ksprintf(s, "      <arg name=\"%s\" type=\"%s\"/>\n", arg, type);
		}
		introspect_itf_annotations(sig->flags, s);
		kputs("    </signal>\n", s);
	}

	/* Properties */
	for(i=0; i<itf->n_properties; i++) {
		const dbo_property_t *p = itf->properties+i;
		ksprintf(s, "    <property name=\"%s\" type=\"%s\" access=\"%s\"", p->name, p->type,
			(p->flags & DBO_READWRITE) == DBO_READWRITE ? "readwrite" : p->flags & DBO_WRITE ? "write" : "read");
		if(!(p->flags & ~DBO_READWRITE)) {
			kputs("/>\n", s);
			continue;
		}
		kputs(">\n", s);
		introspect_itf_annotations(p->flags, s);
		kputs("    </property>\n", s);
	}

	kputs("  </interface>\n", s);
}


/* Serialize a list of registered child nodes for introspection. */
static void introspect_childs(DBusMessage *msg, kstring_t *s) {
	char **childs, **child;
	const char *path = dbus_message_get_path(msg);
	if(!path)
		path = "/";
	ydebug("introspect(%s)", path);
	dbus_connection_list_registered(dbuscon, path, &childs);
	for(child=childs; *child; child++)
		ksprintf(s, "  <node name=\"%s\" />\n", *child);
	dbus_free_string_array(childs);
}


static void handle_introspect(DBusMessage *msg, dbo_t *obj) {
	kstring_t s = {};
	kputs(DBUS_INTROSPECT_1_0_XML_DOCTYPE_DECL_NODE"<node>\n", &s);

	if(obj) {
		/* Only include the Properties interface if one of the interfaces
		 * supported by this object actually has properties. */
		if(obj->hasprops)
			kputs(XML_INTERFACE_PROPERTIES, &s);

		/* Pointless Introspect and Peer interfaces. Even though we support
		 * these on every path, they are only added to introspection data for
		 * objects which we have actually registered. This mimics GDBus
		 * behaviour. */
		kputs(XML_INTERFACE_INTROSPECTABLE XML_INTERFACE_PEER, &s);

		int i;
		for(i=0; i<obj->numregs; i++)
			introspect_itf(obj->regs[i].itf, &s);
	}

	introspect_childs(msg, &s);
	kputs("</node>\n", &s);

	DBusMessage *reply = dbus_message_new_method_return(msg);
	dbus_message_append_args(reply, DBUS_TYPE_STRING, &s.s, DBUS_TYPE_INVALID);
	dbo_sendfree(reply);
	free(s.s);
}


/* Find an object property given the interface and property names. Returns the
 * index of the integer on success, -1 on failure. Sets *res to point to the
 * property object and *idx to the offset for the vtable. */
static int prop_find(dbo_t *obj, const char *interface, const char *prop, const dbo_property_t **res, int *idx) {
	int i, j;

	for(i=0; i<obj->numregs; i++) {
		const dbo_interface_t *itf = obj->regs[i].itf;
		if(*interface && strcmp(itf->name, interface) != 0)
			continue;
		if(idx)
			*idx = 0;
		for(j=0; j<itf->n_properties; j++) {
			if(strcmp(itf->properties[j].name, prop) == 0) {
				*res = itf->properties+j;
				return i;
			}
			if(idx && (itf->properties[j].flags & DBO_WRITE))
				(*idx)++;
		}
	}
	return -1;
}


static void prop_get(dbo_t *obj, DBusMessage *msg) {
	const char *itf, *prop;
	dbus_message_get_args(msg, NULL, DBUS_TYPE_STRING, &itf, DBUS_TYPE_STRING, &prop, DBUS_TYPE_INVALID);
	ydebug("prop_get(%s, %s, %s)", obj->path, itf, prop);
	const dbo_property_t *p;
	int i = prop_find(obj, itf, prop, &p, NULL);
	if(i < 0 || !(p->flags & DBO_READ)) {
		dbo_sendfree(dbus_message_new_error_printf(msg,
			i < 0 ? DBUS_ERROR_UNKNOWN_PROPERTY : DBUS_ERROR_INVALID_ARGS,
			i < 0 ? "No such property `%s'" : "Property `%s' is not writable", prop));
		return;
	}

	DBusMessageIter iter, sub;
	DBusMessage *reply = dbus_message_new_method_return(msg);
	dbus_message_iter_init_append(reply, &iter);
	dbus_message_iter_open_container(&iter, DBUS_TYPE_VARIANT, p->type, &sub);
	p->getter(((char *)obj) + obj->regs[i].poffset, &sub);
	dbus_message_iter_close_container(&iter, &sub);
	dbo_sendfree(reply);
}


static void prop_getall(dbo_t *obj, DBusMessage *msg) {
	const char *interface;
	dbus_message_get_args(msg, NULL, DBUS_TYPE_STRING, &interface, DBUS_TYPE_INVALID);
	ydebug("prop_getall(%s, %s)", obj->path, interface);

	DBusMessageIter iter, sub, sub2, sub3;
	DBusMessage *reply = dbus_message_new_method_return(msg);
	dbus_message_iter_init_append(reply, &iter);
	dbus_message_iter_open_container(&iter, DBUS_TYPE_ARRAY, "{sv}", &sub);
	int i, j;
	for(i=0; i<obj->numregs; i++) {
		const dbo_interface_t *itf = obj->regs[i].itf;
		if(*interface && strcmp(itf->name, interface) != 0)
			continue;
		for(j=0; j<itf->n_properties; j++) {
			if(!(itf->properties[j].flags & DBO_READ))
				continue;
			dbus_message_iter_open_container(&sub, DBUS_TYPE_DICT_ENTRY, NULL, &sub2);
			dbus_message_iter_append_basic(&sub2, DBUS_TYPE_STRING, &(itf->properties[j].name));
			dbus_message_iter_open_container(&sub2, DBUS_TYPE_VARIANT, itf->properties[j].type, &sub3);
			itf->properties[j].getter(((char *)obj) + obj->regs[i].poffset, &sub3);
			dbus_message_iter_close_container(&sub2, &sub3);
			dbus_message_iter_close_container(&sub, &sub2);
		}
	}
	dbus_message_iter_close_container(&iter, &sub);
	dbo_sendfree(reply);
}


static void prop_set(dbo_t *obj, DBusMessage *msg) {
	const char *itf, *prop;

	DBusMessageIter iter, sub;
	dbus_message_iter_init(msg, &iter);
	dbus_message_iter_get_basic(&iter, &itf);
	dbus_message_iter_next(&iter);
	dbus_message_iter_get_basic(&iter, &prop);
	dbus_message_iter_next(&iter);
	dbus_message_iter_recurse(&iter, &sub);

	ydebug("prop_set(%s, %s, %s)", obj->path, itf, prop);
	const dbo_property_t *p;
	int idx;
	int i = prop_find(obj, itf, prop, &p, &idx);
	if(i < 0 || !(p->flags & DBO_WRITE)) {
		dbo_sendfree(dbus_message_new_error_printf(msg,
			i < 0 ? DBUS_ERROR_UNKNOWN_PROPERTY : DBUS_ERROR_PROPERTY_READ_ONLY,
			i < 0 ? "No such property `%s'" : "Property `%s' is read-only", prop));
		return;
	}

	p->setter(obj, msg, &sub, obj->regs[i].vtable[idx + obj->regs[i].itf->n_methods]);
}


static bool route_method(dbo_t *obj, DBusMessage *msg) {
	const char *interface = dbus_message_get_interface(msg);
	const char *method = dbus_message_get_member(msg);
	int i, j;

	for(i=0; i<obj->numregs; i++) {
		const dbo_interface_t *itf = obj->regs[i].itf;
		if(interface && strcmp(itf->name, interface) != 0)
			continue;

		/* Method lookup is done with a linear search, but this can be improved
		 * because the method table is known at compile-time. This could even
		 * use perfect hashing. */
		for(j=0; j<itf->n_methods; j++) {
			const dbo_method_t *m = itf->methods+j;
			if(strcmp(m->name, method) == 0 && dbus_message_has_signature(msg, method_signature(m))) {
				ydebug("method(%s, %s, %s, %s)", obj->path, itf->name, method, method_signature(m));
				DBusMessageIter iter;
				dbus_message_iter_init(msg, &iter);
				m->wrapper(obj, msg, &iter, obj->regs[i].vtable[j]);
				return true;
			}
		}
	}
	return false;
}


static DBusHandlerResult handle_message(DBusConnection *con, DBusMessage *msg, void *data) {
	dbo_t *obj = data;

	/* We only handle method calls. */
	if(dbus_message_get_type(msg) != DBUS_MESSAGE_TYPE_METHOD_CALL)
		return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;

	/* Introspection request */
	if(dbus_message_is_method_call(msg, DBUS_INTERFACE_INTROSPECTABLE, "Introspect") && dbus_message_has_signature(msg, "")) {
		handle_introspect(msg, obj);
		return DBUS_HANDLER_RESULT_HANDLED;
	}

	/* Any other messages require an object */
	if(!obj)
		goto unhandled;

	/* Properties */
	if(obj->hasprops) {
		if(dbus_message_is_method_call(msg, DBUS_INTERFACE_PROPERTIES, "Get") && dbus_message_has_signature(msg, "ss")) {
			prop_get(obj, msg);
			return DBUS_HANDLER_RESULT_HANDLED;
		} else if(dbus_message_is_method_call(msg, DBUS_INTERFACE_PROPERTIES, "GetAll") && dbus_message_has_signature(msg, "s")) {
			prop_getall(obj, msg);
			return DBUS_HANDLER_RESULT_HANDLED;
		} else if(dbus_message_is_method_call(msg, DBUS_INTERFACE_PROPERTIES, "Set") && dbus_message_has_signature(msg, "ssv")) {
			prop_set(obj, msg);
			return DBUS_HANDLER_RESULT_HANDLED;
		}
	}

	if(route_method(obj, msg))
		return DBUS_HANDLER_RESULT_HANDLED;

unhandled:
	yinfo("Unhandled method call: %s %s.%s(%s)", dbus_message_get_path(msg),
		dbus_message_get_interface(msg), dbus_message_get_member(msg), dbus_message_get_signature(msg));
	return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
}


static void handle_unregister(DBusConnection *con, void *data) {
	if(data)
		free(((dbo_t *)data)->path);
}


/* Register the dbo handlers to dbuscon. */
void dbo_init() {
	vtable.message_function = handle_message;
	vtable.unregister_function = handle_unregister;
	dbus_connection_register_fallback(dbuscon, "/", &vtable, NULL);
}


/* Export a new object at the given path. */
void dbo_register(dbo_t *o, const char *path, const dbo_reg_t *regs, int numregs) {
	int i;

	o->path = strdup(path);
	o->regs = regs;
	o->numregs = numregs;

	o->hasprops = false;
	for(i=0; !o->hasprops && i<numregs; i++)
		if(regs[i].itf->n_properties > 0)
			o->hasprops = true;

	dbus_connection_register_object_path(dbuscon, o->path, &vtable, o);
}


void dbo_prop_changed(dbo_prop_changed_t props, void *obj, const dbo_interface_t *itf, void *table) {
	int i;
	if(!props.n)
		return;

	DBusMessageIter iter, sub, sub2, sub3;
	DBusMessage *msg = dbus_message_new_signal(((dbo_t*)obj)->path, DBUS_INTERFACE_PROPERTIES, "PropertiesChanged");
	dbus_message_iter_init_append(msg, &iter);
	dbus_message_iter_append_basic(&iter, DBUS_TYPE_STRING, &itf->name);
	dbus_message_iter_open_container(&iter, DBUS_TYPE_ARRAY, "{sv}", &sub);
	if(table) {
		for(i=0; i<props.n; i++) {
			dbus_message_iter_open_container(&sub, DBUS_TYPE_DICT_ENTRY, NULL, &sub2);
			dbus_message_iter_append_basic(&sub2, DBUS_TYPE_STRING, &(itf->properties[props.l[i]].name));
			dbus_message_iter_open_container(&sub2, DBUS_TYPE_VARIANT, itf->properties[props.l[i]].type, &sub3);
			itf->properties[props.l[i]].getter(table, &sub3);
			dbus_message_iter_close_container(&sub2, &sub3);
			dbus_message_iter_close_container(&sub, &sub2);
		}
	}
	dbus_message_iter_close_container(&iter, &sub);
	dbus_message_iter_open_container(&iter, DBUS_TYPE_ARRAY, "s", &sub);
	if(!table)
		for(i=0; i<props.n; i++)
			dbus_message_iter_append_basic(&sub, DBUS_TYPE_STRING, &(itf->properties[props.l[i]].name));
	dbus_message_iter_close_container(&iter, &sub);
	dbo_sendfree(msg);
}

/* vim: set noet sw=4 ts=4: */
