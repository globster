/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <global.h>


adc_sid_t adc_str2sid(const char *str) {
	int i;
	adc_sid_t r = 0;

	for(i=0; i<4; i++) {
		if(!adc_isbase32(str[i]))
			return -1;
		r = (r << 5) | (str[i] <= '9' ? 26+str[i]-'2' : str[i]-'A');
	}
	return r;
}


void adc_sid2str(adc_sid_t sid, char *dst) {
	static const char alphabet[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
	int i;

	for(i=0; i<4; i++)
		dst[i] = alphabet[(sid >> ((3-i)*5)) & 0x1F];
	dst[4] = 0;
}


void adc_escape_kstr(kstring_t *dest, const char *str) {
	while(*str) {
		switch(*str) {
		case ' ':
			kputs("\\s", dest);
			break;
		case '\n':
			kputs("\\n", dest);
			break;
		case '\\':
			kputs("\\\\", dest);
			break;
		default:
			kputc(*str, dest);
		}
		str++;
	}
}


bool adc_unescape_kstr(kstring_t *dest, const char *str) {
	while(*str && *str != ' ' && *str != '\n') {
		if(*str != '\\') {
			kputc(*str, dest);
			str++;
			continue;
		}
		str++;
		switch(*str) {
		case 's':
			kputc(' ', dest);
			break;
		case 'n':
			kputc('\n', dest);
			break;
		case '\\':
			kputc('\\', dest);
			break;
		default:
			return false;
		}
		str++;
	}
	return true;
}




/* Returns the length (in bytes) of the UTF-8 symbol under *str. Returns 0 on
 * invalid, truncated or overlong input. */
static int adc_utf8_len(const char *str, int len) {
	if(len <= 0)
		return 0;
	if(!(*str & 0x80))
		return 1;
#define S(x) ((x & 0xC0) == 0x80)
	if((*str & 0xE0) == 0xC0) /* two bytes */
		return len >= 2 && S(str[1]) && (*str & 0x1e) != 0 ? 2 : 0;
	if((*str & 0xF0) == 0xE0) /* three bytes */
		return len >= 3 && S(str[1]) && S(str[2]) && ((*str & 0x0f) != 0 || (str[1] & 0x20) != 0) ? 3 : 0;
	if((*str & 0xF8) == 0xF0) /* four bytes */
		return len >= 4 && S(str[1]) && S(str[2]) && S(str[3]) && ((*str & 0x07) != 0 || (str[1] & 0x30) != 0) ? 4 : 0;
	if((*str & 0xFC) == 0xF8) /* five bytes */
		return len >= 5 && S(str[1]) && S(str[2]) && S(str[3]) && S(str[4]) && ((*str & 0x03) != 0 || (str[1] & 0x38) != 0) ? 5 : 0;
	if((*str & 0xFE) == 0xFC) /* six bytes */
		return len >= 6 && S(str[1]) && S(str[2]) && S(str[3]) && S(str[4]) && S(str[5]) && ((*str & 0x01) != 0 || (str[1] & 0x3C) != 0) ? 6 : 0;
#undef S
	return 0;
}


/* Returns -1 on failure, since 0 is a valid parameter length. Doesn't validate
 * parameter names, since there's no general way to figure out which parameters
 * are positional and which are named. */
static int adc_validate_param(const char *param, int len) {
	const char *cur = param;
	int n;
	while(len > 0 && *cur != ' ' && *cur != '\n') {
		/* Must be a printable UTF-8 character */
		if(((unsigned char)*cur < 32 && *cur != '\t' && *cur != '\r') || (n = adc_utf8_len(cur, len)) <= 0)
			return -1;
		if(*cur != '\\') {
			cur += n;
			len -= n;
			continue;
		}
		if(len < 2 || (cur[1] != 's' && cur[1] != 'n' && cur[1] != '\\'))
			return -1;
		cur += 2;
		len -= 2;
	}
	return cur-param;
}


int adc_validate(const char *cmd, int len) {
	char type;
	const char *cur = cmd;
	int n;

	/* Special-case the empty message */
	if(len >= 1 && *cmd == '\n')
		return 1;

	if(len < 5)
		return 0;

	/* Command Type + name */
	type = *cur;
	if(!adc_iscmdtype(type) || !adc_isupalpha(cur[1]) || !adc_isupalphanum(cur[2]) || !adc_isupalphanum(cur[3]))
		return 0;
	cur += 4;
	len -= 4;

	/* my_sid & target_sid */
#define checksid do {\
		if(len < 5 || *cur != ' ' || !adc_isbase32(cur[1]) || !adc_isbase32(cur[2]) || !adc_isbase32(cur[3]) || !adc_isbase32(cur[4]))\
			return 0;\
		len -= 5; cur += 5;\
	} while(0)
	if(adc_hassrc(type))
		checksid;
	if(adc_hasdest(type))
		checksid;
#undef checksid

	/* U-type CID */
	if(type == 'U') {
		if(len < 2 || *cur != ' ' || !adc_isbase32(cur[1]))
			return 0;
		len -= 2; cur += 2;
		while(len > 0 && *cur != ' ' && *cur != '\n') {
			if(!adc_isbase32(*cur))
				return 0;
			len--; cur++;
		}
	}

	/* TODO: Actually validate the feature list? */
	if(type == 'F') {
		if(len < 6 || *cur != ' ')
			return 0;
		cur++; len--;
		if((n = adc_validate_param(cur, len)) == -1)
			return 0;
		cur += n; len -= n;
	}

	/* Params */
	while(len > 0 && *cur == ' ') {
		cur++; len--;
		if((n = adc_validate_param(cur, len)) == -1)
			return 0;
		cur += n; len -= n;
	}

	return len > 0 && *cur == '\n' ? cur-cmd+1 : 0;
}


char *adc_param_start(const char *cmd) {
	char type = *cmd;
	if(*cmd == '\n')
		return NULL;
	cmd += 4 + (adc_hassrc(type) ? 5 : 0) + (adc_hasdest(type) ? 5 : 0);
	if(type == 'F' || type == 'U') {
		do
			cmd++;
		while(*cmd != '\n' && *cmd != ' ');
	}
	return *cmd == ' ' ? (char *)cmd+1 : NULL;
}




void adc_create(kstring_t *dest, char type, adc_cmd_t cmd, adc_sid_t src, adc_sid_t dst) {
	char sid[5];
	kputc(type, dest);
	kputc((cmd >> 16) & 0xFF, dest);
	kputc((cmd >>  8) & 0xFF, dest);
	kputc((cmd >>  0) & 0xFF, dest);
	if(adc_hassrc(type)) {
		kputc(' ', dest);
		adc_sid2str(src, sid);
		kputs(sid, dest);
	}
	if(adc_hasdest(type)) {
		kputc(' ', dest);
		adc_sid2str(dst, sid);
		kputs(sid, dest);
	}
}

/* vim: set noet sw=4 ts=4: */
