/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef UTIL_LIST_H
#define UTIL_LIST_H

/* All macros assume that the next/prev pointers are embedded in the structure
 * of each node, and that they are named 'next' and 'prev'. All macros take a
 * `list' argument, which is a struct that contains a `head' and `tail'
 * pointer.
 *
 * All macros starting with 's' (e.g. slist_ instead of list_) operate on
 * singly-linked lists. The nodes only need to have a 'next' pointer.
 *
 * All macros containing the 'h' (hlist_, shlist_) flag take only a single
 * pointer variable as list argument rather than a struct. This pointer is
 * treated as the `head' of a list.
 *
 * All macros containing the 'p' flag (plist_, splist_) accept an additional
 * prefix argument. This argument is prefixed to the names of the 'next' and
 * 'prev' pointers.
 *
 * All pointers point directly to the outer-most structure of the node and not
 * to some specially-embedded structure. The latter is the approach taken, for
 * example, in the Linux kernel. (https://lwn.net/Articles/336255/)
 *
 * Circular lists are not supported. Lists should be zero-initialized (i.e.
 * NULL,NULL) before use.  These macros do not keep a count of the number of
 * items in the list.
 *
 * Note that only the macros have been implemented that I actually use, which
 * isn't much. This header file isn't really suitable for generic use at the
 * moment.
 */


/* Insert an element in the list before _next.
 * To push an element to the head of the list: list_insert_before(l, n, l.head)
 */
#define list_insert_before(_l, _n, _next) do {\
		(_n)->next = (_next);\
		(_n)->prev = (_next) ? (_next)->prev : NULL;\
		if((_n)->next) (_n)->next->prev = (_n);\
		else           (_l).tail = (_n);\
		if((_n)->prev) (_n)->prev->next = (_n);\
		else           (_l).head = (_n);\
	} while(0)


/* Remove an element from the list.
 * To remove the first element: list_remove(l, l.head);
 * To remove the last element:  list_remove(l, l.tail);
 */
#define list_remove(_l, _n) do {\
		if((_n)->next) (_n)->next->prev = (_n)->prev;\
		if((_n)->prev) (_n)->prev->next = (_n)->next;\
		if((_n) == (_l).head) (_l).head = (_n)->next;\
		if((_n) == (_l).tail) (_l).tail = (_n)->prev;\
	} while(0)


#define hlist_insert_before(_l, _n, _next) do {\
		(_n)->next = (_next);\
		(_n)->prev = (_next) ? (_next)->prev : NULL;\
		if((_n)->next) (_n)->next->prev = (_n);\
		if((_n)->prev) (_n)->prev->next = (_n);\
		else           (_l) = (_n);\
	} while(0)


#define hlist_remove(_l, _n) do {\
		if((_n)->next) (_n)->next->prev = (_n)->prev;\
		if((_n)->prev) (_n)->prev->next = (_n)->next;\
		if((_n) == (_l)) (_l) = (_n)->next;\
	} while(0)


#endif
/* vim: set noet sw=4 ts=4: */
