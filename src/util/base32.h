/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


/* These base32 utility functions do NOT add or handle trailing '=' characters. */

#ifndef UTIL_BASE32_H
#define UTIL_BASE32_H


/* Tests whether a zero-terminated string is a valid base32-encoded string. */
static inline bool isbase32(const char *s) {
	return strspn(s, "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567") == strlen(s);
}


/* Tests whether a zero-terminated string is a valid encoding of a 24-byte
 * value (e.g. a tiger or TTH hash value). The  */
static inline bool isbase32_24(const char *s) {
	return isbase32(s) && strlen(s) == 39 && s[38] >= 'A' && s[38] <= 'Y';
}


static inline bool isbase32_32(const char *s) {
	return isbase32(s) && strlen(s) == 52 && s[51] >= 'A' && s[51] <= 'Q';
}


/* Given the length of a base32-encoded string, calculate the length of its
 * binary version */
static inline int base32_binlength(int l) {
	return (l*5)/8;
}


/* from[len] (binary) -> to[ceil(len*8/5)+1] (ascii, a trailing zero will be added). */
void base32_encode(const char *from, char *to, int len);


/* from[ceil(len*8/5)] (ascii) -> to[len] (binary) */
void base32_decode(const char *from, char *to, int len);

#endif
/* vim: set noet sw=4 ts=4: */
