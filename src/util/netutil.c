/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <global.h>


void net_sockaddr_str(int af, const void *src, char *dst) {
	if(af == AF_INET) {
		inet_ntop(AF_INET, &((struct sockaddr_in *)src)->sin_addr, dst, MAX_ADDRSTRLEN);
		snprintf(dst+strlen(dst), MAX_ADDRSTRLEN-strlen(dst), ":%"PRIu16, ntohs(((struct sockaddr_in *)src)->sin_port));
	} else {
		*dst = '[';
		inet_ntop(AF_INET6, &((struct sockaddr_in6 *)src)->sin6_addr, dst+1, MAX_ADDRSTRLEN-1);
		snprintf(dst+strlen(dst), MAX_ADDRSTRLEN-strlen(dst), "]:%"PRIu16, ntohs(((struct sockaddr_in6 *)src)->sin6_port));
	}
}


int net_socket(int domain, int type, int protocol) {
	int s = socket(domain, type, protocol);
	if(s == -1)
		return s;
	int fl = fcntl(s, F_GETFL, 0);
	if(fl == -1 || fcntl(s, F_SETFL, fl | O_NONBLOCK) == -1) {
		close(s);
		return -1;
	}
	return s;
}




static void net_getaddrinfo_done(evtp_work_t *work) {
	net_getaddrinfo_t *ctx = (net_getaddrinfo_t *)work;
	ctx->cb(ctx);
	free(ctx);
}


static void net_getaddrinfo_work(evtp_work_t *work) {
	net_getaddrinfo_t *ctx = (net_getaddrinfo_t *)work;
	char service[8];
	snprintf(service, sizeof(service), "%d", (int)ctx->service);
	ctx->ret = getaddrinfo(ctx->node, service, &ctx->hints, &ctx->lst);
}


net_getaddrinfo_t *net_getaddrinfo(const char *node, uint16_t service, const struct addrinfo *hints, net_getaddrinfo_func_t cb) {
	assert(node != NULL);
	net_getaddrinfo_t *ctx = calloc(1, offsetof(net_getaddrinfo_t, node) + 1 + strlen(node));
	ctx->cb = cb;
	ctx->service = service;
	strcpy(ctx->node, node);

	/* Defaults as specified in getaddrinfo(3) */
	if(!hints) {
		ctx->hints.ai_socktype = 0;
		ctx->hints.ai_protocol = 0;
		ctx->hints.ai_family = AF_UNSPEC;
		ctx->hints.ai_flags = AI_V4MAPPED | AI_ADDRCONFIG;
	} else
		ctx->hints = *hints;
	ctx->hints.ai_flags |= AI_NUMERICSERV;

	evtp_submit(&ctx->work, threadpool, net_getaddrinfo_work, net_getaddrinfo_done);
	return ctx;
}



/* TODO: Implement a Happy Eyeballs algorithm? */

/* Frees any associated data and runs the callback with the given status. */
static void net_connect_done(net_connect_t *c, net_connect_status_t status, int val, struct addrinfo *addr) {
	ev_timer_stop(EV_DEFAULT_UC_ &c->timer);
	ev_io_stop(EV_DEFAULT_UC_ &c->io);

	if(c->gai) {
		c->gai->work.data = NULL;
		c->gai = NULL;
	}

	if(c->sock >= 0) {
		close(c->sock);
		c->sock = -1;
	}

	/* Don't call freeaddrinfo() before running the callback, since *addr may
	 * point to it. */
	struct addrinfo *lst = c->lst;
	c->lst = NULL;

	net_connect_func_t cb = c->cb;
	c->cb = NULL;
	if(cb)
		cb(c, status, val, addr);

	if(lst)
		freeaddrinfo(lst);
}


static void net_connect_try(net_connect_t *c);

static void net_connect_handle(net_connect_t *c, int err) {
	if(!err) {
		int sock = c->sock;
		c->sock = -1;
		net_connect_done(c, NET_CONNECT_DONE, sock, c->cur);
		return;
	}

	c->cur = c->cur->ai_next;
	if(c->sock >= 0)
		close(c->sock);
	c->sock = -1;

	if(!c->cur) { /* No more addresses */
		errno = err;
		net_connect_done(c, NET_CONNECT_ERROR_CONNECT, 0, NULL);
	} else
		net_connect_try(c); /* This may cause recursion. */
}


static bool net_connect_bind(net_connect_t *c, int af) {
	int r = 0;
	if(af == AF_INET) {
		struct sockaddr_in empty = {};
		r = memcmp(&c->bind4, &empty, sizeof(empty)) != 0 ? bind(c->sock, &c->bind4, (socklen_t)sizeof(empty)) : 0;
	} else { /* AF_INET6 */
		struct sockaddr_in6 empty = {};
		r = memcmp(&c->bind6, &empty, sizeof(empty)) != 0 ? bind(c->sock, &c->bind6, (socklen_t)sizeof(empty)) : 0;
	}
	return r == 0;
}


static void net_connect_res(EV_P_ ev_io *io, int revents) {
	net_connect_t *c = io->data;
	ev_io_stop(EV_A_ io);

	int err = 0;
	socklen_t len = sizeof(err);

	getsockopt(c->sock, SOL_SOCKET, SO_ERROR, &err, &len);
	net_connect_handle(c, err);
}


static void net_connect_try(net_connect_t *c) {
	struct addrinfo *cur = c->cur;
	assert(cur != NULL);
	assert(c->sock == -1);

	c->cb(c, NET_CONNECT_PROGRESS_CONNECT, 0, cur);

	c->sock = net_socket(cur->ai_family, cur->ai_socktype, cur->ai_protocol);
	if(c->sock == -1) {
		net_connect_handle(c, errno);
		return;
	}

	if(!net_connect_bind(c, cur->ai_family)) {
		net_connect_done(c, NET_CONNECT_ERROR_BIND, 0, NULL);
		return;
	}

	int r;
	do
		r = connect(c->sock, cur->ai_addr, cur->ai_addrlen);
	while(r == -1 && errno == EINTR);

	/* Immediate error or success */
	if(r == 0 || errno != EINPROGRESS) {
		net_connect_handle(c, r == 0 ? 0 : errno);
		return;
	}

	/* Otherwise, queue for writing */
	ev_io_set(&c->io, c->sock, EV_WRITE);
	ev_io_start(EV_DEFAULT_UC_ &c->io);
}


static void net_connect_gai(net_getaddrinfo_t *gai) {
	net_connect_t *c = gai->work.data;
	if(!c) { /* Cancelled */
		if(gai->lst)
			freeaddrinfo(gai->lst);
		return;
	}
	c->gai = NULL;
	if(gai->ret || !gai->lst)
		net_connect_done(c, NET_CONNECT_ERROR_DNS, gai->ret, NULL);
	else {
		c->lst = c->cur = gai->lst;
		c->cb(c, NET_CONNECT_PROGRESS_DNS, 0, gai->lst);
		net_connect_try(c);
	}
}


static void net_connect_timeout(EV_P_ ev_timer *timer, int status) {
	net_connect_done(timer->data, NET_CONNECT_ERROR_TIMEOUT, 0, NULL);
}


void net_connect(net_connect_t *c, const char *host, uint16_t port, net_connect_func_t cb) {
	assert(cb != NULL);
	assert(host != NULL);

	c->cb = cb;
	c->sock = -1;
	ev_timer_init(&c->timer, net_connect_timeout, c->timeout <= 0.0 ? 120.0 : c->timeout, 0);
	ev_timer_start(EV_DEFAULT_UC_ &c->timer);
	c->timer.data = c;
	ev_init(&c->io, net_connect_res);
	c->io.data = c;

	struct addrinfo hint = {};
	hint.ai_family = AF_UNSPEC;
	hint.ai_socktype = SOCK_STREAM;
	hint.ai_flags = AI_ADDRCONFIG | AI_NUMERICSERV;
	c->gai = net_getaddrinfo(host, port, &hint, net_connect_gai);
	c->gai->work.data = c;
}


void net_connect_cancel(net_connect_t *c) {
	net_connect_done(c, NET_CONNECT_CANCELLED, 0, NULL);
}


/* vim: set noet sw=4 ts=4: */
