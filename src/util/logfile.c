/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <global.h>


struct logfile_t {
	logfile_t *next, *prev;
	char *fn;
	int fd;
	struct stat st;
};

static logfile_t *logfile_list;


/* (Re)opens the log file and checks for inode and file size changes. */
static void logfile_checkfile(logfile_t *l) {
	struct stat st;

	if(l->fd >= 0 && (stat(l->fn, &st) < 0
		|| (l->st.st_ino != st.st_ino || l->st.st_size > st.st_size))) {
		/* If we were unable to stat our log file, assume it has been rotated
		 * and recreate it.  If our log file has either been truncated or
		 * replaced with a different file, let's reopen it as well. */
		close(l->fd);
		l->fd = -1;
	}

	if(l->fd >= 0) {
		l->st = st;
		return;
	}

	l->fd = open(l->fn, O_WRONLY|O_APPEND|O_CREAT, 0666);

	/* XXX: What do we do if open() failed? */
	if(l->fd < 0)
		return;

	/* Stat the fd in order to fill out l->st with meaningful data for the next
	 * check. */
	if(fstat(l->fd, &st) == 0)
		l->st = st;
	else {
		/* Let's... just try again later. */
		close(l->fd);
		l->fd = -1;
	}
}


logfile_t *logfile_open(const char *fn) {
	logfile_t *l = malloc(sizeof(logfile_t));
	l->fn = strdup(fn);
	l->fd = -1;

	hlist_insert_before(logfile_list, l, logfile_list);
	logfile_checkfile(l);
	return l;
}


void logfile_close(logfile_t *l) {
	hlist_remove(logfile_list, l);

	if(l->fd >= 0)
		close(l->fd);
	free(l->fn);
	free(l);
}


void logfile_log(logfile_t *l, const char *str) {
	logfile_checkfile(l);
	if(l->fd < 0)
		return;

	int len = strlen(str);
	int wr = 0;
	int r;
	/* XXX: What to do if write() failed? */
	while(wr < len && (r = write(l->fd, str+wr, len-wr)) > 0)
		wr += r;
}


static void logfile_reopen(logfile_t *l) {
	if(l->fd >= 0) {
		close(l->fd);
		l->fd = -1;
	}
	logfile_checkfile(l);
}


void logfile_global_reopen() {
	logfile_t *l;
	for(l=logfile_list; l; l=l->next)
		logfile_reopen(l);
}


/* vim: set noet sw=4 ts=4: */
