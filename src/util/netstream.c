/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <global.h>


/* Global TODO:
 * - Would it simplify the code if we got rid of the read queue? It doesn't
 *   make much sense to have more than a single I/O read action queued anyway.
 * - Abstract this stuff away in a separate (non-globster) library
 * - Improvements to allow for reliable rate limiting
 * - sendfile() support
 */


typedef struct nets_queue_t nets_queue_t;

struct nets_queue_t {
	nets_queue_t *next;
	bool tls : 1;        /* if this is a TLS enable/disable toggle */
	bool zlib : 1;       /* if this is a zlib enable/disable toggle */
	nets_sync_t sync : 2;/* 0 if this isn't a sync action */
	bool freebuf : 1;    /* Set if *buf is our property and must be free()d after use */
	bool compressed : 1; /* Set if *buf holds the compressed data */
	void *ptr;           /* buffer given by application for normal I/O, z_streamp for zlib enable, gnutls_session_t for TLS enable */
	void *buf;           /* actual buffer we're using for normal I/O, possibly after some transformations on *ptr */
	void *cb;            /* callback (for actions that have one) */
	size_t val;          /* buffer length of *ptr for normal I/O */
	size_t len;          /* buffer length of *buf for normal I/O */
};

typedef struct {
	nets_queue_t *first, *last;
} nets_queueq_t;


struct nets_t {
	void *data;
	nets_error_cb err;
	nets_destroy_cb destroycb;

	/* Whether an earlier poll has indicated that we can read or write */
	bool rcan : 1, wcan : 1;
	/* Whether we want to poll for reading or writing */
	bool rwant : 1, wwant : 1;

	/* These indicate the "final" state that we will be in if all queued
	 * actions are flushed. Used only for validation of input actions. */
	bool fs_tls : 1, fs_zlibw : 1;

	/* Whether nets_destroy() has been called */
	bool destroyed : 1;

	/* Whether any action is currently being processed */
	bool busy_tls_handshake : 1, busy_tls_bye : 1;

	/* Whether nets_read_cancel() has been called */
	bool read_cancelled : 1;

	/* Reference count on the call depth, if >0, then nets_check() shouldn't do
	 * anything because it will be called in a function higher up the stack in
	 * a bit anyway */
	unsigned int nocheck : 3;

	int sock;

	/* Read/Write io poll */
	ev_io rio, wio;

	/* The queues */
	nets_queueq_t wq, rq;

	/* Thread pools */
	evtp_work_t wtp, rtp; /* Only wtp is currently used */

	/* Return values for the thread pools */
	int werrno, rerrno;
	int wret, wlen, rret, rzret;
	bool rcb;

	/* Last write action in the queue before entering the write thread */
	nets_queue_t *wend;

	/* The "current" TLS session */
	gnutls_session_t tls;

	/* And the "current" zlib write/read streams */
	z_streamp wz, rz;

	/* Intermediate read buffer, used for _unread() functionality and zlib
	 * decompression. The designation of the data in this buffer is one of the
	 * following:
	 * - the TLS pull function (only if busy_tls_handshake = true)
	 * - the zlib decompressor (if rz != NULL)
	 * - the application (if none of the above)
	 * In all cases, the allocated size of the buffer is at least rbuf_size.
	 */
	char *rbuf;
	size_t rbuf_len;
	size_t rbuf_size;
};


/* Maximum number write actions to perform in a single call */
#define NETS_MAXWRITEACT 10

/* Maximum size of a zlib trailer (this is a rather conservative quess) */
#define NETS_MAXZLIBTRAIL 64

/* Default read buffer size when dealing with zlib decompression. */
#define NETS_RBUFLEN (16*1024)

/* If the read buffer is smaller than this, we'll try to read some more data
 * from the network before going to zlib decompression. Must be at least 10
 * bytes or so in order to provide enough data for zlib to be able to actually
 * consume something. */
#define NETS_MINRBUFLEN 1024

#define nets_queue_isio(a) (!(a)->tls && !(a)->zlib && !(a)->sync)

static void nets_check(nets_t *n);


static void nets_write_free(nets_t *n, char *buf, size_t len) {
	free(buf);
}


/* Allocates a new queue item and pushes it to the queue */
static nets_queue_t *nets_enqueue(nets_queueq_t *q) {
	nets_queue_t *a = calloc(1, sizeof(nets_queue_t));
	a->next = NULL;
	if(q->last)
		q->last->next = a;
	else
		q->first = a;
	q->last = a;
	return a;
}


/* Removes the first item from the queue and returns it. Note that an item is
 * only removed from the queue *after* it has been processed, so passing an
 * empty queue here is an error. */
static nets_queue_t *nets_dequeue(nets_queueq_t *q) {
	assert(q->first != NULL);
	nets_queue_t *a = q->first;
	q->first = a->next;
	if(!a->next)
		q->last = NULL;
	return a;
}


/* Synchronise the I/O polling state to n->[rw]want */
static void nets_sync_poll(nets_t *n) {
	if(n->rwant && !ev_is_active(&n->rio))
		ev_io_start(EV_DEFAULT_UC_ &n->rio);
	else if(!n->rwant && ev_is_active(&n->rio))
		ev_io_stop(EV_DEFAULT_UC_ &n->rio);

	if(n->wwant && !ev_is_active(&n->wio))
		ev_io_start(EV_DEFAULT_UC_ &n->wio);
	else if(!n->wwant && ev_is_active(&n->wio))
		ev_io_stop(EV_DEFAULT_UC_ &n->wio);

	ydebug("%p: Polling for %s", n, n->rwant && n->wwant ? "reading & writing" : n->rwant ? "reading" : n->wwant ? "writing" : "nothing");
}


static void nets_do_destroy(nets_t *n) {
	n->rwant = n->wwant = false;
	nets_sync_poll(n);
	ydebug("%p: Destroying", n);

	/* TODO: All this calling the non-io callbacks with NULL nets_t arguments
	 * is silly and probably not really helpful. May want to get rid of that.
	 */
	while(n->wq.first) {
		nets_queue_t *a = nets_dequeue(&n->wq);
		if(a->tls && a->ptr && a->ptr != n->tls)
			gnutls_deinit(a->ptr);
		if(a->zlib && a->ptr && a->ptr != n->wz) {
			deflateEnd(a->ptr);
			free(a->ptr);
		}
		if(a->tls && a->cb)
			((nets_tls_cb)a->cb)(NULL, NULL);
		if(a->sync)
			((nets_sync_cb)(a->cb))(NULL, a->sync);
		else if(nets_queue_isio(a))
			((nets_write_cb)a->cb)(NULL, a->ptr, a->val);
		if(a->freebuf)
			free(a->buf);
		free(a);
	}

	while(n->rq.first) {
		nets_queue_t *a = nets_dequeue(&n->rq);
		if(a->zlib && a->ptr && a->ptr != n->rz) {
			inflateEnd(a->ptr);
			free(a->ptr);
		}
		if(a->sync && a->sync != NETSS_READWRITE)
			((nets_sync_cb)(a->cb))(NULL, a->sync);
		if(nets_queue_isio(a))
			((nets_read_cb)a->cb)(NULL, 0, a->buf, 0);
		free(a);
	}

	if(n->destroycb)
		n->destroycb(n, n->sock);
	else
		close(n->sock);

	if(n->wz) {
		deflateEnd(n->wz);
		free(n->wz);
	}
	if(n->rz) {
		deflateEnd(n->rz);
		free(n->rz);
	}
	if(n->tls)
		gnutls_deinit(n->tls);
	free(n->rbuf);
	free(n);
}


static void nets_rbuf_consume(nets_t *n, size_t len) {
	if(n->rbuf_len > len) {
		n->rbuf_len -= len;
		memmove(n->rbuf, n->rbuf+len, n->rbuf_len);
	} else {
		free(n->rbuf);
		n->rbuf= NULL;
	}
}


static ssize_t nets_tls_pull(gnutls_transport_ptr_t dat, void *buf, size_t len) {
	nets_t *n = dat;

	/* If we've got unread data left, use that. Note that even though
	 * nets_unread() is called in the main thread while the TLS I/O functions
	 * are all called from a thread pool, this does not need a mutex.
	 * nets_unread() requires the read queue to be empty, but it is guaranteed
	 * to not be empty when we're handling an I/O action.
	 * Also note that the _only_ place where we would want to use the read
	 * buffer as TLS input is for the handshake. */
	if(n->busy_tls_handshake && n->rbuf) {
		size_t r = len < n->rbuf_len ? len : n->rbuf_len;
		memcpy(buf, n->rbuf, r);
		nets_rbuf_consume(n, r);
		return r;
	}

	/* TODO: Accounting */
	return read(n->sock, buf, len);
}


static ssize_t nets_tls_push(gnutls_transport_ptr_t dat, const void *buf, size_t len) {
	/* TODO: Accounting */
	nets_t *n = dat;
	return write(n->sock, buf, len);
}




/**
 **   TLS handshake and shutdown
 **/

static void nets_tls_done(evtp_work_t *w) {
	nets_t *n = w->data;
	w->data = NULL;
	if(n->destroyed) {
		nets_check(n);
		return;
	}
	n->nocheck++;

	if(gnutls_error_is_fatal(n->wret)) {
		nets_error_t err = n->wret == GNUTLS_E_PULL_ERROR ? NETSE_READ : n->wret == GNUTLS_E_PUSH_ERROR ? NETSE_WRITE :
			n->busy_tls_handshake ? NETSE_TLS_HANDSHAKE : NETSE_TLS_BYE;
		n->err(n, err, err == NETSE_READ || err == NETSE_WRITE ? n->werrno : n->wret);
	} else if(n->wret != GNUTLS_E_SUCCESS) {
		if(gnutls_record_get_direction(n->tls))
			n->wwant = true;
		else
			n->rwant = true;
	} else {
		if(n->busy_tls_bye) {
			gnutls_deinit(n->tls);
			n->tls = NULL;
		}
		free(nets_dequeue(&n->rq));
		nets_queue_t *q = nets_dequeue(&n->wq);
		n->busy_tls_handshake = n->busy_tls_bye = false;
		if(q->cb)
			((nets_tls_cb)q->cb)(n, n->tls);
		free(q);
	}

	n->nocheck--;
	nets_check(n);
}


static void nets_tls_work(evtp_work_t *w) {
	nets_t *n = w->data;
	n->wret = n->busy_tls_handshake ? gnutls_handshake(n->tls) : gnutls_bye(n->tls, GNUTLS_SHUT_RDWR);
	n->werrno = errno;
}


static void nets_tls_check(nets_t *n) {
	/* All we have to do is start the thread pool when it's not running yet and
	 * we're not polling for I/O */
	if(!n->wtp.data && !n->wwant && !n->rwant) {
		n->wtp.data = n;
		evtp_submit(&n->wtp, threadpool, nets_tls_work, nets_tls_done);
		n->wcan = n->rcan = false;
	}
}


static void nets_tls_start(nets_t *n) {
	assert(n->wq.first && n->wq.first->tls && n->rq.first && n->rq.first->tls);

	if(n->tls) {
		/* Having something in the read buffer will screw up the TLS shutdown.
		 * The application *should* ensure that everything in the read buffer
		 * has been consumed, but it can't easily guarantee this at the moment.
		 * So we'll have to throw the data away for now... */
		if(n->rbuf)
			nets_rbuf_consume(n, n->rbuf_len);
		n->busy_tls_bye = true;
	} else {
		n->busy_tls_handshake = true;
		n->tls = n->wq.first->ptr;
		gnutls_transport_set_ptr(n->tls, n);
		gnutls_transport_set_push_function(n->tls, nets_tls_push);
		gnutls_transport_set_pull_function(n->tls, nets_tls_pull);
	}
	nets_tls_check(n);
}




/**
 **   Read I/O
 **/


/* Simple wrapper around read() and gnutls_record_recv(). Returns the number of
 * bytes read. The return value of the last read operation is in n->rret, and
 * n->rerrno is updated. */
static ssize_t nets_read_net(nets_t *n, char *buf, size_t len) {
	ssize_t r = 0, rd = 0;
	if(!n->tls) {
		do
			r = read(n->sock, buf, len);
		while(r == -1 && errno == EINTR);
		n->rret = r;
		n->rerrno = errno;
		return r > 0 ? r : 0;
	}

	/* gnutls_record_recv() does what it says: it receives a TLS _record_.  Our
	 * receive buffer could be quite a bit larger than a single record, so we
	 * call it multiple times until our buffer is full or there's no more data
	 * to read.
	 * TODO: Avoid another call if the remaining buffer size is below a certain
	 * threshold?
	 */
	rd = 0;
	while(len > 0) {
		r = gnutls_record_recv(n->tls, buf, len);
		if(r == GNUTLS_E_INTERRUPTED)
			continue;
		if(r <= 0)
			break;
		rd += r;
		len -= r;
		buf += r;
	}

	n->rret = r;
	n->rerrno = errno;
	return rd;
}


static void nets_read_fillbuf(nets_t *n) {
	/* No need to read stuff if we already have enough data in the buffer */
	if(n->rbuf && n->rbuf_len >= NETS_MINRBUFLEN)
		return;

	if(!n->rbuf) {
		n->rbuf = malloc(n->rbuf_size);
		n->rbuf_len = 0;
	}
	ssize_t r = nets_read_net(n, n->rbuf+n->rbuf_len, n->rbuf_size-n->rbuf_len);

	n->rbuf_len += r;
	if(!n->rbuf_len) {
		free(n->rbuf);
		n->rbuf = NULL;
	}
}


/* Consumes data from rbuf and writes into the given queue item. Sets n->rzret
 * to the return value of inflate() and n->rcb if something has been written.
 */
static void nets_read_decompress(nets_t *n, nets_queue_t *a) {
	n->rz->next_in = (void*)n->rbuf;
	n->rz->avail_in = n->rbuf_len;

	n->rz->avail_out = (unsigned long)a->len;
	n->rz->next_out = (void*)a->buf;

	unsigned long oldout = n->rz->total_out;
	unsigned long oldin = n->rz->total_in;
	n->rzret = inflate(n->rz, Z_NO_FLUSH);

	if(n->rz->total_out != oldout) {
		a->len = n->rz->total_out - oldout;
		n->rcb = true;
	}
	if(n->rz->total_in != oldin)
		nets_rbuf_consume(n, n->rz->total_in-oldin);
}


static void nets_read_done(evtp_work_t *w) {
	nets_t *n = w->data;
	w->data = NULL;
	if(n->destroyed) {
		nets_check(n);
		return;
	}
	n->nocheck++;

	/* See if we can do a normal dispatch */
	nets_read_t events = 0;
	if(n->read_cancelled) {
		events |= NETSR_CANCELLED;
		n->read_cancelled = false;
	}
	if(n->rz && n->rzret == Z_STREAM_END) {
		events |= NETSR_ZLIB_END;
		inflateEnd(n->rz);
		free(n->rz);
		n->rz = NULL;
	}
	if(n->rret == 0)
		events |= n->tls ? NETSR_TLS_BYE : NETSR_DISCONNECT;
	if(n->rcb || events) {
		nets_queue_t *a = nets_dequeue(&n->rq);
		((nets_read_cb)a->cb)(n, events, a->buf, n->rcb ? a->len : 0);
		free(a);
	}

	/* And handle errors */
	if(!n->destroyed && n->rret < 0 && (n->tls ? gnutls_error_is_fatal(n->rret) : n->rerrno != EAGAIN && n->rerrno != EWOULDBLOCK)) {
		nets_error_t err = !n->tls || n->rret == GNUTLS_E_PULL_ERROR ? NETSE_READ : NETSE_TLS_READ;
		n->err(n, err, err == NETSE_READ ? n->rerrno : n->rret);
	}
	if(!n->destroyed && n->rz && n->rzret != Z_OK && n->rzret != Z_STREAM_END && n->rzret != Z_BUF_ERROR)
		n->err(n, NETSE_ZLIB_INFLATE, n->rzret);

	n->nocheck--;
	nets_check(n);
}


static void nets_read_work(evtp_work_t *w) {
	nets_t *n = w->data;
	nets_queue_t *a = n->rq.first;
	ssize_t r;
	n->rcb = false;

	/* Simple case: we have some data buffered that needs to go to the application */
	if(!n->rz && n->rbuf) {
		r = a->len > n->rbuf_len ? n->rbuf_len : a->len;
		memcpy(a->buf, n->rbuf, r);
		nets_rbuf_consume(n, r);
		n->rret = r;
		n->rcb = true;
		return;
	}

	/* Another simple case: zlib disabled, write directly to the application buffer */
	if(!n->rz) {
		r = nets_read_net(n, a->buf, a->len);
		n->rcb = r > 0;
		if(r > 0)
			a->len = r;
		return;
	}

	/* The super-complex case: zlib enabled, use intermediate buffer */
	nets_read_fillbuf(n);
	if(n->rbuf && n->rbuf_len > 0)
		nets_read_decompress(n, a);
}


static void nets_read_check(nets_t *n) {
	if(n->read_cancelled && !n->rtp.data) {
		nets_queue_t *r = nets_dequeue(&n->rq);
		n->nocheck++;
		((nets_read_cb)(r->cb))(n, NETSR_CANCELLED, r->buf, 0);
		n->nocheck--;
		free(r);
		n->read_cancelled = false;
		n->rwant = false;
	}

	if(!n->rq.first || !nets_queue_isio(n->rq.first))
		return;

	bool havedata =
		   (n->rbuf && !n->rz)
		|| (n->rbuf && n->rz && n->rzret != Z_BUF_ERROR)
		|| (n->tls && gnutls_record_check_pending(n->tls));
	if(!havedata && !n->rcan && !n->rtp.data)
		n->rwant = true;
	if(n->rwant || n->rtp.data)
		return;

	ydebug("%p: Starting read", n);

	/* XXX: In contrast to the write I/O stuff, all read queue items are
	 * processed one at a time.
	 * Similarly, the thread pool is used even when not strictly necessary.
	 * This is because we don't always have to poll for read I/O before running
	 * a callback, i.e. when havedata is true and this function is called from
	 * the context of a nets_read().
	 * So, considering the above, there is some room left for optimizations. */
	n->rcan = false;
	n->rtp.data = n;
	evtp_submit(&n->rtp, threadpool, nets_read_work, nets_read_done);
}




/**
 **   Write I/O
 **/

static void nets_write_setend(nets_t *n) {
	int i = NETS_MAXWRITEACT;
	n->wend = n->wq.first;
	assert(n->wend);
	while(--i > 0 && n->wend->next && nets_queue_isio(n->wend->next))
		n->wend = n->wend->next;
}


static void nets_write_compress(nets_t *n) {
	/* Note that each buffer is compressed individually with Z_SYNC_FLUSH. It
	 * may also be possible to combine the buffers and do a single Z_SYNC_FLUSH
	 * afterwards, but I haven't thouroughly investigated that possibility yet.
	 */
	nets_queue_t *a;
	for(a = n->wq.first; a; a = a == n->wend ? NULL : a->next) {
		if(a->compressed)
			continue;

		/* This would imply that the buffer has already been processed or
		 * partially flushed somewhere, which shouldn't happen because the only
		 * processing currently supported is zlib. */
		assert(!a->freebuf && a->buf == a->ptr);

		/* Very conservative upper bound on the compressed data. We can't
		 * officially use deflateBound() here because it has to be called
		 * directly after deflateInit(). This upper bound is based on the
		 * source of deflateBound().
		 * I suspect that over-allocating and single-pass compression is faster
		 * than more compact allocation and possibly realloc()ing, hence this
		 * solution. It also happens to simplify the code. */
		n->wz->avail_out = a->val + ((a->val+7)>>3) + ((a->val+63)>>6) + 5 + 6 + 5;
		a->buf = malloc(n->wz->avail_out);
		n->wz->next_out = (void*)a->buf;

		n->wz->next_in = (void*)a->ptr;
		n->wz->avail_in = a->val;

		unsigned long last_in = n->wz->total_in;
		unsigned long last_out = n->wz->total_out;

		int r = deflate(n->wz, Z_SYNC_FLUSH);
		assert(r == Z_OK && n->wz->total_in-last_in == a->val);

		a->len = n->wz->total_out - last_out;
		a->freebuf = a->compressed = true;
	}
}


static void nets_write_raw(nets_t *n) {
	struct iovec vec[NETS_MAXWRITEACT];
	int iovcnt = 0;
	nets_queue_t *a;
	for(a = n->wq.first; a; a = a == n->wend ? NULL : a->next) {
		vec[iovcnt].iov_base = a->buf;
		vec[iovcnt].iov_len = a->len;
		iovcnt++;
	}
	assert(iovcnt > 0);

	ssize_t r;
	do
		r = writev(n->sock, vec, iovcnt);
	while(r == -1 && errno == EINTR);

	/* TODO: Accounting */

	n->wret = r;
	n->wlen = r >= 0 ? r : 0;
	n->werrno = errno;
}


static void nets_write_tls(nets_t *n) {
	/* gnutls_record_send() does not support vector writes and tends to return
	 * a short write when the given buffer is larger than ~16k or so. In order
	 * to minimize the thread pool switching and to not strain the main event
	 * loop too much when we're dealing with many smaller buffers or some large
	 * buffers and fast data transfers, we call gnutls_record_send() as many
	 * times as needed until all requested buffers are flushed or until we get
	 * an error.
	 * (This is why we have a separate 'wlen' and 'wret', in order to allow the
	 * thread pool to both flush data and return an error at the same time)
	 */
	ssize_t r = 0;
	size_t w;
	nets_queue_t *a;

	n->wlen = 0;

	for(a = n->wq.first; a; a = a == n->wend ? NULL : a->next) {
		w = 0;
		r = 0;
		while(w < a->len) {
			r = gnutls_record_send(n->tls, a->buf+w, a->len-w);
			if(r == GNUTLS_E_INTERRUPTED)
				continue;
			if(r < 0)
				goto done;
			w += r;
			n->wlen += r;
		}
	}

done:
	n->wret = r;
	n->werrno = errno;
}


/* If some data has been written (n->wlen), update/free the first queue item
 * and run any associated callbacks. Removes the length of this queue item from
 * n->wlen.  */
static void nets_write_dispatch(nets_t *n) {
	nets_queue_t *a = n->wq.first;

	if((size_t)n->wlen < a->len) {
		a->len -= n->wlen;
		n->wlen = 0;
		if(a->freebuf) {
			/* Buffer is under our control, we have to move the data in order
			 * to avoid changing the pointer */
			memmove(a->buf, a->buf+n->wlen, a->len);
		} else {
			/* Now we have *buf point into *ptr, which is more efficient */
			a->buf += n->wlen;
		}
	} else {
		n->wlen -= a->len;
		nets_dequeue(&n->wq);
		((nets_write_cb)a->cb)(n, a->ptr, a->val);
		if(a->freebuf)
			free(a->buf);
		free(a);
	}
}


static void nets_write_done(evtp_work_t *w) {
	nets_t *n = w->data;
	w->data = NULL;
	if(n->destroyed) {
		nets_check(n);
		return;
	}
	n->nocheck++;

	while(!n->destroyed && n->wlen > 0)
		nets_write_dispatch(n);

	if(!n->destroyed && n->wret < 0 && (n->tls ? gnutls_error_is_fatal(n->wret) : n->werrno != EAGAIN && n->werrno != EWOULDBLOCK)) {
		nets_error_t err = !n->tls || n->wret == GNUTLS_E_PUSH_ERROR ? NETSE_WRITE : NETSE_TLS_WRITE;
		n->err(n, err, err == NETSE_WRITE ? n->werrno : n->wret);
	}

	n->nocheck--;
	nets_check(n);
}


static void nets_write_work(evtp_work_t *w) {
	nets_t *n = w->data;

	if(n->wz)
		nets_write_compress(n);

	if(n->tls)
		nets_write_tls(n);
	else
		nets_write_raw(n);
}


static void nets_write_check(nets_t *n) {
	if(!n->wq.first || !nets_queue_isio(n->wq.first))
		return;
	if(!n->wcan && !n->wtp.data)
		n->wwant = true;
	if(n->wwant || n->wtp.data)
		return;

	n->wcan = false;
	nets_write_setend(n);

	ydebug("%p: Starting write", n);
	n->wtp.data = n;
	/* Short-cut a raw write in the main thread (pretending that it's done in a thread pool) */
	if(!n->tls && !n->wz) {
		nets_write_raw(n);
		nets_write_done(&n->wtp);
	} else
		evtp_submit(&n->wtp, threadpool, nets_write_work, nets_write_done);
}




static void nets_zlibw_check(nets_t *n) {
	nets_queue_t *a = n->wq.first;
	if(!a || !a->zlib)
		return;

	if(!n->wz) {
		n->wz = a->ptr;
		nets_dequeue(&n->wq);
	}

	/* When disabling zlib compression, we may have to send some trailing data
	 * in order for the receiving end to get a Z_STREAM_END. In that case, we
	 * generate the remaining footer using zlib and morph this zlib-disable
	 * queue item into a regular I/O write queue item. (We can't insert a new
	 * queue item in front of the list, hence this morphing)
	 *
	 * Note that this currently isn't very efficient, a sequence of
	 * nets_write() queues followed by a nets_zlibw_disable() results in
	 * roughly the following sequence of network writes:
	 *
	 *   Message1<sync>
	 *   Message2<sync>
	 *   <stream_end>
	 *
	 * A more efficient approach would remove the last <sync> and turn that
	 * into a <stream_end>. This is only possible if the application has queued
	 * a nets_write() and nets_zlibw_disable() in direct sequence. See also the
	 * notes about flushing in nets_write_compress().
	 */
	n->wz->avail_in = 0;
	n->wz->next_in = (Bytef *)"";
	char *buf = malloc(NETS_MAXZLIBTRAIL);
	n->wz->avail_out = NETS_MAXZLIBTRAIL;
	n->wz->next_out = (Bytef *)buf;
	int r = deflate(n->wz, Z_FINISH);

	if(r == Z_STREAM_END && n->wz->avail_out != NETS_MAXZLIBTRAIL) {
		a->zlib = false;
		a->cb = nets_write_free;
		a->ptr = a->buf = buf;
		a->val = a->len = NETS_MAXZLIBTRAIL-n->wz->avail_out;
	} else {
		free(buf);
		nets_dequeue(&n->wq);
	}

	deflateEnd(n->wz);
	free(n->wz);
	n->wz = NULL;
}


static void nets_zlibr_check(nets_t *n) {
	nets_queue_t *a = n->rq.first;
	if(!a || !a->zlib)
		return;

	if(!n->rz) {
		/* It's valid for a->ptr to be NULL. This can happen if zlib has
		 * already been disabled with Z_STREAM_END. */
		n->rz = a->ptr;
	} else {
		assert("A zlib read enable action was queued, but zlib is already enabled" && !a->ptr);
		inflateEnd(n->rz);
		free(n->rz);
		n->rz = NULL;
	}
	nets_dequeue(&n->rq);
}


static void nets_sync_check(nets_t *n) {
	nets_queue_t *r = n->rq.first;
	nets_queue_t *w = n->wq.first;

	if(r && r->sync == NETSS_READWRITE && w && w->sync == NETSS_READWRITE) {
		nets_dequeue(&n->rq);
		nets_dequeue(&n->wq);
		n->nocheck++;
		((nets_sync_cb)(r->cb))(n, r->sync);
		n->nocheck--;
		free(r);
		free(w);
		return;
	}

	if(r && r->sync == NETSS_READ) {
		nets_dequeue(&n->rq);
		n->nocheck++;
		((nets_sync_cb)(r->cb))(n, r->sync);
		n->nocheck--;
		free(r);
		return;
	}

	if(w && w->sync == NETSS_WRITE) {
		nets_dequeue(&n->wq);
		n->nocheck++;
		((nets_sync_cb)(w->cb))(n, w->sync);
		n->nocheck--;
		free(w);
		return;
	}
}


/* The main orchestration function. Called when any change to the internal
 * state is made. Will check the current state and determine what action to
 * take next (if any). */
static void nets_check(nets_t *n) {
	if(n->nocheck)
		return;
destroyed:
	if(n->destroyed) {
		/* Wait for the thread pools to finish */
		if(!n->wtp.data && !n->rtp.data)
			nets_do_destroy(n);
		return;
	}

	if(n->busy_tls_bye || n->busy_tls_handshake) {
		nets_tls_check(n);
		goto done;
	}

	/* These checks may immediately dequeue an action, so make sure to run them
	 * multiple times to ensure that all actions that can be processed
	 * immediately are processed immediately. */
	nets_queue_t *rq, *wq;
	do {
		rq = n->rq.first;
		wq = n->wq.first;

		nets_zlibw_check(n);
		nets_zlibr_check(n);
		nets_sync_check(n);
		/* nets_sync_check may run a callback, which may do a nets_destroy() */
		if(n->destroyed)
			goto destroyed;

		nets_write_check(n);

		nets_read_check(n);
		/* nets_read_check may run a callback, too */
		if(n->destroyed)
			goto destroyed;
	} while(rq != n->rq.first || wq != n->wq.first);

	if(n->rq.first && n->rq.first->tls && n->wq.first && n->wq.first->tls) {
		assert("Thread pool can't be active when starting a TLS action" && !n->rtp.data && !n->wtp.data);
		nets_tls_start(n);
	}

done:
	nets_sync_poll(n);
}


static void nets_rio_ready(EV_P_ ev_io *io, int events) {
	nets_t *n = io->data;
	ydebug("%p: Read poll ready", n);
	n->rcan = true;
	n->rwant = false;
	nets_check(n);
}


static void nets_wio_ready(EV_P_ ev_io *io, int events) {
	nets_t *n = io->data;
	ydebug("%p: Write poll ready", n);
	n->wcan = true;
	n->wwant = false;
	nets_check(n);
}




nets_t *nets_create(int sock, nets_error_cb err) {
	nets_t *n = calloc(1, sizeof(nets_t));
	assert("Must set an error callback" && err);

	n->sock = sock;
	n->err = err;
	n->rbuf_size = NETS_RBUFLEN;

	/* Low-level I/O */
	ev_io_init(&n->rio, nets_rio_ready, n->sock, EV_READ);
	ev_io_init(&n->wio, nets_wio_ready, n->sock, EV_WRITE);
	n->rio.data = n->wio.data = n;

	ydebug("%p: Created", n);
	return n;
}


void nets_rbuf_size(nets_t *n, size_t newsize) {
	if(newsize < NETS_MINRBUFLEN)
		newsize = NETS_MINRBUFLEN;
	size_t oldsize = n->rbuf_size;
	n->rbuf_size = newsize;
	if(n->rbuf && oldsize < newsize)
		n->rbuf = realloc(n->rbuf, newsize);
	else if(n->rbuf) {
		/* Avoid realloc() when shrinking. We want to actually free some memory
		 * when lowering the buffer size, and most (all?) realloc()
		 * implementations tend to not put much effort into that. */
		char *buf = malloc(newsize);
		memcpy(buf, n->rbuf, n->rbuf_len);
		free(n->rbuf);
		n->rbuf = buf;
	}
}


void nets_data_set(nets_t *n, void *d) { n->data = d; }
void *nets_data_get(nets_t *n) { return n->data; }


void nets_destroy(nets_t *n, nets_destroy_cb cb) {
	n->destroyed = true;
	n->destroycb = cb;
	nets_check(n);
}


void nets_write(nets_t *n, const char *buf, size_t len, nets_write_cb cb) {
	assert("nets_t object has already been destroyed" && !n->destroyed);
	nets_queue_t *a = nets_enqueue(&n->wq);
	a->cb = cb ? cb : nets_write_free;
	a->ptr = a->buf = (void *)buf;
	a->val = a->len = len;
	nets_check(n);
}


void nets_read(nets_t *n, char *buf, size_t len, nets_read_cb cb) {
	assert("nets_t object has already been destroyed" && !n->destroyed);
	assert("nets_read() can't be called with a NULL callback" && cb);
	nets_queue_t *a = nets_enqueue(&n->rq);
	a->cb = cb;
	a->ptr = a->buf = buf;
	a->val = a->len = len;
	nets_check(n);
}


void nets_read_cancel(nets_t *n) {
	if(!n->rq.first || !nets_queue_isio(n->rq.first))
		return;
	n->read_cancelled = true;
	nets_check(n);
}


void nets_unread(nets_t *n, const char *buf, size_t len) {
	if(!len)
		return;
	assert("nets_t object has already been destroyed" && !n->destroyed);
	assert("Can't unread data when a read action is still queued" && !n->rq.first);
	assert("Can't unread data when there's still unread data left" && !n->rbuf);

	n->rbuf= malloc(len > n->rbuf_size ? len : n->rbuf_size);
	memcpy(n->rbuf, buf, len);
	n->rbuf_len = len;
	/* No need for a nets_check() here, since this isn't a state change */
}


void nets_tls_enable(nets_t *n, gnutls_session_t ses, nets_tls_cb cb) {
	assert("nets_t object has already been destroyed" && !n->destroyed);
	assert("TLS already enabled" && !n->fs_tls);
	assert("Can't use TLS over zlib" && !n->fs_zlibw);
	n->fs_tls = true;

	nets_queue_t *a = nets_enqueue(&n->wq);
	a->tls = true;
	a->cb = cb;
	a->ptr = ses;
	*nets_enqueue(&n->rq) = *a;
	nets_check(n);
}


void nets_tls_disable(nets_t *n, nets_tls_cb cb) {
	assert("nets_t object has already been destroyed" && !n->destroyed);
	assert("TLS already disabled" && n->fs_tls);
	assert("Can't disable TLS while zlib is enabled" && !n->fs_zlibw);
	n->fs_tls = false;

	nets_queue_t *a = nets_enqueue(&n->wq);
	a->tls = true;
	a->cb = cb;
	*nets_enqueue(&n->rq) = *a;
	nets_check(n);
}


void nets_zlibw_enable(nets_t *n, z_streamp z) {
	assert("nets_t object has already been destroyed" && !n->destroyed);
	assert("Zlib for writing already enabled" && !n->fs_zlibw);
	n->fs_zlibw = true;

	nets_queue_t *a = nets_enqueue(&n->wq);
	a->zlib = true;
	if(z) {
		a->ptr = malloc(sizeof(z_stream));
		memcpy(a->ptr, z, sizeof(z_stream));
	} else {
		z = calloc(1, sizeof(z_stream));
		deflateInit(z, Z_DEFAULT_COMPRESSION);
	}
	nets_check(n);
}


void nets_zlibw_disable(nets_t *n) {
	assert("nets_t object has already been destroyed" && !n->destroyed);
	assert("Zlib for writing already disabled" && n->fs_zlibw);
	n->fs_zlibw = false;

	nets_queue_t *a = nets_enqueue(&n->wq);
	a->zlib = true;
	nets_check(n);
}


void nets_zlibr_enable(nets_t *n, z_streamp z) {
	assert("nets_t object has already been destroyed" && !n->destroyed);
	nets_queue_t *a = nets_enqueue(&n->rq);
	a->zlib = true;
	if(z) {
		a->ptr = malloc(sizeof(z_stream));
		memcpy(a->ptr, z, sizeof(z_stream));
	} else {
		z = calloc(1, sizeof(z_stream));
		inflateInit(z);
		a->ptr = z;
	}
	nets_check(n);
}


void nets_sync(nets_t *n, nets_sync_t direction, nets_sync_cb cb) {
	if(direction & NETSS_READ) {
		nets_queue_t *a = nets_enqueue(&n->rq);
		a->sync = direction;
		a->cb = cb;
	}
	if(direction & NETSS_WRITE) {
		nets_queue_t *a = nets_enqueue(&n->wq);
		a->sync = direction;
		a->cb = cb;
	}
	nets_check(n);
}


void nets_zlibr_disable(nets_t *n) {
	assert("nets_t object has already been destroyed" && !n->destroyed);
	nets_queue_t *a = nets_enqueue(&n->rq);
	a->zlib = true;
	nets_check(n);
}


const char *nets_strerror_name(nets_error_t err) {
	switch(err) {
	case NETSE_WRITE:         return "WRITE";
	case NETSE_READ:          return "READ";
	case NETSE_TLS_WRITE:     return "TLS_WRITE";
	case NETSE_TLS_READ:      return "TLS_READ";
	case NETSE_TLS_HANDSHAKE: return "TLS_HANDSHAKE";
	case NETSE_TLS_BYE:       return "TLS_BYE";
	case NETSE_ZLIB_INFLATE:  return "ZLIB_INFLATE";
	}
	return "UNKNOWN";
}


const char *nets_strerror(nets_error_t err, int val) {
	switch(err) {
	case NETSE_WRITE:
	case NETSE_READ:
		return strerror(val);
	case NETSE_TLS_WRITE:
	case NETSE_TLS_READ:
	case NETSE_TLS_HANDSHAKE:
	case NETSE_TLS_BYE:
		return gnutls_strerror(val);
	case NETSE_ZLIB_INFLATE:
		switch(val) {
		case Z_NEED_DICT:    return "No zlib dictionary provided";
		case Z_STREAM_ERROR: return "Corrupted zlib stream structure";
		case Z_DATA_ERROR:   return "Corrupted zlib data";
		}
		/* Other possible errors should have been handled or don't come from
		 * inflate() */
		return "Unknown decompression error";
	}
	return "Unknown error";
}

/* vim: set noet sw=4 ts=4: */
