/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


#ifndef UTIL_NMDC_H
#define UTIL_NMDC_H


/* The returned string should be freed after use. */
char *nmdc_lock2key(const char *lock, int len);


/* A best-effort character conversion function. If, for whatever reason, a
 * character could not be converted, a question mark will be inserted instead.
 * The character sets in 'to' and 'from' are assumed to form a valid conversion
 * according to nmdc_conversion_check().
 */
char *nmdc_convert(const char *to, const char *from, const char *str, int len);


/* Test that iconv() conversion is possible from UTF-8 to enc and back.  Also
 * tests (not very comprehensively) that the encoding does not contain zero
 * bytes, since I can't handle those. */
bool nmdc_convert_check(const char *enc);


/* Unescapes the string and converts it to UTF-8. */
char *nmdc_unescape(const char *enc, const char *str, int len);


/* Escapes the string and converts it to the hub encoding */
char *nmdc_escape(const char *enc, const char *str, int len);




typedef struct {
	/* In hub encoding, use nmdc_convert() if you want UTF-8 */
	char *nick;
	/* All other string fields are UTF-8, and may be NULL if empty */
	char *desc;
	char *conn;
	char *mail;
	char *client; /* Includes the version, without the "V:" part. E.g. "<++ V:0.2,.." -> "++ 0.2". */
	uint8_t hn, hr, ho;
	uint8_t slots;
	char flag;
	char mode; /* 0 = not present, otherwise 'A', 'P' or '5' */
	uint32_t as; /* auto-open-slot, in bytes/s */
	uint64_t share;
} nmdc_myinfo_t;


static inline void nmdc_myinfo_free(nmdc_myinfo_t *n) {
	free(n->nick);
	free(n->desc);
	free(n->conn);
	free(n->mail);
	free(n->client);
}


/* Parses a "$MyINFO ..|" string and puts the result in *nfo. This function
 * does not read past the trailing '|' character, so the buffer does not have
 * to be zero-terminated. Returns false on error. */
bool nmdc_myinfo_parse(nmdc_myinfo_t *n, const char *enc, const char *buf);




/* Tries to guess the upload speed, in bytes/s, from a $MyINFO connection
 * string. Not very reliable, but neither is the user who specified his
 * connection string. :-) */
uint32_t nmdc_conn2speed(const char *conn);


/* Parses a chat message. Does not read past the trailing '|' character.
 * Returns the offset of the chat message. *nickoff points to the offset of
 * the nick name of the chat message, or NULL of no nick could be identified.
 * The length of the nick is written to *nicklen, and *me holds whether this
 * was a first-person message or not.
 * Parses:
 *   <nick> Message|
 *   ** nick Message|
 *   Message|
 */
char *nmdc_chat_parse(const char *msg, char **nickoff, int *nicklen, bool *me);


#endif
/* vim: set noet sw=4 ts=4: */
