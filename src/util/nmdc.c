/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <global.h>


char *nmdc_lock2key(const char *lock, int len) {
	kstring_t key = {};
	char *tmp = malloc(len);
	int i;

	tmp[0] = lock[0] ^ lock[len-1] ^ lock[len-2] ^ 5;
	for(i=1; i<len; i++)
		tmp[i] = lock[i] ^ lock[i-1];

	for(i=0; i<len; i++) {
		char v = ((tmp[i]<<4) & 0xF0) | ((tmp[i]>>4) & 0x0F);
		if(v == 0 || v == 5 || v == 36 || v == 96 || v == 124 || v == 126)
			ksprintf(&key, "/%%DCN%03d%%/", (int)v);
		else
			kputc(v, &key);
	}
	free(tmp);
	return key.s;
}


/* Based on str_convert() from ncdc. */
char *nmdc_convert(const char *to, const char *from, const char *str, int len) {
	iconv_t cd = iconv_open(to, from);
	if(cd == (iconv_t)-1) {
		yerror("iconv_open('%s', '%s') failed: %s", to, from, strerror(errno));
		return strdup("<encoding-error>");
	}

	size_t inlen = len >= 0 ? (size_t)len : strlen(str);
	size_t outlen = inlen+96;
	size_t outsize = inlen+100;
	char *inbuf = (char *)str;
	char *dest = malloc(outsize);
	char *outbuf = dest;
	while(inlen > 0) {
		size_t r = iconv(cd, &inbuf, &inlen, &outbuf, &outlen);
		if(r != (size_t)-1)
			continue;
		if(errno == E2BIG) {
			size_t used = outsize - outlen - 4;
			outlen += outsize;
			outsize += outsize;
			dest = realloc(dest, outsize);
			outbuf = dest + used;
		} else if(errno == EILSEQ || errno == EINVAL) {
			// skip this byte from the input
			inbuf++;
			inlen--;
			// Only output question mark if we happen to have enough space, otherwise
			// it's too much of a hassle...  (In most (all?) cases we do have enough
			// space, otherwise we'd have gotten E2BIG anyway)
			if(outlen >= 1) {
				*outbuf = '?';
				outbuf++;
				outlen--;
			}
		} else {
			yerror("Unknown error from iconv(): %s", strerror(errno));
			return strdup("<encoding-error>");
		}
	}
	memset(outbuf, 0, 4);
	iconv_close(cd);
	return dest;
}


/* Based on str_convert_check() from ncdc */
bool nmdc_convert_check(const char *enc) {
	char *utf8buf = "abc";
	char _encbuf[24] = {};
	char _decbuf[24] = {};
	char *encbuf = _encbuf;
	char *decbuf = _decbuf;
	size_t r;
	size_t utf8len = strlen(utf8buf);
	size_t enclen = sizeof(_encbuf);
	size_t declen = sizeof(_decbuf);
	iconv_t cd;

	cd = iconv_open(enc, "UTF-8");
	if(cd == (iconv_t)-1)
		return false;
	r = iconv(cd, &utf8buf, &utf8len, &encbuf, &enclen);
	iconv_close(cd);
	encbuf = _encbuf;
	enclen = sizeof(_encbuf) - enclen;
	if(r == (size_t)-1 || utf8len != 0 || enclen == 0 || strlen(encbuf) != enclen)
		return false;

	cd = iconv_open("UTF-8", enc);
	if(cd == (iconv_t)-1)
		return false;
	r = iconv(cd, &encbuf, &enclen, &decbuf, &declen);
	iconv_close(cd);
	decbuf = _decbuf;
	declen = sizeof(_decbuf) - declen;
	if(r == (size_t)-1 || enclen != 0 || declen != 3 || strcmp(decbuf, "abc") != 0)
		return false;

	return true;
}


char *nmdc_unescape(const char *enc, const char *str, int len) {
	if(len < 0)
		len = strlen(str);
	kstring_t dest = {};
	while(len > 0) {
		if(*str == '&') {
			if(len >= 5 && strncmp(str, "&#36;", 5) == 0) {
				kputc('$', &dest);
				len -= 5;
				str += 5;
				continue;
			}
			if(len >= 6 && strncmp(str, "&#124;", 6) == 0) {
				kputc('|', &dest);
				len -= 6;
				str += 6;
				continue;
			}
			if(len >= 5 && strncmp(str, "&amp;", 5) == 0) {
				kputc('&', &dest);
				len -= 5;
				str += 5;
				continue;
			}
		}
		kputc(*str, &dest);
		len--;
		str++;
	}
	char *dec = nmdc_convert("UTF-8", enc, dest.s ? dest.s : "", dest.l);
	free(dest.s);
	return dec;
}


char *nmdc_escape(const char *enc, const char *str, int len) {
	char *buf = nmdc_convert(enc, "UTF-8", str, len);
	kstring_t dest = {};
	char *tmp = buf;
	while(*tmp) {
		switch(*tmp) {
		case '$': kputs("&#36;", &dest);  break;
		case '|': kputs("&#124;", &dest); break;
		case '&':
			if(strncmp(tmp, "&amp;", 5) == 0 || strncmp(tmp, "&#36;", 5) == 0 || strncmp(tmp, "&#124;", 6) == 0)
				kputs("&amp;", &dest);
			else
				kputc(*tmp, &dest);
			break;
		default:
			kputc(*tmp, &dest);
		}
		tmp++;
	}
	free(buf);
	return dest.s ? dest.s : calloc(1,1);
}




/* Parses a tag. The buffer points to the start of the description field, len
 * indicates the length of this field (buf[len] == '$'). Returns the new length
 * of the actual description if a tag was found, the unmodified length if no
 * valid tag was found. */
static int nmdc_myinfo_parse_tag(nmdc_myinfo_t *n, const char *buf, int len) {
	if(!len || buf[len-1] != '>')
		return len;
	const char *tmp = buf+len;
	for(tmp--; tmp>=buf; tmp--)
		if(*tmp == '<')
			break;
	if(*tmp != '<')
		return len;
	int ret = (int)(tmp-buf);

	/* Client */
	tmp++;
	kstring_t client = {};
	while(*tmp != '<' && *tmp != '>' && *tmp >= 0x21 && *tmp < 0x7e) { /* Printable ASCII, with some exceptions */
		kputc(*tmp, &client);
		tmp++;
	}
	if(*tmp != ' ') {
		free(client.s);
		return len;
	}
	tmp++;

	/* Version (assumed to always be the first in the list, if present) */
	if(*tmp == 'V' && tmp[1] == ':') {
		tmp += 2;
		kputc(' ', &client);
		while(*tmp != '<' && *tmp != '>' && *tmp != ',' && *tmp >= 0x21 && *tmp < 0x7e) {
			kputc(*tmp, &client);
			tmp++;
		}
	}
	n->client = client.s;

	/* Rest */
	while(*tmp != '>') {
		while(*tmp != '>' && *tmp != ',')
			tmp++;
		if(*tmp == '>')
			break;
		tmp++;

		/* Mode */
		if(*tmp == 'M' && tmp[1] == ':' && (tmp[2] == 'A' || tmp[2] == 'P' || tmp[2] == '5')) {
			n->mode = tmp[2];
			tmp += 3;
			continue;
		}

		/* Hub counts */
		if(*tmp == 'H' && tmp[1] == ':') {
			tmp += 2;
			buf = NULL;
			unsigned long v = strtoul(tmp, (char **)&buf, 10);
			if(!buf || tmp == buf || *buf != '/')
				continue;
			n->hn = v > UINT8_MAX ? UINT8_MAX : v;
			tmp = buf+1;
			buf = NULL;
			v = strtoul(tmp, (char **)&buf, 10);
			if(!buf || tmp == buf || *buf != '/')
				continue;
			n->hr = v > UINT8_MAX ? UINT8_MAX : v;
			tmp = buf+1;
			buf = NULL;
			v = strtoul(tmp, (char **)&buf, 10);
			if(!buf || tmp == buf)
				continue;
			n->ho = v > UINT8_MAX ? UINT8_MAX : v;
			tmp = buf;
		}

		/* Slots */
		if(*tmp == 'S' && tmp[1] == ':') {
			tmp += 2;
			buf = NULL;
			unsigned long v = strtoul(tmp, (char **)&buf, 10);
			if(!buf || tmp == buf)
				continue;
			n->slots = v > UINT8_MAX ? UINT8_MAX : v;
			tmp = buf;
		}

		/* Auto-open-slot */
		if(*tmp == 'O' && tmp[1] == ':') {
			tmp += 2;
			buf = NULL;
			unsigned long v = strtoul(tmp, (char **)&buf, 10);
			if(!buf || tmp == buf)
				continue;
			n->as = v >= UINT32_MAX/1024 ? UINT32_MAX : v*1024;
			tmp = buf;
		}
	}

	return ret;
}


bool nmdc_myinfo_parse(nmdc_myinfo_t *n, const char *enc, const char *buf) {
	int len, ilen;
	char *tmp;

	memset(n, 0, sizeof(nmdc_myinfo_t));
	if(memcmp(buf, "$MyINFO $ALL ", 13) != 0)
		return false;
	buf += 13;

	/* Nick */
	len = strcspn(buf, " |");
	if(buf[len] == '|')
		return false;
	n->nick = strndup(buf, len);
	buf += len+1;

	/* Tag */
	while(*buf == ' ')
		buf++;
	len = strcspn(buf, "|$");
	if(buf[len] == '|')
		return false;
	ilen = nmdc_myinfo_parse_tag(n, buf, len);

	/* Description */
	while(ilen > 0 && buf[ilen-1] == ' ')
		ilen--;
	n->desc = ilen > 0 ? nmdc_unescape(enc, buf, ilen) : NULL;
	buf += len+1;

	/* Mode, some hubs (Ptokax) hide it in that odd otherwise-unused space */
	if(*buf == 'A' || *buf == 'P' || *buf == '5')
		n->mode = n->mode ? n->mode : *buf;
	else if(*buf != ' ')
		return false;
	if(buf[1] != '$')
		return false;
	buf += 2;

	/* Connection + flag */
	len = strcspn(buf, "$|");
	if(len < 1 || buf[len] == '|')
		return false;
	n->flag = buf[len-1];
	n->conn = len > 1 ? nmdc_unescape(enc, buf, len-1) : NULL;
	buf += len+1;

	/* Mail */
	while(*buf == ' ')
		buf++;
	len = strcspn(buf, "$|");
	if(buf[len] == '|')
		return false;
	n->mail = len > 0 ? nmdc_unescape(enc, buf, len) : NULL;
	buf += len+1;

	/* Share size */
	tmp = NULL;
	n->share = strtoull(buf, &tmp, 10);
	if(tmp == buf)
		n->share = 0;
	return tmp && *tmp == '$' && tmp[1] == '|';
}


uint32_t nmdc_conn2speed(const char *conn) {
	if(!conn || !*conn)
		return 0;

	float v;
	/* "<float>", used by DC++ and derivatives and is supposed to be intepreted as Mbit/s */
	if(strspn(conn, "1234567890.") == strlen(conn) && sscanf(conn, "%f", &v) == 1)
		return roundf(v*1000.0f*1000.0f/8.0f);

	char end = 0;
	/* "<x> KiB/s", started by DC++ to indicate the setting of the BW limiter. */
	if(sscanf(conn, " %f KiB/%c", &v, &end) == 2 && end == 's')
		return roundf(v*1024.0f);

	/* "<float>Kbps". 28.8Kbps, 33.6Kbps and 56Kbps were used by NMDC1. Who
	 * knows what other values people may use. */
	end = 0;
	if(sscanf(conn, " %f Kbp%c", &v, &end) == 2 && end == 's')
		return roundf(v*1000.0f/8.0f);

	/* Some old strings used by NMDC. These numbers are bullshit, but so are
	 * the connection strings... */
	struct { char s[12]; uint32_t r; } str[] = {
		{ "Satellite", 64*1024 },
		{ "ISDN",      16*1024 },
		{ "DSL",      128*1024 }, /* Can be anything, really. */
		{ "Cable",    128*1024 }, /* Same */
		{ "Modem",      7*1024 },
		{ "LAN(T1)",    193000 }, /* Nobody has these anymore */
		{ "LAN(T3)",   5592000 }
	};
	size_t i;
	for(i=0; i<sizeof(str)/sizeof(*str); i++)
		if(strcmp(str[i].s, conn) == 0)
			return str[i].r;

	return 0; /* No idea... */
}


char *nmdc_chat_parse(const char *msg, char **nickoff, int *nicklen, bool *me) {
	*nickoff = NULL;
	*nicklen = 0;
	*me = false;

	if(*msg == '<') {
		int l = strcspn(msg+1, ">|");
		if(l && msg[l+1] != '|' && msg[l+2] == ' ') {
			*nickoff = (char *)msg+1;
			*nicklen = l;
			return (char *) (msg + l + 3);
		}

	} else if(*msg == '*' && msg[1] == '*' && msg[2] == ' ') {
		int l = strcspn(msg+3, " |");
		if(l && msg[l+3] != '|') {
			*nickoff = (char *)msg+3;
			*nicklen = l;
			*me = true;
			return (char *) (msg + l + 4);
		}
	}

	return (char *)msg;
}


/* vim: set noet sw=4 ts=4: */
