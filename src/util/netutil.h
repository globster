/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


#ifndef UTIL_NETUTIL_H
#define UTIL_NETUTIL_H


/* Check whether an IPv4 or IPv6 addr is the "any" address. */
static inline bool net_ip4_isany(struct in_addr x) {
	return x.s_addr == INADDR_ANY;
}

static inline bool net_ip6_isany(struct in6_addr x) {
	return memcmp(&x, &in6addr_any, sizeof(struct in6_addr)) == 0;
}



#define MAX_ADDRSTRLEN 50


/* Convert a socket address to a string, addr must be large enough to hold
 * MAX_ADDSTRRLEN bytes. Resulting string will be in the form of "0.0.0.0:123"
 * for IPv4 and "[123::FFF]:123" for IPv6. */
void net_sockaddr_str(int af, const void *src, char *dst);


/* Create a non-blocking socket */
int net_socket(int domain, int type, int protocol);




typedef struct net_getaddrinfo_t net_getaddrinfo_t;
typedef void(*net_getaddrinfo_func_t)(net_getaddrinfo_t *);

struct net_getaddrinfo_t {
	evtp_work_t work;
	net_getaddrinfo_func_t cb;
	struct addrinfo hints; /* in */
	uint16_t service;      /* in */
	int ret;               /* out */
	struct addrinfo *lst;  /* out */
	char node[];           /* in (flexible array member) */
};


/* Performs an async getaddrinfo(). When done, cb will be called with the results.
 * The caller is responsible for freeing the 'lst' member with freeaddrinfo(),
 * but the net_getaddrinfo_t object itself is automatically freed after the cb
 * has returned.
 * The node and hints are copied, so do not have to remain accessible.
 * 'node' should not be NULL, and, as should be obvious from the function
 * signature, symbolic service lookups are not supported. */
net_getaddrinfo_t *net_getaddrinfo(const char *node, uint16_t service, const struct addrinfo *hints, net_getaddrinfo_func_t cb);




/* Connection API. */

typedef enum {
	/* getaddrinfo() succeeded. @addr points to the first address. */
	NET_CONNECT_PROGRESS_DNS,
	/* We're attempting to connect() to @addr. */
	NET_CONNECT_PROGRESS_CONNECT,
	/* The connection has been established. @val is set to the socket handle,
	 * @addr points to the address that succeeded. */
	NET_CONNECT_DONE,
	/* getaddrinfo() failed. @val holds its return value. */
	NET_CONNECT_ERROR_DNS,
	/* bind() failed (always fatal, indicates a misconfiguration). Error is in errno. */
	NET_CONNECT_ERROR_BIND,
	/* The last connect() failed, and we ran out of addresses. Error is in errno. */
	NET_CONNECT_ERROR_CONNECT,
	/* Unable to establish connection within the timeout */
	NET_CONNECT_ERROR_TIMEOUT,
	/* Connection has been cancelled with net_connect_cancel() */
	NET_CONNECT_CANCELLED
} net_connect_status_t;

typedef struct net_connect_t net_connect_t;

/* Function that is called when there is something to report. The
 * interpretation of @val and @addr depends on the status, as described above.
 * The memory that @addr points to is freed automatically after the last call
 * to the callback function.
 */
typedef void(*net_connect_func_t)(net_connect_t *c, net_connect_status_t status, int val, struct addrinfo *addr);

struct net_connect_t {
	/* Public */
	void *data;

	/* Timeout on the total DNS resolution and connect operation. Leave
	 * zero-initialized to default to 120 seconds. */
	ev_tstamp timeout;

	/* Addresses to bind() to before calling connect(). Leave zero-initialized
	 * to not call bind() at all.*/
	struct sockaddr_in bind4;
	struct sockaddr_in6 bind6;

	/* Private */
	int sock;
	ev_io io;
	ev_timer timer;
	net_getaddrinfo_t *gai;
	net_connect_func_t cb;
	struct addrinfo *lst, *cur;
};


/* Resolves the hostname or IP address and attempts to open a connection. When
 * done, cb() will be called with either NET_CONNECT_DONE or a
 * NET_CONNECT_ERROR_* status. It may be called zero or more times with a
 * NET_PROGRESS_* status while it's busy.
 * The net_connect_t object must remain valid until cb() has been called for
 * the last time. */
void net_connect(net_connect_t *c, const char *host, uint16_t port, net_connect_func_t cb);


/* Cancels an ongoing connection. The callback is immediately called with a
 * NET_PROGRESS_CANCELLED status, so beware of recursion. */
void net_connect_cancel(net_connect_t *c);


#endif
/* vim: set noet sw=4 ts=4: */
