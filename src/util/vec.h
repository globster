/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef UTIL_VEC_H
#define UTIL_VEC_H


/* Simple vector/array abstraction.
 *
 * v.n = number of items in the array
 * v.m = number of allocated items
 * v.a = the array
 *
 * Vectors must be zero-initialized before use.
 * To free the vector: free(v.a) or vec_clear()
 *
 * Element access and iteration is as usual:
 *
 *   size_t i;
 *   for(i=0; i<v.n; i++)
 *     printf("v[%d] = %s\n", i, v.a[i]);
 */

#define vec_t(type) struct { size_t n, m; type *a; }


#define vec_clear(_v) (free((_v).a), (_v).a = NULL, (_v).n = (_v).m = 0)


#define vec_grow(_v) (\
		(_v).m = (_v).m ? (_v).m << 1 : 4,\
		(_v).a = realloc((_v).a, sizeof(*(_v).a) * (_v).m)\
	)


/* Insert an item at the given index in the array. If there already is an
 * element at given index, all elements on and after the index will be moved
 * one position to the right to preserve order of the list.
 * The index must be 0 <= i <= v.n.
 *
 * To insert an item in front of the array:
 *   vec_insert_order(v, 0, value);
 * To append an item to the end of the array:
 *   vec_insert_order(v, v.n, value);
 */
#define vec_insert_order(_v, _i, _x) (\
		(void)((_v).n+1 >= (_v).m && vec_grow(_v)),\
		memmove((_v).a+(_i)+1, (_v).a+(_i), sizeof(*(_v).a) * ((_v).n-(_i))),\
		(_v).a[_i] = (_x),\
		(_v).n++\
	)


/* Remove an item. Any elements to the right of the given index are moved one
 * position to the left to preserve order of the list. */
#define vec_remove_order(_v, _i) (\
		memmove((_v).a+(_i), (_v).a+(_i)+1, sizeof(*(_v).a) * ((_v).n - (_i))),\
		(_v).n--\
	)


/* Binary search.
 *
 * The _cmp argument is an expression that evaluates to an integer. Within the
 * expression, the variable `i' is available and set to the index of the
 * element to be compared with the requested element. If the resulting integer
 * is negative, the requested element should be located in the array before
 * `i', if it is positive it is located after `i'. The value 0 indicates that
 * the element is found.
 *
 * The _res argument is the code to be evaluated when the item has been found.
 * Again, the variable `i' is available and set to the index of the found item.
 * If nothing is found, _res will not be evaluated.
 *
 * An example of how to find an item of value `10' in an vec_t(int):
 *
 *   vec_t(int) v;
 *   size_t r = (size_t)-1;
 *   vec_search(v, v.a[i] - 10, r = i);
 *   if(r != (size_t)-1)
 *     // v.a[r] == 10
 *   else
 *     // No element with value 10 found
 */
#define vec_search(_v, _cmp, _res) do { if((_v).n) {\
		size_t _b = 0, _e = (_v).n-1;\
		while(_b <= _e) {\
			size_t i = _b + (_e - _b)/2;\
			int _c = (_cmp);\
			if(!_c) { { _res ; }; break; }\
			else if(_c < 0) _b = i+1;\
			else if(i)      _e = i-1;\
			else            break;\
		}\
	} } while(0)


/* Find the index where to insert an item into the array in the correct order.
 * The _cmp argument is the same as vec_search(). The result is stored in _r,
 * which must be a variable of type size_t.
 *
 * If _cmp evaluates to 0 (i.e. there is an item in the same position as you
 * wish to insert it), then _r is the index of that element. If there are
 * multiple elements for which _cmp may evaluate to 0, then _r is the index of
 * one such element, but which one is somewhat arbitrary.
 */
#define vec_search_insert(_v, _r, _cmp) do {\
		(_r) = 0;\
		vec_search(_v, ((_r) = i, (_cmp)),);\
		size_t i = (_r);\
		if(i < (_v).n && (_cmp) < 0) (_r)++;\
	} while(0)


/* Convenience wrapper for vec_search_insert() and vec_insert_order(). */
#define vec_insert_sorted(_v, _cmp, _x) do {\
		size_t _r;\
		vec_search_insert(_v, _r, _cmp);\
		vec_insert_order(_v, _r, _x);\
	} while(0)


#endif
/* vim: set noet sw=4 ts=4: */
