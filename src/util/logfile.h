/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


#ifndef UTIL_LOGFILE_H
#define UTIL_LOGFILE_H

/* This abstraction allows logging to files. The lines are logged as they are
 * given, without timestamp or other information. A global list of all log
 * files is kept in order to provide a mechanism to re-open all logs (on
 * SIGHUP, for example).
 *
 * These functions are not thread-safe. I/O errors are silently ignored. :-(
 *
 * TODO: Currently, logfile_log() flushes each line to the OS before returning.
 * This is required if you don't want to lose lines in the case of a crash, but
 * hinders performance if losing the very last entries is acceptable, for
 * example with chat logs. An interface should be added to allow deferred
 * flushing of the log entries.
 */

typedef struct logfile_t logfile_t;

logfile_t *logfile_open(const char *fn);

void logfile_close(logfile_t *);

void logfile_log(logfile_t *, const char *);

#define logfile_logf(logfile_l, ...) do {\
		kstring_t logfile_fmt = {};\
		ksprintf(&logfile_fmt, __VA_ARGS__);\
		logfile_log(logfile_l, logfile_fmt.s);\
		free(logfile_fmt.s);\
	} while(0)

void logfile_global_reopen();

#endif
/* vim: set noet sw=4 ts=4: */
