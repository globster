/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <global.h>


void base32_encode(const char *from, char *to, int len) {
	static const char alphabet[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
	int i, bits = 0, idx = 0, value = 0;
	for(i=0; i<len; i++) {
		value = (value << 8) | (unsigned char)from[i];
		bits += 8;
		while(bits > 5) {
			to[idx++] = alphabet[(value >> (bits-5)) & 0x1F];
			bits -= 5;
		}
	}
	if(bits > 0)
		to[idx++] = alphabet[(value << (5-bits)) & 0x1F];
	to[idx] = 0;
}


void base32_decode(const char *from, char *to, int len) {
	int i = 0, bits = 0, idx = 0, value = 0;
	while(idx < len) {
		value = (value << 5) | (from[i] <= '9' ? (26+(from[i]-'2')) : from[i]-'A');
		i++;
		bits += 5;
		while(bits >= 8) {
			to[idx++] = (value >> (bits-8)) & 0xFF;
			bits -= 8;
		}
	}
}

/* vim: set noet sw=4 ts=4: */
