/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


#ifndef UTIL_ADC_H
#define UTIL_ADC_H

typedef int32_t adc_cmd_t;
typedef int32_t adc_sid_t;
typedef int16_t adc_param_t;


/* Converts a three-letter string into an adc_cmd_t */
#define ADC_CMD(a, b, c) ((((adc_cmd_t)(a))<<16) + (((adc_cmd_t)(b))<<8) + (adc_cmd_t)(c))


/* Some predefined command names */
#define ADC_CMD_SUP ADC_CMD('S','U','P')
#define ADC_CMD_STA ADC_CMD('S','T','A')
#define ADC_CMD_INF ADC_CMD('I','N','F')
#define ADC_CMD_MSG ADC_CMD('M','S','G')
#define ADC_CMD_SCH ADC_CMD('S','C','H')
#define ADC_CMD_RES ADC_CMD('R','E','S')
#define ADC_CMD_CTM ADC_CMD('C','T','M')
#define ADC_CMD_RCM ADC_CMD('R','C','M')
#define ADC_CMD_GPA ADC_CMD('G','P','A')
#define ADC_CMD_PAS ADC_CMD('P','A','S')
#define ADC_CMD_QUI ADC_CMD('Q','U','I')
#define ADC_CMD_GET ADC_CMD('G','E','T')
#define ADC_CMD_GFI ADC_CMD('G','F','I')
#define ADC_CMD_SND ADC_CMD('S','N','D')
#define ADC_CMD_SID ADC_CMD('S','I','D')
#define ADC_CMD_ZON ADC_CMD('Z','O','N')
#define ADC_CMD_ZOF ADC_CMD('Z','O','F')


/* Convert a two-letter parameter name to an adc_param_t. */
#define ADC_PARAM(a, b) ((((adc_param_t)(a))<<8) + ((adc_param_t)(b)))


/* Macros for "internal" use. */
#define adc_isupalpha(x)    ((x) >= 'A' && (x) <= 'Z')
#define adc_isnum(x)        ((x) >= '0' && (x) <= '9')
#define adc_isupalphanum(x) (adc_isupalpha(x) || adc_isnum(x))
#define adc_isbase32(x)     (((x) >= 'A' && (x) <= 'Z') || ((x) >= '2' && (x) <= '7'))
#define adc_iscmdtype(x)    ((x) == 'B' || (x) == 'C' || (x) == 'D' || (x) == 'E' || (x) == 'F' || (x) == 'H' || (x) == 'I' || (x) == 'U')
#define adc_isparamend(x)   ((x) == 0 || (x) == ' ' || (x) == '\n')
#define adc_hassrc(x)       ((x) == 'B' || (x) == 'D' || (x) == 'E' || (x) == 'F')
#define adc_hasdest(x)      ((x) == 'D' || (x) == 'E')


/* Protocol states */
typedef enum {
	ADC_PROTOCOL =  1,
	ADC_IDENTIFY =  2,
	ADC_VERIFY   =  4,
	ADC_NORMAL   =  8,
	ADC_DATA     = 16
} adc_state_t;




/*
 * General utility functions
 */


/* Convert a (four-character, base32-encoded) SID into an integer in the range
 * of 0 to 2^20-1, both inclusive. Returns -1 on error. */
adc_sid_t adc_str2sid(const char *str);


/* The opposite of str2sid, writes the zero-terminated base32-encoded string to
 * dst, which must have space for at least 5 characters. */
void adc_sid2str(adc_sid_t sid, char *dst);


/* ADC-escapes the given zero-terminated string and appends the result to the
 * given kstring. */
void adc_escape_kstr(kstring_t *dest, const char *str);


/* Similar to adc_escape_kstr(), but returns a new buffer which should be
 * free()d after use. */
static inline char *adc_escape(const char *str) {
	kstring_t dest = {};
	adc_escape_kstr(&dest, str);
	return dest.s ? dest.s : calloc(1, 1);
}


/* Unescapes the parameter pointed to by *str, which must be terminated with
 * either \0, \n or a space. Returns false if the string contains an invalid
 * encoding, after which some rubbish may have been appended to *dest. */
bool adc_unescape_kstr(kstring_t *dest, const char *str);


static inline char *adc_unescape(const char *str) {
	kstring_t dest = {};
	if(!adc_unescape_kstr(&dest, str)) {
		free(dest.s);
		return NULL;
	}
	return dest.s ? dest.s : calloc(1, 1);
}




/*
 * Command validation and parsing
 */

/* TODO: Fetching the CID for U-type messages and fetching/parsing the feature
 * list for F-type messages. */


/* Validates the command, which must be terminated with a \n. If the command is
 * syntactically valid, its length in bytes is returned, including the final
 * \n. 0 is returned if the command is invalid or malformed.
 * This is the only function that can handle a malformed command, the other
 * functions below assume that the given command has been validated. */
int adc_validate(const char *cmd, int len);


/* Returns the name of the command, in ADC_CMD() form. Returns 0 on if this is
 * the empty command (i.e. a lone \n). */
static inline adc_cmd_t adc_command(const char *cmd) {
	return *cmd == '\n' ? 0 : ADC_CMD(cmd[1], cmd[2], cmd[3]);
}


/* Returns the message type (B/C/D/E/F/H/I/U), or 0 if this is the empty command. */
static inline char adc_type(const char *cmd) {
	return *cmd == '\n' ? 0 : *cmd;
}


/* Returns the source SID (my_sid) of the command, or -1 if it doesn't have one.
 */
static inline adc_sid_t adc_source(const char *cmd) {
	return adc_hassrc(*cmd) ? adc_str2sid(cmd+5) : -1;
}


/* Returns the destination SID (target_sid) of the command, or -1 if it doesn't
 * have one. */
static inline adc_sid_t adc_dest(const char *cmd) {
	return adc_hasdest(*cmd) ? adc_str2sid(cmd+10) : -1;
}


/* Two functions for iterating over the command parameters. adc_param_start()
 * returns a pointer to the first parameter, or NULL if the command has no
 * parameters. Similarly, given such a pointer, adc_param_next() returns the
 * pointer to the next parameter, or NULL if there are no more. */
char *adc_param_start(const char *cmd);


static inline char *adc_param_next(const char *param) {
	while(*param != '\n' && *param != ' ')
		param++;
	return *param == ' ' ? (char *)param+1 : NULL;
}


/* Returns the name of the parameter in the form of ADC_PARAM(), or 0 if this
 * can't be a valid named parameter. */
static inline adc_param_t adc_param_name(const char *param) {
	return adc_isupalpha(param[0]) && adc_isupalphanum(param[1]) ? ADC_PARAM(param[0], param[1]) : 0;
}


/* Unescapes the (positional) parameter and returns a newly allocated buffer
 * with its value, which must be free()d after use. */
static inline char *adc_param_posvalue(const char *param) {
	return adc_unescape(param);
}


/* Similar to adc_param_posvalue, but for obtaining the value of a named
 * parameter. This function requires that adc_param_name(param) != 0, i.e. the
 * given parameter must be a named one. */
static inline char *adc_param_namevalue(const char *param) {
	return adc_unescape(param+2);
}




/*
 * Command creation and formatting
 */


/* These functions are used as follows:
 *
 *   kstring_t cmd = {};
 *   adc_create(&cmd, 'D', ADC_CMD_MSG, my_sid, target_sid);
 *   adc_appendf(&cmd, "hugs %s", target_user);  // positional parameter
 *   adc_append(&cmd, "ME1");                    // named parameter
 *   adc_final(&cmd);
 *
 * After that, cmd.s contains the zero-terminated command.
 */

/* Create a new ADC command. src and/or dst are ignored for command types that
 * don't have them. adc_append() must be used to add the feature list or CID
 * for F and U-types, respectively. */
void adc_create(kstring_t *dest, char type, adc_cmd_t cmd, adc_sid_t src, adc_sid_t dst);


static inline void adc_append(kstring_t *dest, const char *str) {
	kputc(' ', dest);
	adc_escape_kstr(dest, str);
}


/* This is kinda ugly, but kstring doesn't have a vsprintf() variant. */
#define adc_appendf(dest, ...) do {\
		kstring_t adc_fmt = {};\
		ksprintf(&adc_fmt, __VA_ARGS__);\
		adc_append(dest, adc_fmt.s);\
		free(adc_fmt.s);\
	} while(0)


static inline void adc_final(kstring_t *dest) {
	kputc('\n', dest);
}


#endif
/* vim: set noet sw=4 ts=4: */
