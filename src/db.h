/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef DB_H
#define DB_H


/* Global database connection handle */
extern sqlasync_t *db_sql;

/* Database/session directory. Note that this path may be relative and may not
 * necessarily be in UTF-8. */
extern const char *db_dir;


/* Wrapper around sqlasync_queue_get(). If the result is either SQLITE_OK,
 * SQLITE_ROW or SQLITE_DONE, then it is passed as-is. Otherwise, this function
 * logs the error, frees the result and returns NULL.
 * `op' should be a short descriptive name of the operation you were doing, in
 * order to provide an easier-to-trace error message. */
sqlasync_result_t *db_get(sqlasync_queue_t *q, const char *op);


void db_init(const char *, logfile_t **);


/* This closes the database and, when done, drops the reference on the event
 * loop. This function is called from main_shutdown(), which means that, if any
 * SQL queries have to be processed while shutting down, they need to be queued
 * directly in one of the shutdown handlers called from main_shutdown().
 * Queueing any SQL operations after this call is an error.
 * A more "clean" solution would ensure that no reference is kept on the event
 * loop at all, allowing any code to keep queueing SQL queries while shutting
 * down. This is not easily possible because we need to keep the error queue
 * (db_error_cb) alive. It is possible to use a hack'ish workaround using
 * ev_unref(), but I'd like to avoid that as long as the above restriction
 * doesn't complicate other parts of the code too much.
 */
void db_shutdown();

void db_destroy();


#endif
/* vim: set noet sw=4 ts=4: */
