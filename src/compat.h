/* Copyright (c) 2012-2013 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef COMPAT_H
#define COMPAT_H


#ifndef DBUS_ERROR_UNKNOWN_PROPERTY
#define DBUS_ERROR_UNKNOWN_PROPERTY "org.freedesktop.DBus.Error.UnknownProperty"
#endif
#ifndef DBUS_ERROR_PROPERTY_READ_ONLY
#define DBUS_ERROR_PROPERTY_READ_ONLY "org.freedesktop.DBus.Error.PropertyReadOnly"
#endif


/* GnuTLS / libgcrypt functions */
#ifdef USE_GCRYPT
#define crypt_rnd(buf, len) gcry_randomize(buf, len, GCRY_STRONG_RANDOM)
static inline void crypt_sha256(const void *buf, int len, void *res) {
	gcry_md_hd_t hd;
	gcry_md_open(&hd, GCRY_MD_SHA256, 0);
	gcry_md_write(hd, buf, len);
	memcpy(res, gcry_md_read(hd, 0), 32);
	gcry_md_close(hd);
}
#else
#define crypt_rnd(buf, len) assert(gnutls_rnd(GNUTLS_RND_RANDOM, buf, len) == 0)
static inline void crypt_sha256(const void *buf, int len, void *res) {
	gnutls_hash_hd_t hd;
	gnutls_hash_init(&hd, GNUTLS_DIG_SHA256);
	gnutls_hash(hd, buf, len);
	gnutls_hash_deinit(hd, res);
}
#endif


#endif
/* vim: set noet sw=4 ts=4: */
